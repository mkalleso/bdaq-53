#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Run customized analysis on a specified raw data file
'''

from bdaq53.analysis.analysis import Analysis
from bdaq53.analysis.plotting import Plotting


raw_data_file = '.h5'

with Analysis(raw_data_file=raw_data_file, analyzed_data_file=None,
                       store_hits=False, cluster_hits=False, chunk_size=1000000) as a:
    a.analyze_data()
#     a.analyze_adc_data()
    
with Plotting(analyzed_data_file=a.analyzed_data_file, pdf_file=None, level='preliminary',
              internal=False, save_single_pdf=False, save_png=False) as p:
    p.create_standard_plots()
