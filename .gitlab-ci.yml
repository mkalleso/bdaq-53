stages:
  - codestyle
  - test
  - report

# Tests using RD53 and FPGA co-simulation
test:rd53:
    stage: test
    image: centos:centos7.9.2009 # centos 7 docker image
    needs: []  # do not wait for code style
    variables:
        # Otherwise lock file of cancelled runs might stall future check outs
        # https://gitlab.cern.ch/silab/bdaq53/issues/293
        GIT_STRATEGY: clone
    tags:  # tags to differentiate runners able to run the job
      - itkpixv1
    before_script:
      - source /RD53B/setup_env.sh
      - yum groupinstall "Development Tools" -y --setopt=group_package_types=mandatory,default,optional
      - yum install git -y
      - curl -L https://repo.continuum.io/miniconda/Miniconda3-4.7.12.1-Linux-x86_64.sh -o miniconda.sh
      - bash miniconda.sh -b -p $HOME/miniconda
      - export PATH=$HOME/miniconda/bin:$PATH
      - conda update --yes conda
      - conda install --yes numpy bitarray pytest pyyaml scipy numba pytables pyqt matplotlib tqdm pyzmq blosc psutil pexpect coloredlogs ptyprocess iminuit lxml pip
      # Setup co-simulation
      - pip install cocotb
      - pip install cocotb-bus
      - git clone -b master --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..;
    script:
      - python setup.py develop
      - cd bdaq53/tests
      - source setup_questa.sh > /dev/null
      - pytest -s -v rd53a/test_rd53

# Tests bdaq software without hardware co-simulation, do scan tests on different runner to save time
test:software:
    stage: test
    needs:
      - codestyle:code_style
    variables:
      # Otherwise lock file of cancelled runs might stall future check outs
      # https://gitlab.cern.ch/silab/bdaq53/issues/293
      GIT_STRATEGY: clone
    tags:  # tags to differentiate runners able to run the job
      - docker  # Use CERN shared runners
    image: continuumio/anaconda3:2021.05  # Ubuntu based miniconda image
    before_script:
      # Install git-lfs
      - apt-get update --fix-missing --allow-releaseinfo-change --allow-unauthenticated
      - apt-get install -y curl build-essential
      - curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
      - apt-get install -y git-lfs
      - git lfs install
      - git submodule sync --recursive
      - git submodule update --init --recursive
      # Update miniconda python and install required binary packages
      - conda update --yes conda
      - conda install --yes numpy bitarray pytest pyyaml scipy numba pytables pyqt matplotlib tqdm pyzmq blosc psutil pexpect coloredlogs iminuit lxml pympler
      # - conda install pytest-xdist  # Allow testing in parallel, does not work due to mocks that act globally
      # Install virtual x server for bdaq53 monitor Qt gui tests
      - apt-get install -y xvfb
      - pip install xvfbwrapper pytest-cov  # pytest plugins: pytest-sugar, pytest-cov for progress visualization and coverage reporting
      # Install basil tag from github
      - git clone -b master --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..;
      # - if [ -z "$CI_COMMIT_TAG"]; then git clone -b v3.0.0 --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..; else git clone -b development --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..; fi
    script:
      - python setup.py develop
      # Try to activate conda evironment in the newest broken miniconda docker
      - conda init bash  # https://github.com/ContinuumIO/docker-images/issues/89
      - source ~/.bashrc  # since ==> For changes to take effect, close and re-open your current shell. <==
      - conda activate  # to link properly to pytest
      # Run unit tests with a coverage and create a coberture-xml report to be used by gitlab and also print report to terminal
      - export COVERAGE_FILE=.coverage_software
      - pytest --cov-report term --cov=bdaq53 bdaq53/tests/test_software/base --ignore=bdaq53/tests/test_software/base/test_memory_leaks.py
      # Filter warnings and deactivate coverage, since these data is stored in python and would trigger a "memory leak"
      - pytest bdaq53/tests/test_software/base/test_memory_leaks.py -p no:warnings
    # coverage: '/^TOTAL.+?(\d+\%)$/'
    artifacts:
      paths:
        - ".coverage_software"
    
# Tests for scan script regressions in software without hardware co-simulation
test:scans:
    stage: test
    needs:
      - codestyle:code_style
    variables:
      # Otherwise lock file of cancelled runs might stall future check outs
      # https://gitlab.cern.ch/silab/bdaq53/issues/293
      GIT_STRATEGY: clone
    tags:  # tags to differentiate runners able to run the job
      - docker  # Use CERN shared runner
    image: continuumio/anaconda3:2021.05  # Ubuntu based miniconda image
    before_script:
      # Install git-lfs
      - apt-get update --fix-missing --allow-releaseinfo-change --allow-unauthenticated
      - apt-get install -y curl build-essential
      - curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
      - apt-get install -y git-lfs
      - git lfs install
      - git submodule sync --recursive
      - git submodule update --init --recursive
      # Update miniconda python and install required binary packages
      - conda update --yes conda
      - conda install --yes numpy bitarray pytest pyyaml scipy numba pytables pyqt matplotlib tqdm pyzmq blosc psutil pexpect coloredlogs iminuit lxml
      # - conda install pytest-xdist  # Allow testing in parallel, does not work due to mocks that act globally
      # Install virtual x server for bdaq53 monitor Qt gui tests
      - apt-get install -y xvfb
      - pip install xvfbwrapper pytest-cov # pytest plugins: pytest-sugar, pytest-cov for progress visualization and coverage reporting
      # Install basil tag from github
      - git clone -b master --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..;
      # - if [ -z "$CI_COMMIT_TAG"]; then git clone -b v3.0.0 --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..; else git clone -b development --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..; fi
    script:
      - python setup.py develop
      # Try to activate conda evironment in the newest broken miniconda docker
      - conda init bash  # https://github.com/ContinuumIO/docker-images/issues/89
      - source ~/.bashrc  # since ==> For changes to take effect, close and re-open your current shell. <==
      - conda activate  # to link properly to pytest
      - export COVERAGE_FILE=.coverage_scans
      - pytest --cov=bdaq53 bdaq53/tests/test_software/scans/test_scans.py
      - pytest --cov=bdaq53 --cov-append bdaq53/tests/test_software/scans/test_meta_scans.py
      - pytest --cov=bdaq53 --cov-append --cov-report term bdaq53/tests/test_software/scans/test_tunings.py
    # coverage: '/^TOTAL.+?(\d+\%)$/'
    artifacts:
      paths:
        - ".coverage_scans"
        
# Tests for scan script regressions in software without hardware co-simulation
test:analysis:
    stage: test
    needs:
      - codestyle:code_style
    variables:
      # Otherwise lock file of cancelled runs might stall future check outs
      # https://gitlab.cern.ch/silab/bdaq53/issues/293
      GIT_STRATEGY: clone
    tags:  # tags to differentiate runners able to run the job
      - docker  # Use CERN shared runners
    image: continuumio/anaconda3:2021.05  # Ubuntu based miniconda image
    before_script:
      # Install git-lfs
      - apt-get update --fix-missing --allow-releaseinfo-change --allow-unauthenticated
      - apt-get install -y curl build-essential
      - curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
      - apt-get install -y git-lfs
      - git lfs install
      - git submodule sync --recursive
      - git submodule update --init --recursive
      # Update miniconda python and install required binary packages
      - conda update --yes conda
      - conda install --yes numpy bitarray pytest pyyaml scipy numba pytables pyqt matplotlib tqdm pyzmq blosc psutil pexpect coloredlogs iminuit lxml
      # - conda install pytest-xdist  # Allow testing in parallel, does not work due to mocks that act globally
      # Install virtual x server for bdaq53 monitor Qt gui tests
      - apt-get install -y xvfb
      - pip install xvfbwrapper pytest-cov # pytest plugins: pytest-sugar, pytest-cov for progress visualization and coverage reporting
      # Install basil tag from github
      - git clone -b master --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..;
      # - if [ -z "$CI_COMMIT_TAG"]; then git clone -b v3.0.0 --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..; else git clone -b development --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..; fi
    script:
      - python setup.py develop
      # Try to activate conda evironment in the newest broken miniconda docker
      - conda init bash  # https://github.com/ContinuumIO/docker-images/issues/89
      - source ~/.bashrc  # since ==> For changes to take effect, close and re-open your current shell. <==
      - conda activate  # to link properly to pytest
      - export COVERAGE_FILE=.coverage_analysis
      - pytest --cov-report term --cov=bdaq53 bdaq53/tests/test_software/analysis --ignore=bdaq53/tests/test_software/analysis/test_monitor.py
    artifacts:
      paths:
        - ".coverage_analysis"
        
# Combine coverage reports and report to gitlab, https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html
report:coverage:
    stage: report
    needs:  # wait for coverage reporting test jobs
      - test:software
      - test:scans
      - test:analysis
    tags:  # tags to differentiate runners able to run the job
      - docker  # Use CERN shared runners
    image: continuumio/anaconda3:2021.05  # Ubuntu based miniconda image
    before_script:
      # Install git-lfs
      - apt-get update --fix-missing --allow-releaseinfo-change --allow-unauthenticated
      - apt-get install -y curl build-essential
      - curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
      - apt-get install -y git-lfs
      - git lfs install
      - git submodule sync --recursive
      - git submodule update --init --recursive
      # Update miniconda python and install required binary packages
      - conda update --yes conda
      - conda install --yes coverage pytest
      - pip install pytest-cov # pytest plugins: pytest-sugar, pytest-cov for progress visualization and coverage reporting
    script:
      # Try to activate conda evironment in the newest broken miniconda docker
      - conda init bash  # https://github.com/ContinuumIO/docker-images/issues/89
      - source ~/.bashrc  # since ==> For changes to take effect, close and re-open your current shell. <==
      - conda activate  # to link properly to pytest
      - coverage combine .coverage_software .coverage_analysis .coverage_scans # combine .coverage* files
      - coverage xml  # create cobertura formatted coverage.xml, that can be used by gitlab
      - coverage report  # print to terminal to allow regex to catch output
    coverage: '/^TOTAL.+?(\d+\%)$/'  # regex parse to show on gitlab
    artifacts:
      name: all-coverage-reports
      paths:
        - "*.coverage*"
      expire_in: 4 month
      reports:
        coverage_report:
          coverage_format: cobertura
          path: coverage.xml

# Tests for eudaq newest v1.x-dev branch compatibility
test:eudaq:
    stage: test
    needs:
      - codestyle:code_style
    variables:
      # Otherwise lock file of cancelled runs might stall future check outs
      # https://gitlab.cern.ch/silab/bdaq53/issues/293
      GIT_STRATEGY: clone
    tags:  # tags to differentiate runners able to run the job
      - docker  # Use CERN shared runners
    image: continuumio/anaconda3:2021.05  # Ubuntu based miniconda image
    before_script:
      # Install git-lfs
      - apt-get update --fix-missing --allow-releaseinfo-change --allow-unauthenticated
      - apt-get install -y curl build-essential
      - curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
      - apt-get install -y git-lfs
      - git lfs install
      - git submodule sync --recursive
      - git submodule update --init --recursive
      - apt-get install -y build-essential cmake  # to be able to compile EUDAQ
      # Update miniconda python and install required binary packages
      - conda update --yes conda
      - conda install --yes numpy bitarray pytest pyyaml scipy numba pytables pyqt matplotlib tqdm pyzmq blosc psutil pexpect coloredlogs iminuit lxml
      # Install basil tag from github
      - git clone -b master --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..;
      # - if [ -z "$CI_COMMIT_TAG"]; then git clone -b v3.0.0 --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..; else git clone -b development --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..; fi
    script:
       # Install EUDAQ
      - source bdaq53/tests/setup_eudaq.sh
      - python setup.py develop
      # Try to activate conda evironment in the newest broken miniconda docker
      - conda init bash  # https://github.com/ContinuumIO/docker-images/issues/89
      - source ~/.bashrc  # since ==> For changes to take effect, close and re-open your current shell. <==
      - conda activate  # to link properly to pytest
      # Do not run virtual x server tests (monitor) in this runner due to segfault
      - pytest -v bdaq53/tests/test_software/scans/test_eudaq.py

# Tests that need bdaq53 readout hardware and RD53 FE
# Needs Ubuntu system with xvfb, Miniconda3 and full Xilinx installed and
# Gitlab-runner with shell executor: https://docs.gitlab.com/runner/executors/
test:hardware:
  stage: test
  needs: []  # do not wait for code style
  allow_failure: true
  variables:
    GIT_STRATEGY: clone
  tags:  # tags to differentiate runners able to run the job
    - hardware  # Use silab hardware runner
  image: centos:7 # centos 7 docker image
  before_script:
    - source /RD53B/setup_env.sh
    - yum install git -y
    - yum install libusb -y
    - curl -L https://repo.continuum.io/miniconda/Miniconda3-4.7.12.1-Linux-x86_64.sh -o miniconda.sh
    - bash miniconda.sh -b -p $HOME/miniconda
    - export PATH=$HOME/miniconda/bin:$PATH
    - conda update --yes conda
    - conda install --yes numpy bitarray pytest pyyaml scipy numba pytables pyqt matplotlib tqdm pyzmq blosc psutil pexpect coloredlogs ptyprocess iminuit lxml pip
    # Setup co-simulation
    - pip install xvfbwrapper
    - yum install libX11 -y
    - git clone -b master --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..;
  script:
    - python setup.py develop
    # Compile firmware if changes detected and flash
    #- if [[ `git diff ..development -- firmware` ]]; then bdaq_firmware --firmware BDAQ53_RX640 -c; bdaq_firmware --firmware BDAQ53_1LANE_RX640.bit --target fpga; else bdaq_firmware --bdaq_firmware dev_BDAQ53_1LANE_RX640 --target fpga; fi
    - bdaq_firmware --firmware BDAQ53_RX640 -c; bdaq_firmware --firmware BDAQ53_1LANE_RX640_1G.bit --target fpga
    - pytest -v bdaq53/tests/test_hardware
      # use the hardware runner for online monitor test, since cern gitlab software runner segfaults when using xserver
    # - pytest -v bdaq53/tests/test_software/analysis/test_monitor.py # This is no longer possible, since the runner is now fully dockerized
  after_script:
    - cd /RD53B
#    - docker system prune -a -f
    - if [ -d ./artifacts ]; then rm -R ./artifacts; fi
    - mkdir artifacts
    - if [ -f /builds/silab/bdaq53/firmware/vivado/vivado.log ]; then cp -p /builds/silab/bdaq53/firmware/vivado/vivado.log /RD53B/artifacts/vivado.log; fi
    - if [ -d /builds/silab/bdaq53/bdaq53/scans/output_data/ ]; then cp -rp /builds/silab/bdaq53/bdaq53/scans/output_data/ /RD53B/artifacts/output_data/; fi
    - if [ -f /builds/silab/bdaq53/BDAQ53_1LANE_RX640_1G.bit ]; then cp -p /builds/silab/bdaq53/BDAQ53_1LANE_RX640_1G.bit /RD53B/artifacts/BDAQ53_1LANE_RX640_1G.bit; fi
  artifacts:
    when: always  # also upload verilog log on failure
    paths:
      - "vivado.log"
      - "output_data/"
      - "BDAQ53_1LANE_RX640_1G.bit"
    expire_in: 1 month

# Tests for code style violations in new code lines
codestyle:code_style:
    stage: codestyle
    variables:
      # Otherwise lock file of cancelled runs might stall future check outs
      # https://gitlab.cern.ch/silab/bdaq53/issues/293
      GIT_STRATEGY: clone
    tags:  # tags to differentiate runners able to run the job
      - docker  # Use CERN shared runners
    image: continuumio/anaconda3:2021.05  # Ubuntu based miniconda image
    before_script:
      - apt-get update --fix-missing --allow-releaseinfo-change --allow-unauthenticated
      - apt-get install -y build-essential
      # Update miniconda python and install required binary packages
      - conda update --yes conda
      - conda install --yes numpy bitarray pytest pyyaml scipy numba pytables pyqt matplotlib tqdm pyzmq blosc psutil pexpect coloredlogs flake8 iminuit lxml
      - pip install flake8-diff
      # Install basil tag from github
      - git clone -b master --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..;
      # - if [ -z "$CI_COMMIT_TAG"]; then git clone -b v3.0.0 --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..; else git clone -b development --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..; fi
    script:
      - python setup.py develop
      - flake8-diff origin/development  # check code style flaws of changed lines agains development branch

# Tests using ITkPixV1 and FPGA co-simulation
test:itkpixv1:
    stage: test
    image: centos:centos7.9.2009  # centos 7 docker image
    needs: []  # do not wait for code style
    variables:
        GIT_STRATEGY: clone
    tags:  # tags to differentiate runners able to run the job
      - itkpixv1
    before_script:
      - source /RD53B/setup_env.sh
      - yum groupinstall "Development Tools" -y --setopt=group_package_types=mandatory,default,optional
      - yum install git -y
      - curl -L https://repo.continuum.io/miniconda/Miniconda3-4.7.12.1-Linux-x86_64.sh -o miniconda.sh
      - bash miniconda.sh -b -p $HOME/miniconda
      - export PATH=$HOME/miniconda/bin:$PATH
      - conda update --yes conda
      - conda install --yes numpy bitarray pytest pyyaml scipy numba pytables pyqt matplotlib tqdm pyzmq blosc psutil pexpect coloredlogs ptyprocess iminuit lxml pip
      # Setup co-simulation
      - pip install cocotb
      - pip install cocotb-bus
      - git clone -b master --depth 1 https://github.com/SiLab-Bonn/basil.git; cd basil; python setup.py develop; cd ..;
    script:
      - python setup.py develop
      - cd bdaq53/tests
      - source setup_questa.sh
      - pytest -s -v ITkPixV1/test_rd53/test_SimDigitalScan.py
