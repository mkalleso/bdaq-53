#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script tests the data merging feature of RD53B chips
'''

import tables as tb
import random
import numpy as np
from bdaq53.analysis import plotting
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis_utils
from bdaq53.analysis import rd53b_analysis
from tqdm import tqdm

scan_configuration = {
    'invert': True,
    'start_delay': 0,
    'stop_delay': 150,
    'n_test': 10
}

SHIFT_TO_PS = 27.90178571


class ValueTable(tb.IsDescription):
    register = tb.StringCol(64, pos=0)
    write = tb.UInt16Col(pos=1)
    read = tb.UInt16Col(pos=2)
    rx = tb.UInt16Col(pos=3)
    clk_shift = tb.UInt16Col(pos=4)
    clk_shift_ps = tb.UInt16Col(pos=5)
    tries = tb.UInt16Col(pos=6)


class TestPatternTable(tb.IsDescription):
    in_pattern = tb.UInt64Col(pos=0)
    out_pattern = tb.UInt64Col(pos=1)
    rx = tb.UInt16Col(pos=2)
    clk_shift = tb.UInt16Col(pos=3)
    clk_shift_ps = tb.UInt16Col(pos=4)
    tries = tb.UInt16Col(pos=5)


class DataMergingTest(ScanBase):
    scan_id = 'data_merging_test'

    def _configure(self, **_):
        self.chip.write_command(self.chip.CMD_SYNC, repetitions=1000)

    def _scan(self, invert=True, start_delay=0, stop_delay=20, n_test=10):
        value_table = self.h5_file.create_table(self.h5_file.root, name='values', title='Values', description=ValueTable)
        pattern_table = self.h5_file.create_table(self.h5_file.root, name='pattern_table', title='pattern_table', description=TestPatternTable)

        # Configure BDAQ board for data merging
        self.bdaq.dm_invert(invert)  # Invert the DM signal (because of the Mini-DP cable) TODO: Invert in FW?
        self.bdaq.disable_tlu_module()
        timeout = 100
        self.bdaq.dm_output_enable('all')
        self.chip.disable_data_merging()
        chip_in_to_bdaq_out_dict = {0b0001: 1, 0b0010: 2, 0b0100: 3, 0b1000: 0}

        # self.chip.enable_data_merging(data_en=0b1111)
        # while(1):
        #     self.bdaq.dm_increment()
        #     print("low")
        #     time.sleep(10)
        #     self.bdaq.dm_decrement()
        #     print("high")
        #     time.sleep(10)

        for _ in range(start_delay):
            self.bdaq.dm_increment()
        for delay in tqdm(range(start_delay, stop_delay)):
            self.bdaq.dm_increment()
            for test_lanes in [0b0001, 0b0010, 0b0100, 0b1000]:
                self.bdaq['DATA_MERGING']['DM_ENABLE'] = 0
                self.bdaq['DATA_MERGING'].write()
                self.bdaq.dm_output_enable(chip_in_to_bdaq_out_dict[test_lanes])
                self.chip.write_command(self.chip.CMD_SYNC, repetitions=1000, wait_for_done='true')
                self.bdaq['FIFO'].get_data()
                self.log.debug('Establishing Datamerging communication')
                self.bdaq.dm_type('hitdata')
                self.bdaq.set_monitor_filter(mode='block')  # Block monitor frames
                try_cnt = 0
                self.chip.write_command(self.chip.CMD_SYNC, repetitions=3000, wait_for_done='true')
                for n in range(10):
                    try_cnt += 1
                    self.chip.enable_data_merging(data_en=test_lanes)
                    self.bdaq['FIFO']['RESET'] = 1
                    self.bdaq['FIFO'].get_data()
                    rx_data = 0
                    write_dat = 0xaaaaaaaa

                    self.bdaq.dm_send_hitdata(data=write_dat)
                    for _ in range(timeout):
                        self.chip.write_command(self.chip.CMD_SYNC, repetitions=50, wait_for_done='true')
                        if self.bdaq['FIFO'].get_FIFO_SIZE() > 0:
                            data = self.bdaq['FIFO'].get_data()
                            for elem in data:
                                if not elem & 0x80010000:
                                    rx_data = rx_data << 16
                                    rx_data += elem
                            if len(data) == 0:
                                continue
                    if write_dat == rx_data:
                        self.log.debug('Successfully established Datamerging communication')
                        break
                    else:
                        self.chip.disable_data_merging()

                # Test 1: Hit data
                self.log.debug('Testing data merging.')
                self.bdaq.dm_type('hitdata')
                self.bdaq.set_monitor_filter(mode='block')  # Block monitor frames
                self.bdaq['FIFO'].get_data()
                for x in range(n_test):
                    rx_data = 0
                    row = pattern_table.row
                    write_dat = random.randint(0, int(2**32 - 1))
                    # write_dat = (x << 28) + (x << 24) + (x << 20) + (x << 16) + (x << 12) + (x << 8) + (x << 4) + x
                    row['in_pattern'] = write_dat
                    row['rx'] = test_lanes
                    row['tries'] = try_cnt
                    row['clk_shift'] = delay
                    row['clk_shift_ps'] = delay * SHIFT_TO_PS
                    self.bdaq.dm_send_hitdata(data=write_dat)
                    for _ in range(timeout):
                        self.chip.write_command(self.chip.CMD_SYNC, repetitions=50, wait_for_done='true')
                        if self.bdaq['FIFO'].get_FIFO_SIZE() > 0:
                            data = self.bdaq['FIFO'].get_data()
                            for elem in data:
                                if not elem & 0x80010000:
                                    rx_data = rx_data << 16
                                    rx_data += elem
                            if len(data) == 0:
                                continue
                    row['out_pattern'] = rx_data
                    row.append()

                # Test 2: Register data
                self.log.debug('Starting data merging test. Testing {} registers with random data'.format(10))
                self.bdaq.dm_type('monitor')
                self.bdaq.set_monitor_filter(mode='filter')  # Allow monitor frames to pass
                self.chip.write_command(self.chip.CMD_SYNC, repetitions=100, wait_for_done='true')

                for dm_addr in range(n_test):
                    dm_data = random.randint(0, int(2**16 - 1))
                    row = value_table.row
                    row['register'] = dm_addr
                    row['write'] = dm_data
                    row['read'] = 0
                    row['rx'] = test_lanes
                    row['tries'] = try_cnt
                    row['clk_shift'] = delay
                    row['clk_shift_ps'] = delay * SHIFT_TO_PS

                    for BTF in [0x99]:  # d2:MM, 99:MA, 55:AM, AA:B4
                        self.bdaq['FIFO'].get_data()
                        self.bdaq.dm_send_register(header=BTF, address=dm_addr, data=dm_data)
                        if self.bdaq.board_version != 'SIMULATION':
                            self.chip.write_command(self.chip.CMD_SYNC, repetitions=100, wait_for_done='true')

                        for _ in range(timeout):
                            if self.bdaq['FIFO'].get_FIFO_SIZE() > 0:
                                data = self.bdaq['FIFO'].get_data()
                                interpreted_userk_data = rd53b_analysis.interpret_userk_data(data)
                                processed_userk_data = analysis_utils.process_userk(interpreted_userk_data)
                                if len(processed_userk_data) == 0:
                                    continue
                                for val in processed_userk_data:
                                    self.log.debug('Reading data merging data. Address= {}, Data= {})'.format(hex(val['Address']), hex(val['Data'])))
                                    row['read'] = val['Data']

                            if self.bdaq.board_version != 'SIMULATION':
                                self.chip.write_command(self.chip.CMD_SYNC, repetitions=10, wait_for_done='true')
                        row.append()
            self.bdaq['DATA_MERGING']['DM_ENABLE'] = 0
            self.bdaq['DATA_MERGING'].write()
            self.chip.disable_data_merging()
            self.log.success('Scan finished')

    def _analyze(self):
        if self.configuration['bench']['analysis']['create_pdf']:
            self.log.info('Comparing data...')
            with tb.open_file(self.output_filename + '.h5', 'r') as in_file:
                data = in_file.root.values[:]
                pattern_table = in_file.root.pattern_table[:]
                if len(data) == 0:
                    self.log.error('Data Merging test failed! No register value received.')
            mapping_dict = {1: 0, 2: 1, 4: 2, 8: 3}
            x_val = sorted(list(set(data['clk_shift'])))
            tested_lanes = sorted(list(set(data['rx'])))
            reg_out_array = np.zeros((len(tested_lanes), len(x_val)))
            hit_out_array = np.zeros((len(tested_lanes), len(x_val)))
            for delay_val in x_val:
                cur_reg_delay_arr = data[data['clk_shift'] == delay_val]
                cur_hit_delay_arr = pattern_table[data['clk_shift'] == delay_val]
                for lane in tested_lanes:
                    cur_reg_lane_arr = cur_reg_delay_arr[cur_reg_delay_arr['rx'] == lane]
                    cur_hit_lane_arr = cur_hit_delay_arr[cur_hit_delay_arr['rx'] == lane]
                    reg_success_cnt = len(cur_reg_lane_arr[cur_reg_lane_arr['write'] == cur_reg_lane_arr['read']])
                    reg_out_array[mapping_dict[lane], delay_val] = reg_success_cnt
                    hit_success_cnt = len(cur_hit_lane_arr[cur_hit_lane_arr['in_pattern'] == cur_hit_lane_arr['out_pattern']])
                    hit_out_array[mapping_dict[lane], delay_val] = hit_success_cnt

            with plotting.Plotting(analyzed_data_file=self.output_filename + '.h5') as p:
                p.create_standard_plots()
                fig = Figure()
                FigureCanvas(fig)
                ax = fig.add_subplot(111)
                p._add_text(fig)

                x_label = "Phase Shift [ps]"
                y_label = "Succesful communication attempts"
                color_reg = ['r.', 'g.', 'b.', 'y.']
                color_hit = ['rx', 'gx', 'bx', 'yx']

                for cnt, lane in enumerate(hit_out_array):
                    x_val = np.array(range(len(lane))) * SHIFT_TO_PS
                    ax.plot(x_val, lane, color_hit[cnt])
                    fig2 = Figure()
                    FigureCanvas(fig2)
                    ax2 = fig2.add_subplot(111)
                    ax2.plot(x_val, lane, color_hit[cnt])
                    ax2.set_xlabel(x_label)
                    ax2.set_ylabel(y_label)
                    ax2.grid()
                    ax2.set_title('Data Merging Lane ' + str(cnt) + ' Hit Data vs. Phase Shift', color='#07529a')
                    p._add_text(fig2)
                    p._save_plots(fig2, suffix='data_merging_hit' + str(cnt))
                for cnt, lane in enumerate(reg_out_array):
                    x_val = np.array(range(len(lane))) * SHIFT_TO_PS
                    ax.plot(x_val, lane, color_reg[cnt])
                    fig2 = Figure()
                    FigureCanvas(fig2)
                    ax2 = fig2.add_subplot(111)
                    ax2.plot(x_val, lane, color_reg[cnt])
                    ax2.set_xlabel(x_label)
                    ax2.set_ylabel(y_label)
                    ax2.grid()
                    ax2.set_title('Data Merging Lane ' + str(cnt) + ' Register Data vs. Phase Shift', color='#07529a')
                    p._add_text(fig2)
                    p._save_plots(fig2, suffix='data_merging_reg' + str(cnt))
                ax.set_xlabel(x_label)
                ax.set_ylabel(y_label)
                ax.grid()
                ax.set_title(str('All Data Merging Lanes vs. Phase Shift to CMD'), color='#07529a')
                p._save_plots(fig, suffix='data_merging_all')


if __name__ == '__main__':
    with DataMergingTest(scan_config=scan_configuration) as test:
        test.start()
