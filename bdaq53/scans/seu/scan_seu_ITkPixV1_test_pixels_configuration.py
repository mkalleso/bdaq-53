#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test writes random data into the specified registers,
    then reads back the data and compares read and written data.
'''
import sys
import os

import random
import numpy as np
import time

import tables as tb

from bdaq53.scans.scan_pixel_registers import PixelRegisterScan
from bdaq53.analysis import rd53b_analysis


np.set_printoptions(threshold=np.inf)

DEBUG = False   # Use option -v or -V or --verbose or -verbose to activate
# Prints every tested register + associated value

pixels_config_itkpixv1 = np.empty((400, 384), dtype=int)
for i in range(400):
    for j in range(384):
        pixels_config_itkpixv1[i, j] = 85  # 8 bits: patern 01010101

local_configuration_itkpixv1 = {  # Scan parameters
    # 'maskfile': 'REF/ref_mask.h5',  # Use proper reference files after tuning
    # 'maskfile': None,  # Use None to find proper threshold
    'pixels_config': pixels_config_itkpixv1,
    'ignore': ['PIX_PORTAL', 'CdrConf',
               'GlobalPulseConf',
               'GlobalPulseWidth',
               'ServiceDataConf']
}


# UPDATE LOCAL CONFIG AS NEEDED

local_configuration = local_configuration_itkpixv1


class ValueTable(tb.IsDescription):
    register = tb.StringCol(64, pos=0)
    write = tb.UInt16Col(pos=1)
    read = tb.UInt16Col(pos=2)


class SEUTable(tb.IsDescription):
    pixel_row = tb.UInt16Col()
    pixel_col = tb.UInt16Col()
    written_value = tb.UInt16Col()
    read_value = tb.UInt16Col()


class PixelTest(PixelRegisterScan):
    scan_id = 'pixel_test'

    def process_userk_pixels(self, userk_in):

        userk_out = np.zeros(userk_in.shape[0] * 2, dtype={'names': ['Address', 'Data'],
                                                           'formats': ['uint16', 'uint16']})

        userk_counter = 0
        error_count = 0

        for i in userk_in:
            AuroraKWord = i['AuroraKWord']
            if AuroraKWord == 0:
                userk_out[userk_counter]['Address'] = i['Data1_Addr']
                userk_out[userk_counter]['Data'] = i['Data1']

                userk_out[userk_counter + 1]['Address'] = i['Data0_Addr']
                userk_out[userk_counter + 1]['Data'] = i['Data0']

                userk_counter = userk_counter + 2

            if AuroraKWord == 1:
                userk_out[userk_counter]['Address'] = i['Data1_Addr']
                userk_out[userk_counter]['Data'] = i['Data1']

                userk_counter = userk_counter + 1

            if AuroraKWord == 2:
                userk_out[userk_counter]['Address'] = i['Data0_Addr']
                userk_out[userk_counter]['Data'] = i['Data0']

                userk_counter = userk_counter + 1

            if userk_counter - 1 > 0 and not(i['Data1_Addr'] == 1 and i['Data0_Addr'] == 511) and not(i['Data1_Addr'] == 511 and i['Data0_Addr'] == 1):
                current_addr = userk_out[userk_counter - 1]['Address']
                prev_addr = userk_out[userk_counter - 2]['Address']

                if current_addr == prev_addr:
                    error_count = error_count + 1

        userk_out = userk_out[:userk_counter]

        for i in userk_out:
            self.log.debug('Address= %s \t\t\tData= %s ', hex(i['Address']), hex(i['Data']))

        return userk_out, error_count

    def _configure(self, **kwargs):

        for _ in self.iterate_chips():
            self.chip.enable_monitor_data()

    def _scan(self, ignore=[], **kwargs):
        '''
        Pixels test main loop

        Parameters
        ----------
        In kwargs:
            'rws' : array of int
            A list of rows to scan
            'cols' : array of int
            A list of columns to scan
        # FIXME : for now, only consecutive columns and rows can be tested. To change that, writing and reading matrix should always be full-size ones
        '''
        t1 = time.time()

        voltages, currents = self.chip.get_chip_status()
        text = ''

        for key, value in currents.items():
            text += '{0}: {1}A, '.format(key, value[1])
        for key, value in voltages.items():
            text += '{0}: {1}V, '.format(key, value[1])
        self.log.info(text[:-2])

        self.log.info('Starting scan...')

        # Adding the default global configuration at the beginning
        self.log.info('Starting scan... by writing default register values')

        for reg in self.chip.registers.values():
            if reg['mode'] != 1 or reg['reset'] == 0 or reg['name'] in ignore:
                continue
            value = int(reg['default'])
            # Writing the default register value
            reg.write(value)

        ################################################################################

        cols = self.chip.masks.dimensions[0]  # 400
        rows = self.chip.masks.dimensions[1]  # 384 192

        # creation of write group of pytables and array to store the written matrix
        written = self.h5_file.create_group(self.h5_file.root, 'written', 'Pixel written matrix')
        written_matrix_all = kwargs['pixels_config']
        self.h5_file.create_array(written, 'written_matrix', obj=written_matrix_all)

        if reponse == 2:
            for row in range(rows):
                for col in range(cols):
                    # Simulate a random error on a random pixel in the first column
                    err_bool = random.randrange(50)
                    if err_bool == 1:  # if random number = 1 then we simulate an error on a random bit among the 8.
                        bin_to_change = random.randrange(8)
                        written_matrix_all[col][row] ^= (1 << bin_to_change)  # toggle bit

        self.log.info('Writing {nbr_columns} columns of {rows} rows of pixels...'.format(nbr_columns=cols, rows=rows))
        self.write_pixels(written_matrix_all)
        self.log.success('Writing finished')

        # Uncomment to allow the operator to presse ENTER after a spill/trigger
        self.log.info('Press ENTER to simulate end of spill trigger')
        input()

        try:
            self.chip.init_communication(write_chip_reset=False)
        except RuntimeError:
            self.log.error("Communication initialization failed !!")

        self.log.info('Reading back {nbr_columns} columns of {rows} rows of pixels...'.format(nbr_columns=cols, rows=rows))
        self.read_pixels()
        self.log.info('Read operations sent. Waiting for readback data...')

        # now, raw data is pre-analyzed to verify that we get back the correct amount of values
        raw_data = self.h5_file.root.raw_data[:]
        # an error count is given : some values are read multiple times, so we expect as many more values (unique values)
        interpreted_userk = rd53b_analysis.interpret_userk_data(raw_data)
        userk_data, error_count = self.process_userk_pixels(interpreted_userk)
        nb_pixels = rows * cols / 2

        while len(userk_data) < nb_pixels + error_count:
            # repeat until correct number of values (including duplicates=errors) are returned
            raw_data = self.h5_file.root.raw_data[:]
            interpreted_userk = rd53b_analysis.interpret_userk_data(raw_data)
            userk_data, error_count = self.process_userk_pixels(interpreted_userk)

        if DEBUG:
            for element in userk_data:
                self.log.info('Element of readback : {element}'.format(element=element))
            self.log.info('Fetched {number} elements with {errors} redundancy (errors)'.format(number=len(userk_data), errors=error_count))

        # fetched data has to be stored in a matrix corresponding to read pixels
        test_data = np.zeros((cols, rows))

        for cnt, entries in enumerate(interpreted_userk):
            if (entries['Data1_Addr'] == 1 and entries['Data0_Addr'] == 511) and entries['Data1_Data'] < rows:
                ccol = entries['Data1_Data']
            elif (entries['Data1_Addr'] == 511 and entries['Data0_Addr'] == 1) and entries['Data0_Data'] < rows:
                ccol = entries['Data0_Data']
            else:
                if entries['Data1_Addr'] != 511:
                    test_data[ccol * 2, entries['Data1_Addr']] = entries['Data1_Data'] >> 8
                    test_data[ccol * 2 + 1, entries['Data1_Addr']] = entries['Data1_Data'] & 0xff
                if entries['Data0_Addr'] != 511:
                    test_data[ccol * 2, entries['Data0_Addr']] = entries['Data0_Data'] >> 8
                    test_data[ccol * 2 + 1, entries['Data0_Addr']] = entries['Data0_Data'] & 0xff

        self.log.success('All read data has been fetched')

        # store back the matrix
        self.h5_file.create_group(self.h5_file.root, 'read', 'Pixel read matrix')
        self.h5_file.create_array('/read', 'read_matrix', obj=test_data)

        # Checking for global registers at the end of the scan + filling h5 file
        if self.h5_file.__contains__('/Registers'):
            print("ok0")

        reg_table = self.h5_file.create_table(self.h5_file.root, name='Registers', title='Reg', description=ValueTable)
        self.log.info('Checking global registers...')
        for reg in self.chip.registers.values():
            if reg['mode'] != 1 or reg['reset'] == 0 or reg['name'] in ignore or reg['address'] in ignore:
                continue

            row = reg_table.row
            row['register'] = reg['name']
            row['write'] = reg['default']
            row['read'] = reg.read()
            row.append()
        #################################

        self.log.success('Scan finished')

        t2 = time.time()
        print(t2 - t1)

    def _analyze(self):

        columns = range(self.chip.masks.dimensions[0])  # 400
        rows = range(self.chip.masks.dimensions[1])  # 384 192

        self.log.info('Comparing data...')

        with tb.open_file(self.output_filename + '.h5', 'r') as in_file:

            read_matrix_all = in_file.root.read.read_matrix[:]  # complete read matrix for all spills
            read_matrix = read_matrix_all[columns[0]:columns[-1] + 1, rows[0]:rows[-1] + 1]  # read matrix for current spill, regarding current columns and rows

            if len(read_matrix) == 0:
                raise IOError('Received no data from the chip!')

            written_matrix_all = in_file.root.written.written_matrix[:]  # complete written matrix for all spills
            written_matrix = written_matrix_all[columns[0]:columns[-1] + 1, rows[0]:rows[-1] + 1]  # written matrix for current spill, regarding current columns and rows

            if DEBUG:
                self.log.info('Read complete matrix : {matrix}'.format(matrix=read_matrix))
                self.log.info('Written complete matrix : {matrix}'.format(matrix=written_matrix))

            seu_count = 0
            seu_list = []

            dim1, dim2 = written_matrix.shape
            if len(columns) != dim1 or len(rows) != dim2:
                self.log.error('Columns and rows to analyze do not match shape of written matrix. Terminating.')
                return False

            # finally comparing written and read values
            for column in columns:
                for row in rows:

                    if(written_matrix[column - columns[0]][row - rows[0]] != read_matrix[column - columns[0]][row - rows[0]]):

                        self.log.warning('SEU detected on pixel\tColumn {column}\tRow {row}\tWritten value : {written}\tRead value : {read}'.format(column=column, row=row, written=written_matrix[column - columns[0]][row - rows[0]], read=read_matrix[column - columns[0]][row - rows[0]]))

                        seu_count = seu_count + 1
                        new_seu = ((column, row), (written_matrix[column - columns[0]][row - rows[0]], read_matrix[column - columns[0]][row - rows[0]]))
                        seu_list.append(new_seu)

            if(seu_count == 0):
                self.log.success('No SEU detected!')
            else:
                self.log.info('{seu_count} SEU detected.'.format(seu_count=seu_count))

            # store detected SEUs in ultimate TXT file ###
            data_file = open(self.output_filename + '.txt', 'a+')

            for seu in seu_list:
                data_to_save = []
                data_to_save.append(str(seu[0][1]) + "\t" + str(seu[0][0]) + "\t" + str(seu[1][0]) + "\t" + str(seu[1][1]))
                np.savetxt(data_file, data_to_save, fmt="%s")

            if(seu_count != 0):
                self.log.info('Pixels with SEU saved in {SEUfile}'.format(SEUfile=self.output_filename + '.txt'))

        return True


if __name__ == '__main__':
    if len(sys.argv) > 1 and (sys.argv[1] == '-v' or sys.argv[1] == '-V' or sys.argv[1] == '-verbose' or sys.argv[1] == '--verbose'):
        DEBUG = False

    print('')
    print('Run decrease current routine ? (1 = yes, others = no)')
    answer = int(input('Yes (1) or No (0): '))
    if answer == 1:
        os.system('python scan_seu_ITkPixV1_decrease_current.py')

    with PixelTest(scan_config=local_configuration) as scan:

        while True:

            print('')
            print('                MENU')
            print('Test register without FAKE SEU ---------------- 1')
            print('Test register with FAKE SEU ------------------- 2')
            print('Leave the test ----------------- Other value')

            reponse = int(input('Your choice : '))

            if reponse == 1 or reponse == 2:
                if reponse == 2:
                    print("====> Launching scan with fake SEU")
                scan.start()
                os.system('mv output_data/module_0/chip_0/chip_0.log output_data/module_0/chip_0/' + scan.run_name + '.log')

            else:
                break
