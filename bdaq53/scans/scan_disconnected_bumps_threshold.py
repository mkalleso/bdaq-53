#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This scan identifies pixels with disconnected bump bonds by doing two threshold scans,
    one with reverse biased sensor and one with slightly forward biased sensor.
    A bump is assumed to be connected if the threshold and or the noise shift by a certain amount.
    The used cuts are hard-coded. They should be outside of the shift distributions of a bare chip.
    For cross checking, the shift distributions are plotted in the output PDF file.

    Note:
    To obtain correct results over the whole chip, all flavors have to be tuned to 650 DVCAL prior to running this scan,
    optimally without hight voltage (HV = 0V). The tuning must be done using the same custom register settings as used in this scan:
    'chip': {'registers': {'IBIASP1_SYNC': 38,
                           'IBIASP2_SYNC': 10,
                           'IBIAS_SF_SYNC': 45,
                           'PA_IN_BIAS_LIN': 30,
                           'PRMP_DIFF': 10
                           }
             }
'''

import numpy as np
import tables as tb

from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis import online as oa
from bdaq53.analysis import plotting

scan_configuration = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192,

    # Start threshold scan at injection setting where a fraction of min_response of the selected
    # pixels see the fraction of hits min_hits
    'min_response': 0.001,
    'min_hits': 0.05,
    # Stop threshold scan at injection setting where a fraction of max_response of the selected
    # pixels see the fraction of hits max_hits
    'max_response': 0.99,
    'max_hits': 0.99,

    'n_injections': 100,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 500,         # Start value of injection
    'VCAL_HIGH_stop': 2000,         # Maximum injection, can be None
    'VCAL_HIGH_step_fine': 25,      # Step size during threshold scan
    'VCAL_HIGH_step_coarse': 50,    # Step when seaching start

    'bias_voltage_forward': +1,     # Be careful!
    'bias_voltage_reverse': -40,
    'bias_current_limit': 10e-6
}


class BumpConnThrShScan(ScanBase):
    scan_id = 'bump_connection_bias_thr_shift_scan'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''
        self.data.module_name = self.module_settings['name']

        if not self.periphery.enabled:
            raise Exception('Periphery module needs to be enabled!')
        if not (self.data.module_name in list(self.periphery.module_devices.keys()) and 'HV' in self.periphery.module_devices[self.data.module_name].keys()):
            raise Exception('No sensor bias device defined for {}!'.format(self.data.module_name))

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()

        self.chip.masks.update(force=True)

        # Need to use modified register settings. Do not change.
        self.chip.registers['IBIASP1_SYNC'].write(38)
        self.chip.registers['IBIASP2_SYNC'].write(10)
        self.chip.registers['IBIAS_SF_SYNC'].write(45)
        self.chip.registers['PA_IN_BIAS_LIN'].write(30)
        self.chip.registers['PRMP_DIFF'].write(10)

        self.data.hist_occ = oa.OccupancyHistogramming(chip_type=self.chip.chip_type.lower(), rx_id=int(self.chip.receiver[-1]))

    def _scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192,
              n_injections=200, VCAL_MED=500, VCAL_HIGH_start=500, VCAL_HIGH_stop=2000, VCAL_HIGH_step_fine=10, VCAL_HIGH_step_coarse=50,
              min_response=0.01, max_response=0.99, min_hits=0.2, max_hits=1.,
              bias_voltage_forward=+0.0, bias_voltage_reverse=-20, bias_current_limit=1e-6, **_):
        '''
        Threshold scan main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.

        min_response: float 0.01,
            Minimal needed fraction of pixels seeing 'min_hits' before starting scan
        min_hits: float 0.1,
            Minimal fraction of hits needed to be seen by 'min_response' fraction of pixels, before starting scan
        max_response: float 0.99,
            Scan stops at this fraction of pixels seeing 'max_hits'
        max_hits: float 1.0,
            Minimal fraction of hits needed to be seen by 'max_response' fraction of pixels before stopping scan

        n_injections : int
            Number of injections.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            First VCAL_HIGH value to scan.
        VCAL_HIGH_stop : int
            VCAL_HIGH value to stop the scan. This value is excluded from the scan.
        VCAL_HIGH_step_fine : int
            VCAL_HIGH interval.
        VCAL_HIGH_step_coarse : int
            VCAL_HIGH interval for searching injection start value.

        bias_voltage_reverse: int
            Reverse bias voltage
        bias_voltage_forward: int
            Forward bias voltage
        bias_current_limit: float
            Current limit for sensor bias device
        '''

        biases = [bias_voltage_reverse, bias_voltage_forward]

        # Calculated max pixels that can respond
        sel_pix = self.chip.masks['enable'][start_column:stop_column, start_row:stop_row]
        n_sel_pix = np.count_nonzero(sel_pix)

        self.periphery.power_off_HV(self.data.module_name)
        try:
            scan_param_id = -1                      # Do not store data before S-curve sampling

            for bias_id, bias in enumerate(biases):
                self.periphery.power_on_HV(self.data.module_name, hv_voltage=0, hv_current_limit=bias_current_limit)
                self.periphery.power_on_HV(self.data.module_name, hv_voltage=bias, hv_current_limit=bias_current_limit)

                self.data.start_data_taking = False     # Indicates if threshold scan has already started
                vcal_high = VCAL_HIGH_start             # Start value

                pbar = None

                self.log.info('Search for maximum response...')

                self.chip.setup_analog_injection(vcal_high=VCAL_HIGH_stop, vcal_med=VCAL_MED)
                with self.readout(scan_param_id=scan_param_id, callback=self.analyze_data_online):
                    for fe, _ in self.chip.masks.shift(masks=['enable', 'injection'], cache=True):
                        if not fe == 'skipped' and fe == 'SYNC':
                            self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections, latency=122, wait_cycles=400)
                        elif not fe == 'skipped':
                            self.chip.inject_analog_single(repetitions=n_injections, latency=122, wait_cycles=400)

                occupancy = self.data.hist_occ.get()    # Interpret raw data and create occupancy histogram
                max_n_pix = np.count_nonzero(occupancy >= n_injections * max_hits)

                self.log.info('Maximum reponse is {0} of {1} pixels ({2:1.2f}%)'.format(max_n_pix, n_sel_pix, float(max_n_pix) / n_sel_pix * 100.))

                self.log.info('Search for best start parameter...')

                while vcal_high < VCAL_HIGH_stop:
                    if not self.data.start_data_taking:
                        self.log.info('Try Delta VCAL = {0}...'.format(vcal_high - VCAL_MED))

                    self.chip.setup_analog_injection(vcal_high=vcal_high, vcal_med=VCAL_MED)
                    with self.readout(scan_param_id=scan_param_id, callback=self.analyze_data_online):
                        for fe, _ in self.chip.masks.shift(masks=['enable', 'injection'], cache=True):
                            if not fe == 'skipped' and fe == 'SYNC':
                                self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections, latency=122, wait_cycles=400)
                            elif not fe == 'skipped':
                                self.chip.inject_analog_single(repetitions=n_injections, latency=122, wait_cycles=400)
                        if self.data.start_data_taking:
                            self.store_scan_par_values(scan_param_id=scan_param_id, vcal_high=vcal_high, vcal_med=VCAL_MED, bias_id=bias_id)

                    occupancy = self.data.hist_occ.get()    # Interpret raw data and create occupancy histogram
                    if not self.data.start_data_taking:     # Log responding pixels
                        n_pix = np.count_nonzero(occupancy >= n_injections * min_hits)
                        # self.log.info('{0} of {1} pixels ({2:1.2f}%) see {3}% of injections'.format(n_pix, n_sel_pix, float(n_pix) / n_sel_pix * 100., int(min_hits * 100.)))
                    else:
                        n_pix = np.count_nonzero(occupancy >= n_injections * max_hits)
                        # self.log.info('{0} of {1} pixels ({2:1.2f}%) see {3}% of injections'.format(n_pix, n_sel_pix, float(n_pix) / n_sel_pix * 100., int(max_hits * 100.)))

                    if self.data.start_data_taking and n_pix >= max_n_pix:
                        pbar.update(n_sel_pix - pbar.n)
                        pbar.close()
                        self.log.info('S-curve fully sampled. Stop sampling.')
                        break

                    if pbar is not None:
                        try:
                            pbar.update(n_pix - pbar.n)
                        except ValueError:
                            pass

                    if np.count_nonzero(occupancy >= n_injections * max_hits) >= n_sel_pix * max_response:  # Check for stop condition: number of pixels that see all hits
                        if not self.data.start_data_taking:     # Overlooked the start condition
                            self.log.info('Start sampling S-curves for {0} enabled pixels...'.format(n_sel_pix))
                            self.data.start_data_taking = True
                            pbar = tqdm(total=n_sel_pix, unit=' Pixels')
                            scan_param_id += 1
                            # Go back two steps
                            if vcal_high - 2 * VCAL_HIGH_step_coarse + VCAL_HIGH_step_fine > VCAL_HIGH_start:
                                vcal_high = vcal_high - 2 * VCAL_HIGH_step_coarse + VCAL_HIGH_step_fine
                            else:
                                vcal_high = VCAL_HIGH_start
                            continue
                        else:
                            pbar.update(n_sel_pix - pbar.n)
                            pbar.close()
                            self.log.info('S-curve fully sampled. Stop sampling.')
                            break

                    if not self.data.start_data_taking:
                        # Check start condition: number of pixels with hits
                        if np.count_nonzero(occupancy >= n_injections * min_hits) >= float(n_sel_pix * min_response):
                            self.log.info('Start sampling S-curves for {0} enabled pixels...'.format(n_sel_pix))
                            self.data.start_data_taking = True
                            pbar = tqdm(total=n_sel_pix, unit=' Pixels')
                            scan_param_id += 1
                            # Go back one step
                            if vcal_high - VCAL_HIGH_step_coarse + VCAL_HIGH_step_fine > VCAL_HIGH_start:
                                vcal_high = vcal_high - VCAL_HIGH_step_coarse + VCAL_HIGH_step_fine
                            else:
                                vcal_high = VCAL_HIGH_start
                        else:
                            vcal_high += VCAL_HIGH_step_coarse
                    else:  # already scanning, just increase scan pars and value
                        scan_param_id += 1
                        vcal_high += VCAL_HIGH_step_fine
                # Maximum charge reached and no abort triggered (break of while loop)
                else:
                    scan_param_id -= 1
                    self.log.warning('Maximum injection reached. Abort.')

        except Exception as e:
            self.log.error('An error occurred: %s' % e)
        finally:
            self.periphery.power_off_HV(self.data.module_name)

        # Stop analysis process
        self.data.hist_occ.close()

        self.log.success('Scan finished')

    def analyze_data_online(self, data_tuple, receiver=None):
        raw_data = data_tuple[0]
        self.data.hist_occ.add(raw_data)
        # Only store s-curve data when scan started
        if self.data.start_data_taking:
            super(BumpConnThrShScan, self).handle_data(data_tuple, receiver)

    def _analyze(self):

        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', store_hits=False) as a:
            a.analyze_data()
            scan_params = a.get_scan_param_values()

        with tb.open_file(self.output_filename + '_interpreted.h5', 'r+') as in_file:
            run_config = au.ConfigDict(in_file.root.configuration_in.scan.scan_config[:])

            start_column, stop_column, start_row, stop_row = run_config['start_column'], run_config['stop_column'], run_config['start_row'], run_config['stop_row']
            enable_mask = np.zeros(shape=(400, 192), dtype=bool)
            enable_mask[start_column:stop_column, start_row:stop_row] = True
            disable_mask = in_file.root.configuration_in.chip.use_pixel[:]
            enabled_pixels = np.logical_and(enable_mask, disable_mask)

            bias_ids = scan_params['bias_id']

            hist_occ_raw = in_file.root.HistOcc[:]
            hist_occ_reverse = hist_occ_raw[:, :, bias_ids == 0]
            hist_occ_forward = hist_occ_raw[:, :, bias_ids == 1]

            scan_params_raw = scan_params['vcal_high'] - scan_params['vcal_med']
            scan_params_reverse = scan_params_raw[scan_params['bias_id'] == 0]
            scan_params_forward = scan_params_raw[scan_params['bias_id'] == 1]

            threshold_map_reverse, noise_map_reverse, chi2_map_reverse = au.fit_scurves_multithread(hist_occ_reverse.reshape((192 * 400, -1)), scan_params_reverse, run_config['n_injections'], optimize_fit_range=False)
            threshold_map_forward, noise_map_forward, chi2_map_forward = au.fit_scurves_multithread(hist_occ_forward.reshape((192 * 400, -1)), scan_params_forward, run_config['n_injections'], optimize_fit_range=False)

            in_file.create_carray(in_file.root, name='ThresholdMapReverse', title='Reverse Biased Threshold Map',
                                  obj=threshold_map_reverse,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='NoiseMapReverse', title='Reverse Biased Noise Map',
                                  obj=noise_map_reverse,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='Chi2MapReverse', title='Reverse Biased Chi2 / ndf Map',
                                  obj=chi2_map_reverse,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            in_file.create_carray(in_file.root, name='ThresholdMapForward', title='Forward Biased Threshold Map',
                                  obj=threshold_map_forward,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='NoiseMapForward', title='Reverse Forward Noise Map',
                                  obj=noise_map_forward,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='Chi2MapForward', title='Forward Biased Chi2 / ndf Map',
                                  obj=chi2_map_forward,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            # Calculate shift of theshold and noise with bias and identify disconnected bumps

            threshold_shift_map = threshold_map_forward - threshold_map_reverse
            noise_shift_map = noise_map_forward - noise_map_reverse

            # Cut values determined from shift distribution plots for a bare chip
            thr_cut_SYNC_low = run_config.get("thr_cut_SYNC_low", -25)
            thr_cut_SYNC_high = run_config.get("thr_cut_SYNC_high", 30)
            thr_cut_LIN_low = run_config.get("thr_cut_LIN_low", -50)
            thr_cut_LIN_high = run_config.get("thr_cut_LIN_high", 60)
            thr_cut_DIFF_low = run_config.get("thr_cut_DIFF_low", -150)
            thr_cut_DIFF_high = run_config.get("thr_cut_DIFF_high", 80)
            noise_cut_SYNC_low = run_config.get("noise_cut_SYNC_low", -40)
            noise_cut_SYNC_high = run_config.get("noise_cut_SYNC_high", 40)
            noise_cut_LIN_low = run_config.get("noise_cut_LIN_low", -25)
            noise_cut_LIN_high = run_config.get("noise_cut_LIN_high", 25)
            noise_cut_DIFF_low = run_config.get("noise_cut_DIFF_low", -200)
            noise_cut_DIFF_high = run_config.get("noise_cut_DIFF_high", 200)

            connected_bumps_map = np.zeros(shape=(400, 192), dtype=bool)
            connected_bumps_map[0:128, :] = np.logical_or(np.logical_or(threshold_shift_map[0:128, :] < thr_cut_SYNC_low,
                                                                        threshold_shift_map[0:128, :] > thr_cut_SYNC_high),
                                                          np.logical_or(noise_shift_map[0:128, :] < noise_cut_SYNC_low,
                                                                        noise_shift_map[0:128, :] > noise_cut_SYNC_high))
            connected_bumps_map[128:264, :] = np.logical_or(np.logical_or(threshold_shift_map[128:264, :] < thr_cut_LIN_low,
                                                                          threshold_shift_map[128:264, :] > thr_cut_LIN_high),
                                                            np.logical_or(noise_shift_map[128:264, :] < noise_cut_LIN_low,
                                                                          noise_shift_map[128:264, :] > noise_cut_LIN_high))
            connected_bumps_map[264:400, :] = np.logical_or(np.logical_or(threshold_shift_map[264:400, :] < thr_cut_DIFF_low,
                                                                          threshold_shift_map[264:400, :] > thr_cut_DIFF_high),
                                                            np.logical_or(noise_shift_map[264:400, :] < noise_cut_DIFF_low,
                                                                          noise_shift_map[264:400, :] > noise_cut_DIFF_high))

            max_VCAL = run_config["VCAL_HIGH_stop"] - run_config["VCAL_MED"]
            failed_pixels_r = np.logical_or(threshold_map_reverse == 0, threshold_map_reverse > max_VCAL * 2)
            failed_pixels_f = np.logical_or(threshold_map_forward == 0, threshold_map_forward > max_VCAL * 2)

            failed_connected = np.logical_and(failed_pixels_f, np.logical_not(failed_pixels_r))
            failed_unknown = failed_pixels_r

            connected_bumps_map = np.logical_or(connected_bumps_map, failed_connected)
            num_non_enabled_pixels = np.count_nonzero(np.invert(enabled_pixels))
            enabled_pixels = np.logical_and(enabled_pixels, np.invert(failed_unknown))
            num_connected_bumps = np.count_nonzero(connected_bumps_map[enabled_pixels])
            num_disconnected_bumps = np.count_nonzero(np.invert(connected_bumps_map[enabled_pixels]))
            num_unknown_bumps = np.count_nonzero(failed_unknown)

            self.log.info('Found {0} connected pixels'.format(num_connected_bumps))
            self.log.info('Found {0} disconnected pixels'.format(num_disconnected_bumps))
            self.log.info('Ignored {0} non-enabled pixels'.format(num_non_enabled_pixels))
            self.log.info('Failed for {0} pixels'.format(num_unknown_bumps))

            # Create array for bump connectivity plots
            hist_bump_bonds_connection = np.full(shape=connected_bumps_map.shape, fill_value=-12.)
            hist_bump_bonds_connection[np.logical_and(connected_bumps_map, enabled_pixels)] = 1
            hist_bump_bonds_connection[np.logical_and(np.invert(connected_bumps_map), enabled_pixels)] = 0

            ConnectionStatistics = {
                "connected_bumps": num_connected_bumps,
                "disconnected_bumps": num_disconnected_bumps,
                "non_enabled_pixels": num_non_enabled_pixels,
                "unknown_state_bumps": num_unknown_bumps,
            }

            cuts = {
                "thr_cut_SYNC_low": thr_cut_SYNC_low,
                "thr_cut_SYNC_high": thr_cut_SYNC_high,
                "thr_cut_LIN_low": thr_cut_LIN_low,
                "thr_cut_LIN_high": thr_cut_LIN_high,
                "thr_cut_DIFF_low": thr_cut_DIFF_low,
                "thr_cut_DIFF_high": thr_cut_DIFF_high,
                "noise_cut_SYNC_low": noise_cut_SYNC_low,
                "noise_cut_SYNC_high": noise_cut_SYNC_high,
                "noise_cut_LIN_low": noise_cut_LIN_low,
                "noise_cut_LIN_high": noise_cut_LIN_high,
                "noise_cut_DIFF_low": noise_cut_DIFF_low,
                "noise_cut_DIFF_high": noise_cut_DIFF_high,
            }

            in_file.create_carray(in_file.root, name='BumpConnectivityMap', title='Bump Connectivity', obj=hist_bump_bonds_connection,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            in_file.create_carray(in_file.root, name='ThresholdShiftMap', title='Threshold shift', obj=threshold_shift_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            in_file.create_carray(in_file.root, name='NoiseShiftMap', title='Noise shift', obj=noise_shift_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            in_file.create_table(in_file.root, name='BumpConnectivityStatistics', title='Bump Connections',
                                 obj=np.array([(key, ConnectionStatistics[key]) for key in ConnectionStatistics.keys()], dtype=[('attribute', 'S64'), ('value', 'i4')]))

            in_file.create_table(in_file.root, name='Cuts', title='Cuts',
                                 obj=np.array([(key, cuts[key]) for key in cuts.keys()], dtype=[('attribute', 'S64'), ('value', 'f4')]))

        compare_filename = run_config.get("compare_filename", None)
        if compare_filename is not None:
            with tb.open_file(compare_filename + '.h5', 'r') as in_file:
                bump_bonds_connection_map = in_file.root.BumpConnectivityMap[:]
                bump_bonds_connection_map[bump_bonds_connection_map == .99] = 1
                cbar_label = "source scan"
        else:
            bump_bonds_connection_map = None

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=self.output_filename + '_interpreted.h5', save_png=False) as p:
                if bump_bonds_connection_map is not None:
                    p.connectivity_map_to_compare = bump_bonds_connection_map
                    p.compared_connectivity_map_title = cbar_label

                p.create_standard_plots()


if __name__ == '__main__':
    with BumpConnThrShScan(scan_config=scan_configuration) as scan:
        scan.start()
