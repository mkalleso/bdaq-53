#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script does a threshold scan and only uses hits that occur at
    the same BCID (in-time threshold scan). To optimize the time point of
    charge injection and to compensate for the pixel position dependent
    injection delay, a fine_delay map should be provided for RD53A. For
    ITKPixV1, the fine_delay has been hard-coded.
'''

import numba
from tqdm import tqdm
import numpy as np
import tables as tb
from shutil import copyfile
import os

from bdaq53.system.scan_base import ScanBase
from bdaq53.chips.shift_and_inject import shift_and_inject, get_scan_loop_mask_steps
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting
from bdaq53.chips import rd53a
from bdaq53.chips import ITkPixV1 as itkpixv1
import bdaq53.analysis.analysis_utils as au

scan_configuration = {
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 700,
    'VCAL_HIGH_stop': 1200,
    'VCAL_HIGH_step': 20,

    'fine_delay': 21
}


class InTimeThrScan(ScanBase):
    scan_id = 'in_time_threshold_scan'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()

        self.chip.masks.update(force=True)

    def _scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, n_injections=100, VCAL_MED=500, VCAL_HIGH_start=1000, VCAL_HIGH_stop=4000, VCAL_HIGH_step=100, fine_delay=21, **_):
        '''
        In Time Threshold scan main loop

        Parameters
        ----------
        n_injections : int
            Number of injections.
        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            First VCAL_HIGH value to scan.
        VCAL_HIGH_stop : int
            VCAL_HIGH value to stop the scan. This value is excluded from the scan.
        VCAL_HIGH_step : int
            VCAL_HIGH interval.
        '''

        vcal_high_range = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)

        delay_map = self.chip.masks['injection_delay'][:]
        finedelay_range = np.unique(delay_map[start_column:stop_column] % 16)
        if self.chip.chip_type == 'ITKPixV1':
            finedelay_range = [fine_delay]
            delay_map = np.zeros_like(delay_map)
        self.h5_file.create_carray(self.h5_file.root.configuration_in, name='delay_mask', title='Fine Delay Mask',
                                   obj=delay_map,
                                   filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        pbar = tqdm(total=get_scan_loop_mask_steps(scan=self) * len(vcal_high_range) * len(finedelay_range), unit=' Mask steps')
        for scan_param_id, vcal_high in enumerate(vcal_high_range):
            for delay in finedelay_range:
                self.chip.setup_analog_injection(vcal_high=vcal_high, vcal_med=VCAL_MED, fine_delay=delay)
                self.store_scan_par_values(scan_param_id=scan_param_id, delay_id=scan_param_id * 16 + int(delay))
                with self.readout(scan_param_id=scan_param_id):
                    shift_and_inject(scan=self, n_injections=n_injections, pbar=pbar, scan_param_id=scan_param_id, cache=True)

        pbar.close()
        self.log.success('Scan finished')

    def _analyze(self):
        self.configuration['bench']['analysis']['store_hits'] = True
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()
            chunk_size = a.chunk_size
        with tb.open_file(self.output_filename + '_interpreted.h5', 'r+') as in_file:
            hist_rel_bcid_orig = in_file.root.HistRelBCID[:]
            hist_occ_orig = in_file.root.HistOcc[:]
            hist_tot_orig = in_file.root.HistTot[:]
            in_file.root.HistRelBCID._f_remove()
            in_file.root.HistOcc._f_remove()
            in_file.root.HistTot._f_remove()
            scan_config = au.ConfigDict(in_file.root.configuration_in.scan.scan_config[:])
            chip_config = au.ConfigDict(in_file.root.configuration_in.chip.settings[:])
            scan_param_id_table = np.array(a.get_scan_param_values(scan_parameter='delay_id'), dtype=int)
            scan_range = [v - scan_config['VCAL_MED'] for v in range(scan_config['VCAL_HIGH_start'],
                                                                     scan_config['VCAL_HIGH_stop'],
                                                                     scan_config['VCAL_HIGH_step'])]

            # Determines the chip, the front end which is active and sets the correct bcid cutting value
            if chip_config['chip_type'].lower() == 'rd53a':
                rows = 192
                delay_map = in_file.root.configuration_in.chip.masks.injection_delay[:]
                tot = np.zeros(shape=(400, rows, len(scan_range), 16, 16), dtype=np.uint8)
                bcid = np.zeros(shape=(400, rows, len(scan_range), 32, 16), dtype=np.uint8)
                hist_occ_in_time = np.zeros(shape=(400, 192, len(scan_range)), dtype=np.uint8)
                for i in tqdm(range(0, in_file.root.Hits.shape[0], chunk_size)):
                    hits = in_file.root.Hits[i:i + chunk_size]
                    bcid, tot = finedelay_select(bcid, tot, hits, scan_param_id_table)
            elif chip_config['chip_type'].lower() == 'itkpixv1':
                rows = 384
                # Delay of 303 for ITKPixV1
                delay_map = in_file.root.configuration_in.chip.masks.injection_delay[:] - 48
                tot = hist_tot_orig
                bcid = hist_rel_bcid_orig
                hist_occ_in_time = hist_occ_orig
            else:
                pass


            in_file.create_carray(in_file.root, name='HistTot_all', title='ToT Histogram for all fine delays',
                                  obj=tot,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='HistRelBCID_all',
                                  title='Relative BCID Histogram for all fine delays',
                                  obj=bcid,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.root.Hits._f_remove()

            bcid_hist = hist_rel_bcid_orig.sum(axis=(0, 1, 2))
            rel_bcid = np.where(bcid_hist == np.max(bcid_hist))[0]

            if chip_config['chip_type'].lower() == 'rd53a':
                shrink_bcid = np.zeros(bcid.shape[:-1], dtype=np.uint8)
                fine_bcid = choose_fine_delay_map(delay_map, bcid, shrink_bcid, rows)
                shrink_tot = np.zeros(tot.shape[:-1], dtype=np.uint8)
                fine_tot = choose_fine_delay_map(delay_map, tot, shrink_tot, rows)
                hist_occ = np.array(np.sum(fine_bcid, axis=3), np.uint8)
                hist_occ_in_time = choose_bcid_map(delay_map, fine_bcid, hist_occ_in_time, rows)
            elif chip_config['chip_type'].lower() == 'itkpixv1':
                fine_bcid = bcid
                fine_tot = tot
                hist_occ = np.array(np.sum(fine_bcid, axis=3), np.uint8)
                hist_occ_in_time = hist_rel_bcid_orig[:, :, :, rel_bcid]

            fine_bcid_cut = np.zeros(fine_bcid.shape, dtype=np.uint8)
            fine_bcid_cut[:, :, :, rel_bcid] = fine_bcid[:, :, :, rel_bcid]

            in_file.create_carray(in_file.root, name='HistOcc',
                                  title='Occupancy Histogram',
                                  obj=hist_occ,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            in_file.create_carray(in_file.root, name='InTimeHistOcc',
                                  title='Occupancy Histogram',
                                  obj=hist_occ_in_time,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            threshold_map, noise_map, chi2_map = au.fit_scurves_multithread(hist_occ.reshape((rows * 400, -1)),
                                                                            scan_range,
                                                                            scan_config['n_injections'],
                                                                            optimize_fit_range=False, rows=rows)
            in_t_threshold_map, in_t_noise_map, in_t_chi2_map = au.fit_scurves_multithread(hist_occ_in_time.reshape((rows * 400, -1)),
                                                                                           scan_range,
                                                                                           scan_config['n_injections'],
                                                                                           optimize_fit_range=False, rows=rows)

            in_file.create_carray(in_file.root, name='ThresholdMap', title='Threshold Map',
                                  obj=threshold_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='NoiseMap', title='Noise Map', obj=noise_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='Chi2Map', title='Chi2 / ndf Map',
                                  obj=chi2_map, filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='HistTot', title='ToT Histogram',
                                  obj=fine_tot,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='HistRelBCID',
                                  title='Relativ BCID Histogram',
                                  obj=fine_bcid,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='InTimeHistRelBCID',
                                  title='In Time Relativ BCID Histogram',
                                  obj=fine_bcid_cut,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            in_file.create_carray(in_file.root, name='InTimeThresholdMap', title='Threshold Map',
                                  obj=in_t_threshold_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='InTimeNoiseMap', title='Noise Map', obj=in_t_noise_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='InTimeChi2Map', title='Chi2 / ndf Map',
                                  obj=in_t_chi2_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        os.rename(self.output_filename + '_interpreted.h5', self.output_filename + '_inter.h5')
        copyfile(self.output_filename + '_inter.h5', self.output_filename + '_interpreted.h5')
        os.remove(self.output_filename + '_inter.h5')

        # Calculate overdrive
        try:
            overdrive = np.median(in_t_threshold_map[in_t_threshold_map > 0]) - np.median(threshold_map[threshold_map > 0])
            self.log.success('The estimated overdrive is {0} [Delta VCAL]'.format(int(overdrive)))
        except ValueError:
            overdrive = 0
            self.log.error('Overdrive estimation failed')

        self.plot_standard_plots()
        return overdrive

    def plot_standard_plots(self):
        with tb.open_file(self.output_filename + '_interpreted.h5', 'r') as in_file_h5:
            delay_map = in_file_h5.root.configuration_in.chip.masks.injection_delay[:]
            in_t_hist_occ = in_file_h5.root.InTimeHistOcc[:]
            in_t_threshold_map = in_file_h5.root.InTimeThresholdMap[:]
            in_t_noise_map = in_file_h5.root.InTimeNoiseMap[:]
            fine_bcid = in_file_h5.root.InTimeHistRelBCID[:]
            threshold_map = in_file_h5.root.ThresholdMap[:]
            overdrive = in_t_threshold_map - threshold_map
            scan_config = au.ConfigDict(in_file_h5.root.configuration_in.scan.scan_config[:])
            chip_config = au.ConfigDict(in_file_h5.root.configuration_in.chip.settings[:])

        with plotting.Plotting(analyzed_data_file=self.output_filename + '_interpreted.h5', save_png=False) as p:
            p.create_standard_plots()
            p._plot_occupancy(hist=np.ma.masked_array(delay_map, p.enable_mask).T,
                              electron_axis=False,
                              z_label='Injection Delay [LSB]',
                              title='Injection Delay Map',
                              use_electron_offset=False,
                              show_sum=False,
                              z_min=np.min(np.ma.masked_array(delay_map, p.enable_mask)),
                              z_max=np.max(np.ma.masked_array(delay_map, p.enable_mask)),
                              suffix='injection_delay_map')

            p._plot_distribution(np.ma.masked_array(delay_map, p.enable_mask).T,
                                 plot_range=np.arange(8 * 16, 12 * 16),
                                 electron_axis=False,
                                 x_axis_title='Injection Delay [LSB]',
                                 title='Injection Delay Distribution of Enabled Pixels',
                                 print_failed_fits=False,
                                 suffix='delay_distribution')

            scan_parameter_name = '$\\Delta$ VCAL'
            electron_axis = True
            scan_parameter_range = [v - p.scan_config['VCAL_MED'] for v in
                                    range(p.scan_config['VCAL_HIGH_start'],
                                          p.scan_config['VCAL_HIGH_stop'] + 1,
                                          p.scan_config['VCAL_HIGH_step'])]

            if chip_config['chip_type'].lower() == 'itkpixv1':
                flavor = 'itkpixv1'
                rows = 384
                min_tdac, max_tdac, range_tdac, _ = itkpixv1.get_tdac_range(flavor)
            else:
                flavor = rd53a.get_flavor(scan_config['stop_column'] - 1)
                rows = 192
                min_tdac, max_tdac, range_tdac, _ = rd53a.get_tdac_range(flavor)

            p._plot_scurves(scurves=in_t_hist_occ.reshape((rows * 400, -1)).T,
                            scan_parameters=scan_parameter_range,
                            electron_axis=electron_axis,
                            scan_parameter_name=scan_parameter_name,
                            suffix='in_time_scurves')
            p._plot_distribution(in_t_threshold_map[in_t_threshold_map != 0 & ~p.enable_mask].T,
                                 plot_range=scan_parameter_range,
                                 electron_axis=electron_axis,
                                 x_axis_title=scan_parameter_name,
                                 title='In Time Threshold distribution for enabled pixels',
                                 print_failed_fits=True,
                                 suffix='in_t_threshold_distribution')
            p._plot_relative_bcid(hist=fine_bcid.sum(axis=(0, 1, 2)).T)
            p._plot_stacked_threshold(data=in_t_threshold_map[in_t_threshold_map != 0 & ~p.enable_mask].T,
                                      tdac_mask=p.tdac_mask[in_t_threshold_map != 0 & ~p.enable_mask].T,
                                      plot_range=scan_parameter_range,
                                      electron_axis=electron_axis,
                                      x_axis_title=scan_parameter_name,
                                      title='In Time Threshold distribution for enabled pixels',
                                      suffix='in_t_tdac_threshold_distribution',
                                      min_tdac=min(min_tdac, max_tdac),
                                      max_tdac=max(min_tdac, max_tdac),
                                      range_tdac=range_tdac)
            p._plot_occupancy(hist=np.ma.masked_array(in_t_threshold_map, p.enable_mask).T,
                              electron_axis=True,
                              z_label='In Time Threshold',
                              title='In Time Threshold',
                              use_electron_offset=True,
                              show_sum=False,
                              z_min=None,
                              z_max=None,
                              suffix='threshold_map')
            p._plot_distribution(overdrive[overdrive > 0 & ~p.enable_mask].T,
                                 plot_range=None,
                                 electron_axis=None,
                                 x_axis_title=scan_parameter_name,
                                 title='Overdrive distribution for enabled pixels',
                                 print_failed_fits=True,
                                 suffix='overdrive_distribution')
            p._plot_occupancy(hist=np.ma.masked_array(in_t_threshold_map - threshold_map, p.enable_mask).T,
                              electron_axis=True,
                              z_label='Overdrive',
                              title='Overdrive',
                              use_electron_offset=True,
                              show_sum=False,
                              z_min=None,
                              z_max=None,
                              suffix='overdrive_map')
            plot_range = None
            if in_t_noise_map[in_t_noise_map != 0 & ~p.enable_mask].T.shape[0] == 0:
                plot_range = [0, 10]

            p._plot_distribution(in_t_noise_map[in_t_noise_map != 0 & ~p.enable_mask].T,
                                 title='Noise distribution for enabled pixels',
                                 plot_range=plot_range,
                                 electron_axis=electron_axis,
                                 use_electron_offset=False,
                                 x_axis_title=scan_parameter_name,
                                 y_axis_title='# of hits',
                                 print_failed_fits=True,
                                 suffix='noise_distribution')

            p._plot_occupancy(hist=np.ma.masked_array(in_t_noise_map, p.enable_mask).T,
                              electron_axis=False,
                              use_electron_offset=False,
                              z_label='In Time Noise',
                              z_max='median',
                              title='In Time Noise',
                              show_sum=False,
                              suffix='noise_map')


@numba.njit
def finedelay_select(bcid, tot, hits_raw, scan_param_id_table):
    for entries in hits_raw:
        bcid[entries['col'], entries['row'], scan_param_id_table[entries['scan_param_id']] // 16, entries['rel_bcid'], scan_param_id_table[entries['scan_param_id']] % 16] += 1
        tot[entries['col'], entries['row'], scan_param_id_table[entries['scan_param_id']] // 16, entries['tot'], scan_param_id_table[entries['scan_param_id']] % 16] += 1
    return bcid, tot


@numba.njit
def choose_fine_delay_map(finedelay_map, shrink_array, shrinked_array, rows):
    for col in range(400):
        for row in range(rows):
            shrinked_array[col, row, :, :] = shrink_array[col, row, :, :, finedelay_map[col, row] % 16]
    return shrinked_array


@numba.njit
def choose_bcid_map(bcid_map, shrink_array, shrinked_array, rows):
    for col in range(400):
        for row in range(rows):
            shrinked_array[col, row, :] = shrink_array[col, row, :, 31 - bcid_map[col, row] // 16]
    return shrinked_array


if __name__ == '__main__':
    with InTimeThrScan(scan_config=scan_configuration) as scan:
        # scan.output_filename = '/faust/user/mstandke/RD53_env/RD53B_SIM/bdaq53/bdaq53/scans/output_data/module_0/chip_0/20220614_132357_in_time_threshold_scan'
        # scan.analyze()
        scan.start()
