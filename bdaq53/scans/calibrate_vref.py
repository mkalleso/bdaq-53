#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This calibration routine calibrates the VREF A and D so VDDA and VDDD are as close as possible to 1.2V.
    Please make sure you have a Multimeter or Sourcemeter connected to your PC and activated the periphery module in testbench.yaml.

    Power the chip in LDO mode.

    Probe_locations:
        direct: Connect the ground lead of the multimeter to the ground point on the SCC: Use Pin 8 of the SLDO_PLL_MON header.
                First, connect the positive lead of the multimeter to VDDA on the PWR A header. Once the script tells you to,
                connect the positive lead to VDDD on the PWR D header.

        lemo:   Connect your Multimeter to the IMUX Lemo on the single chip card.
'''

import time
from tqdm import tqdm
import numpy as np
import tables as tb

from bdaq53.system.scan_base import ScanBase

scan_configuration = {
    'samples': 1,
    'periphery_device': 'Sourcemeter',
    'probe_location': 'lemo'
}


class MultimeterTable(tb.IsDescription):
    voltage = tb.StringCol(128, pos=0)
    trimbit_setting = tb.Int16Col(pos=1)
    sample = tb.Int16Col(pos=2)
    value = tb.Float64Col(pos=3)


class VREFCalib(ScanBase):
    scan_id = 'calibrate_vref'

    def _configure(self, **_):
        if not self.periphery.enabled:
            raise IOError('Periphery has to be enabled and a multimeter configured correctly!')

    def _scan(self, samples=1, target=1.2, probe_location='direct', periphery_device='Multimeter', **_):
        '''
        VREF calibration main loop

        Parameters
        ----------
        samples: int
            Amount of samples to measure at each point
        target: float
            Target VDDA/D to trim to. Default for RD53A is 1.2V.
        probe_location: 'direct' or 'lemo'
            Is the multimeter / sourcemeter connected to the LEMO VMUX out connector or directly to the VDDA/D pin?
        periphery_device: 'Multimeter' or 'Sourcemeter'
            Are you using a Multimeter or Sourcemeter for this measurement?
        '''

        self.data.multimeter_data_table = self.h5_file.create_table(self.h5_file.root, name='multimeter_data', title='multimeter_data', description=MultimeterTable)
        row = self.data.multimeter_data_table.row

        for channel in ['VDDA', 'VDDD']:
            addresses = {'VDDA': 24, 'VDDD': 30}
            if probe_location.lower() == 'lemo':
                bg_factor = 2.
                # Measure reference GND level
                self.chip.registers['MONITOR_SELECT'].write(int('1' + format(32, '06b') + format(27, '07b'), 2))
                time.sleep(0.1)
                gnd = self.periphery.aux_devices['Sourcemeter'].get_voltage()

                self.chip.registers['MONITOR_SELECT'].write(int('1' + format(32, '06b') + format(addresses[channel], '07b'), 2))
            else:
                bg_factor = 1.
                gnd = 0.
                input('Connect the multimeter to {0} of chip {1} and press Enter to continue...'.format(channel, self.chip.get_sn()))
            diffs = {}
            self.log.info('Scanning {0}...'.format(channel))
            pbar = tqdm(total=32, unit='Setting')
            for trim_value in range(31, -1, -1):
                self.chip.registers['VOLTAGE_TRIM'].write(int('{0:05b}'.format(trim_value) + '{0:05b}'.format(trim_value), 2), verify=True)
                v = []
                for i in range(samples):
                    v.append(self.periphery.aux_devices[periphery_device].get_voltage())
                    row['voltage'] = channel
                    row['trimbit_setting'] = trim_value
                    row['sample'] = i + 1
                    row['value'] = v[-1]
                    row.append()
                voltage = (np.mean(v) - gnd) * bg_factor
                diffs[trim_value] = abs(voltage - target)
                pbar.update(1)
                if voltage < target * 0.95:
                    break

            pbar.close()
            self.log.success('Scan finished')
            opt_trim_value = min(diffs, key=diffs.get)

            self.chip.registers['VOLTAGE_TRIM'].write(int('{0:05b}'.format(opt_trim_value) + '{0:05b}'.format(opt_trim_value), 2), verify=True)
            if channel == 'VDDA':
                self.chip.configuration['trim']['VREF_A_TRIM'] = opt_trim_value
            elif channel == 'VDDD':
                self.chip.configuration['trim']['VREF_D_TRIM'] = opt_trim_value
            voltage = (self.periphery.aux_devices[periphery_device].get_voltage() - gnd) * bg_factor

            self.log.success('Optimal {0} is {1}. Trimmed {2} is {3:1.3f}V.'.format('VREF_A_TRIM' if channel == 'VDDA' else 'VREF_D_TRIM', opt_trim_value, channel, voltage))


if __name__ == "__main__":
    with VREFCalib(scan_config=scan_configuration) as calibration:
        calibration.scan()
