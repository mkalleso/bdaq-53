#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import logging
import yaml
import shutil
import os

import bdaq53
from bdaq53.scans.ITkPixV1.test_data_merging import DataMergingTest
from bdaq53.tests import utils

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config_file = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))

local_configuration = {
    'test_registers': range(10),
    'test_hitdata': range(10),
    'invert': False
}


class TestDataMerging(unittest.TestCase):
    def test_data_merging(self):
        ''' Enable ITkPixV1 settings'''
        with open(bench_config_file) as f:
            bench_config = yaml.full_load(f)

        bench_config['hardware']['bypass_mode'] = False
        bench_config['modules']['module_0']['chip_0']['chip_type'] = 'ITkPixV1'
        bench_config['modules']['module_0']['chip_0']['chip_id'] = 15
        bench_config['modules']['module_0']['chip_0']['receiver'] = 'rx0'
        bench_config['modules']['module_0']['chip_0']['use_ptot'] = False
        bench_config['general']['abort_on_rx_error'] = False  # simulation too slow for RX sync check
        bench_config['general']['use_database'] = False  # data base feature not working for this chip

        logging.info('Starting data merging test')
        self.sim_dc = 21
        with DataMergingTest(bdaq_conf=utils.setup_cocotb(self.sim_dc, chip='ITkPixV1', rx_lanes=1),
                             bench_config=bench_config,
                             scan_config=local_configuration) as scan:

            scan.start()

    def tearDown(self):
        utils.close_sim()
        shutil.rmtree('output_data/', ignore_errors=True)


if __name__ == '__main__':
    unittest.main()
