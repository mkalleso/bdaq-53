#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
import os
import unittest
import pytest

import matplotlib

import bdaq53
from bdaq53.tests import utils  # noqa: E731
from bdaq53.analysis.combine_module_files import CombineModuleFiles
from bdaq53.analysis.plotting import Plotting

bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))

matplotlib.use('Agg')  # Allow headless plotting


class TestCombineModuleFiles(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestCombineModuleFiles, cls).setUpClass()
        plt_logger = logging.getLogger('Plotting')
        cls._plt_log_handler = utils.MockLoggingHandler(level='DEBUG')
        plt_logger.addHandler(cls._plt_log_handler)
        cls.plt_log_messages = cls._plt_log_handler.messages

        ana_logger = logging.getLogger('CombineModuleFile')
        cls._ana_log_handler = utils.MockLoggingHandler(level='DEBUG')
        ana_logger.addHandler(cls._ana_log_handler)
        cls.ana_log_messages = cls._ana_log_handler.messages

    @classmethod
    def tearDownClass(cls):
        dcm_dir = os.path.join(data_folder, 'modules', 'DCM0')
        utils.try_remove(os.path.join(dcm_dir, '20210101_000002_autorange_threshold_scan_DC_interpreted.h5'))
        utils.try_remove(os.path.join(dcm_dir, '20210101_000002_autorange_threshold_scan_DC_interpreted.pdf'))
        utils.try_remove(os.path.join(dcm_dir, '20210101_000002_autorange_threshold_scan_QC_interpreted.h5'))
        utils.try_remove(os.path.join(dcm_dir, '20210101_000002_autorange_threshold_scan_QC_interpreted.pdf'))
        utils.try_remove(os.path.join(dcm_dir, 'last_scan.pdf'))

    @classmethod
    def tearDown(cls):
        # Reset messages after each test
        cls._plt_log_handler.reset()
        cls._ana_log_handler.reset()

    def test_combine_DC_module_files(self):
        ''' Test combination of multiple interpretted data files to one DC module file  '''

        # Filename of interpreted data files of same module without path and extension (ScanBase.run_name)
        interpreted_data_filename = '20210101_000002_autorange_threshold_scan_interpreted'

        # Common module directory with output of all chips in subdirectories
        module_path = os.path.join(data_folder, 'modules', 'DCM0')

        # Relative paths to chip directorys
        chip_rel_paths = ['0x0000', '0x0001']

        # Optional parameters that replace default values:
        module_type = None  # (str) module type defined in modules/module_types.yaml.
        new_chip_ids = None  # (list of int) chip_ids of analyzed_data_files
        target_file = os.path.join(module_path, '20210101_000002_autorange_threshold_scan_DC_interpreted.h5')  # (str) target file name

        filenames = [os.path.join(module_path, rel_path, interpreted_data_filename + '.h5') for rel_path in chip_rel_paths]

        with CombineModuleFiles(analyzed_data_files=filenames,
                                chip_ids=new_chip_ids,
                                module_type=module_type,
                                target_file=target_file) as cmf:
            cmf.combine_standard_tables()

        with Plotting(analyzed_data_file=cmf.target_file) as p:
            p.create_standard_plots()

        self.assertTrue(any('Module files successfully merged:' in m for m in self.ana_log_messages['success']))

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'modules', 'DCM0', '20210101_000002_autorange_threshold_scan_DC_interpreted.h5'),
            os.path.join(data_folder, 'modules', 'DCM0', '20210101_000002_autorange_threshold_scan_DC_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_combine_QM_module_files(self):
        ''' Test combination of multiple interpretted data files to one QM module file
            Data of a DCM is used, while one chip file is used twice and one chip file is not provided
        '''

        # Filename of interpreted data files of same module without path and extension (ScanBase.run_name)
        interpreted_data_filename = '20210101_000002_autorange_threshold_scan_interpreted'

        # Common module directory with output of all chips in subdirectories
        module_path = os.path.join(data_folder, 'modules', 'DCM0')

        # Relative paths to chip directorys
        chip_rel_paths = ['0x0000', '0x0001', '0x0000', None]

        # Optional parameters that replace default values:
        module_type = "common_quad"  # (str) module type defined in modules/module_types.yaml.
        new_chip_ids = [0, 1, 2, 3]  # (list of int) chip_ids of analyzed_data_files
        new_chip_sns = ['0x0000', '0x0001', '0x0002', '0x0003']  # (list of int) chip_ids of analyzed_data_files
        target_file = os.path.join(module_path, '20210101_000002_autorange_threshold_scan_QC_interpreted.h5')  # (str) target file name

        filenames = [os.path.join(module_path, rel_path, interpreted_data_filename + '.h5')
                     if rel_path else rel_path
                     for rel_path in chip_rel_paths]

        with CombineModuleFiles(analyzed_data_files=filenames,
                                chip_ids=new_chip_ids,
                                module_type=module_type,
                                target_file=target_file,
                                chip_sns=new_chip_sns) as cmf:
            cmf.combine_standard_tables()

        with Plotting(analyzed_data_file=cmf.target_file) as p:
            p.create_standard_plots()

        self.assertTrue(any('Module files successfully merged:' in m for m in self.ana_log_messages['success']))

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'modules', 'DCM0', '20210101_000002_autorange_threshold_scan_QC_interpreted.h5'),
            os.path.join(data_folder, 'modules', 'DCM0', '20210101_000002_autorange_threshold_scan_QC_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)


if __name__ == '__main__':
    pytest.main([__file__])
