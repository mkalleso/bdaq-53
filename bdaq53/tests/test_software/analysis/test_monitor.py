''' Script to check the bdaq53 modules for the online monitor

    Simulation producer, interpreter converter and receiver.
'''

import os
import sys
import unittest
import yaml
import time
import psutil
from PyQt5.QtWidgets import QApplication

from online_monitor import OnlineMonitor

import bdaq53
from bdaq53.analysis.online_monitor import start_bdaq53_monitor

bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))


# Create online monitor yaml config with bdaq53 monitor entities
def create_config_yaml(chip_type='rd53a', replay_data='threshold_scan.h5', base_port=5500):
    conf = {}
    # Add producer
    devices = {}
    devices['DAQ0'] = {'backend': 'tcp://127.0.0.1:%i' % base_port,
                       'kind': 'bdaq53_sim',
                       'delay': 0.1,
                       'data_file': os.path.join(data_folder, replay_data)
                       }
    devices['DAQ1'] = {'backend': 'tcp://127.0.0.1:%i' % (base_port + 1),
                       'kind': 'bdaq53_sim',
                       'delay': 0.1,
                       'data_file': os.path.join(data_folder, replay_data)
                       }
    conf['producer_sim'] = devices
    # Add converter
    devices = {}
    devices['DUT0'] = {'kind': 'bdaq53_inter',
                       'frontend': 'tcp://127.0.0.1:%i' % base_port,
                       'backend': 'tcp://127.0.0.1:%i' % (base_port + 100),
                       'chip_type': chip_type
                       }
    devices['DUT1'] = {'kind': 'bdaq53_inter',
                       'frontend': 'tcp://127.0.0.1:%i' % (base_port + 1),
                       'backend': 'tcp://127.0.0.1:%i' % (base_port + 101),
                       'chip_type': chip_type
                       }

    conf['converter'] = devices
    # Add receiver
    devices = {}
    devices['DUT0'] = {'kind': 'bdaq53_recv',
                       'frontend': 'tcp://127.0.0.1:%i' % (base_port + 100),
                       }
    devices['DUT1'] = {'kind': 'bdaq53_recv',
                       'frontend': 'tcp://127.0.0.1:%i' % (base_port + 101),
                       }
    conf['receiver'] = devices
    return yaml.dump(conf, default_flow_style=False)


def get_python_processes():  # return the number of python processes
    n_python = 0
    for proc in psutil.process_iter():
        try:
            if 'python' in proc.name():
                n_python += 1
        except psutil.AccessDenied:
            pass
    return n_python


class TestOnlineMonitor(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        with open('tmp_cfg_rd53a.yml', 'w') as outfile:
            config_file = create_config_yaml()
            outfile.write(config_file)
        with open('tmp_cfg_rd53b.yml', 'w') as outfile:
            config_file = create_config_yaml(chip_type='itkpixv1', replay_data='analog_scan_ptot.h5', base_port=5700)
            outfile.write(config_file)
        # Linux CIs run usually headless, thus virtual x server is needed for gui testing
        if os.getenv('CI', False):
            # raise unittest.SkipTest("CERN CI runner with Miniconda python docker has segfault in these tests.")
            from xvfbwrapper import Xvfb
            cls.vdisplay = Xvfb()
            cls.vdisplay.start()
        # Start the simulation producer to create some fake data
        cls.prod_sim_proc_a = start_bdaq53_monitor.run_script_in_shell('', 'tmp_cfg_rd53a.yml', 'start_producer_sim')
        cls.prod_sim_proc_b = start_bdaq53_monitor.run_script_in_shell('', 'tmp_cfg_rd53b.yml', 'start_producer_sim')
        # Start converter
        cls.conv_manager_proc_a = start_bdaq53_monitor.run_script_in_shell('', 'tmp_cfg_rd53a.yml', command='start_converter')
        cls.conv_manager_proc_b = start_bdaq53_monitor.run_script_in_shell('', 'tmp_cfg_rd53b.yml', command='start_converter')
        # Create Gui
        time.sleep(2)
        cls.app = QApplication(sys.argv)
        cls.online_monitor_rd53a = OnlineMonitor.OnlineMonitorApplication('tmp_cfg_rd53a.yml')
        cls.online_monitor_rd53b = OnlineMonitor.OnlineMonitorApplication('tmp_cfg_rd53b.yml')
        time.sleep(2)

    @classmethod
    def tearDownClass(cls):  # Remove created files
        time.sleep(1)
        start_bdaq53_monitor.kill(cls.prod_sim_proc_a)
        start_bdaq53_monitor.kill(cls.prod_sim_proc_b)
        start_bdaq53_monitor.kill(cls.conv_manager_proc_a)
        start_bdaq53_monitor.kill(cls.conv_manager_proc_b)
        time.sleep(1)
        os.remove('tmp_cfg_rd53a.yml')
        os.remove('tmp_cfg_rd53b.yml')
        cls.online_monitor_rd53a.close()
        cls.online_monitor_rd53b.close()
        time.sleep(1)

    def test_data_chain_rd53a(self):
        ''' Checks for received data for the 2 receivers

            This effectively checks the full chain:
            producer --> converter --> receiver
        '''

        # Qt evsent loop does not run in tests, thus we have to trigger the
        # event queue manually
        self.app.processEvents()
        # Check all receivers present
        self.assertEqual(len(self.online_monitor_rd53a.receivers), 2, 'Number of receivers wrong')
        self.app.processEvents()  # Clear event queue

        # Case 1: Activate status widget, no data should be received
        self.online_monitor_rd53a.tab_widget.setCurrentIndex(0)
        self.app.processEvents()
        time.sleep(5)
        self.app.processEvents()
        time.sleep(5)
        # Data structure to check for no data since receiver widget
        # is not active
        data_recv_0 = []
        self.app.processEvents()
        for receiver in self.online_monitor_rd53a.receivers:
            data_recv_0.append(receiver.occupancy_img.getHistogram(bins=100, step=100))

        # Case 2: Activate DUT widget, receiver 1 should show data
        self.online_monitor_rd53a.tab_widget.setCurrentIndex(1)
        self.app.processEvents()
        time.sleep(5)
        self.app.processEvents()
        time.sleep(5)
        # Data structure to check for data since receiver widget
        # is active
        data_recv_1 = []
        for receiver in self.online_monitor_rd53a.receivers:
            data_recv_1.append(receiver.occupancy_img.getHistogram(bins=100, step=100))

        # Case 3: Activate other DUT widget, receiver 2 should show data
        self.online_monitor_rd53a.tab_widget.setCurrentIndex(2)
        self.app.processEvents()
        time.sleep(5)
        self.app.processEvents()
        time.sleep(5)
        data_recv_2 = []
        for receiver in self.online_monitor_rd53a.receivers:
            data_recv_2.append(receiver.occupancy_img.getHistogram(bins=100, step=100))

        self.assertListEqual(data_recv_0, [(None, None), (None, None)])
        self.assertTrue(data_recv_1[0][0] is not None)
        self.assertTupleEqual(data_recv_0[1], (None, None))
        self.assertTrue(data_recv_2[1][0] is not None)

    def test_data_chain_rd53b(self):
        ''' Checks for received data for the 2 receivers

            This effectively checks the full chain:
            producer --> converter --> receiver
        '''

        # Qt evsent loop does not run in tests, thus we have to trigger the
        # event queue manually
        self.app.processEvents()
        # Check all receivers present
        self.assertEqual(len(self.online_monitor_rd53b.receivers), 2, 'Number of receivers wrong')
        self.app.processEvents()  # Clear event queue

        # Case 1: Activate status widget, no data should be received
        self.online_monitor_rd53b.tab_widget.setCurrentIndex(0)
        self.app.processEvents()
        time.sleep(5)
        self.app.processEvents()
        time.sleep(5)
        # Data structure to check for no data since receiver widget
        # is not active
        data_recv_0 = []
        self.app.processEvents()
        for receiver in self.online_monitor_rd53b.receivers:
            data_recv_0.append(receiver.occupancy_img.getHistogram(bins=100, step=100))

        # Case 2: Activate DUT widget, receiver 1 should show data
        self.online_monitor_rd53b.tab_widget.setCurrentIndex(1)
        self.app.processEvents()
        time.sleep(5)
        self.app.processEvents()
        time.sleep(5)
        # Data structure to check for data since receiver widget
        # is active
        data_recv_1 = []
        for receiver in self.online_monitor_rd53b.receivers:
            data_recv_1.append(receiver.occupancy_img.getHistogram(bins=100, step=100))

        # Case 3: Activate other DUT widget, receiver 2 should show data
        self.online_monitor_rd53b.tab_widget.setCurrentIndex(2)
        self.app.processEvents()
        time.sleep(5)
        self.app.processEvents()
        time.sleep(5)
        data_recv_2 = []
        for receiver in self.online_monitor_rd53b.receivers:
            data_recv_2.append(receiver.occupancy_img.getHistogram(bins=100, step=100))

        self.assertListEqual(data_recv_0, [(None, None), (None, None)])
        self.assertTrue(data_recv_1[0][0] is not None)
        self.assertTupleEqual(data_recv_0[1], (None, None))
        self.assertTrue(data_recv_2[1][0] is not None)

    #  Test the UI
    def test_ui(self):
        # 2 receiver + status widget expected
        self.assertEqual(self.online_monitor_rd53a.tab_widget.count(), 3, 'Number of tab widgets wrong')


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestOnlineMonitor)
    unittest.TextTestRunner(verbosity=2).run(suite)
