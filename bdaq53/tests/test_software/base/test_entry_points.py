''' Script to check the bdaq53 modules for the online monitor

    Simulation producer, interpreter converter and receiver.
'''

import subprocess
import unittest


class TestEntryPoints(unittest.TestCase):

    def test_bdaq_entry_point(self):
        ''' Check entry point: bdaq
        '''
        self.assertTrue(b'Bonn' in subprocess.check_output("bdaq -h", shell=True))

    def test_firmware_entry_point(self):
        ''' Check entry point: bdaq_firmware
        '''
        self.assertTrue(b'manager' in subprocess.check_output("bdaq_firmware -h", shell=True))

    def test_monitor_entry_point(self):
        ''' Check entry point: bdaq_firmware
        '''
        self.assertTrue(b'online' in subprocess.check_output("bdaq_monitor -h", shell=True))

    def test_eudaq_entry_point(self):
        ''' Check entry point: bdaq_firmware
        '''
        self.assertTrue(b'producer' in subprocess.check_output("bdaq_eudaq -h", shell=True))

    def test_deprecated_entry_point(self):
        deprecate_commands = ["bdaq53 -h", "bdaq53_monitor -h", "bdaq53_eudaq -h"]
        for cmd in deprecate_commands:
            with self.assertRaises(subprocess.CalledProcessError):
                subprocess.check_output(cmd, shell=True)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestEntryPoints)
    unittest.TextTestRunner(verbosity=2).run(suite)
