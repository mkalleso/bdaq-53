#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import copy
import shutil
import time
import mock
from mock import call
import os
import unittest
import yaml

import bdaq53
from bdaq53.tests import bdaq_mock

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))


class TestPeriphery(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestPeriphery, cls).setUpClass()

        # Use hardware mocks to be able to test without hardware
        cls.bhm = bdaq_mock.BdaqMock(n_chips=3)
        # Speed up testing time drastically, by not calling mask shifting
        cls.bhm.patch_function('bdaq53.chips.rd53a.RD53AMaskObject.update')
        cls.bhm.patch_function('bdaq53.chips.ITkPixV1.ITkPixV1MaskObject.update')

        cls.power_on_LV_mock = mock.MagicMock()
        cls.bhm.patch_function('bdaq53.system.periphery.BDAQ53Periphery.power_on_LV', cls.power_on_LV_mock)
        cls.power_off_LV_mock = mock.MagicMock()
        cls.bhm.patch_function('bdaq53.system.periphery.BDAQ53Periphery.power_off_LV', cls.power_off_LV_mock)

        cls.bhm.start()

        # Load standard bench config to change in test cases
        with open(bench_config) as f:
            cls.bench_config = yaml.full_load(f)

        # Enable periphery for all scans
        cls.bench_config['periphery']['enable_periphery'] = True
        # Use the chip of the second module to make setup more complex
        cls.bench_config['modules']['module_1'] = copy.deepcopy(cls.bench_config['modules']['module_0'])
        cls.bench_config['modules']['module_1']['powersupply']['lv_name'] = 'LV-1'  # second power supply for 2nd module
        cls.bench_config['modules']['module_1']['chip_0']['chip_sn'] = '0x0003'
        cls.bench_config['modules']['module_1']['chip_0']['receiver'] = "rx2"
        cls.bench_config['modules']['module_1']['chip_0']['send_data'] = "tcp://127.0.0.1:5502"
        # Use the second chip of a dual module to make setup more complex
        cls.bench_config['modules']['module_0']['chip_1'] = copy.deepcopy(cls.bench_config['modules']['module_0']['chip_0'])
        cls.bench_config['modules']['module_0']['chip_1']['chip_sn'] = '0x0002'
        cls.bench_config['modules']['module_0']['chip_1']['chip_id'] = 1
        cls.bench_config['modules']['module_0']['chip_1']['receiver'] = "rx1"
        cls.bench_config['modules']['module_0']['chip_1']['send_data'] = "tcp://127.0.0.1:5501"

    @classmethod
    def tearDownClass(cls):
        cls.bhm.stop()

    @classmethod
    def tearDown(cls):
        # Reset messages after each test
        cls.power_on_LV_mock.reset()  # reset call list
        shutil.rmtree('output_data', ignore_errors=True)  # always delete output from previous test
        time.sleep(0.1)  # shutil.rmtree does not block until file is really deleted, https://bugs.python.org/issue22024

    def test_scan_power_on(self):
        ''' Test scan with power on without power cycle'''
        from bdaq53.scans import scan_digital

        with scan_digital.DigitalScan(bench_config=self.bench_config, scan_config=scan_digital.scan_configuration) as scan:
            scan.init()

        # Check that LV of both modules was turned on
        self.power_on_LV_mock.assert_has_calls([call('module_0', lv_name='LV-0', lv_voltage=1.7, lv_current_limit=2.0),
                                                call('module_1', lv_name='LV-1', lv_voltage=1.7, lv_current_limit=2.0)])

        # Check that no power off was issues (power cycle)
        self.power_off_LV_mock.assert_not_called()

    def test_scan_with_power_cycle(self):
        ''' Test digital scan with power cycle'''
        from bdaq53.scans import scan_digital

        bench_config = copy.deepcopy(self.bench_config)

        bench_config['modules']['module_0']['power_cycle'] = True
        bench_config['modules']['module_1']['power_cycle'] = True

        with scan_digital.DigitalScan(bench_config=bench_config, scan_config=scan_digital.scan_configuration) as scan:
            scan.init()

        # Check that LV of both modules was turned on
        self.power_on_LV_mock.assert_has_calls([call('module_0', lv_name='LV-0', lv_voltage=1.7, lv_current_limit=2.0),
                                                call('module_1', lv_name='LV-1', lv_voltage=1.7, lv_current_limit=2.0)])

        # Check that LV of both modules was turned off
        self.power_off_LV_mock.assert_has_calls([call('module_0'), call('module_1')])


if __name__ == '__main__':
    unittest.main()
