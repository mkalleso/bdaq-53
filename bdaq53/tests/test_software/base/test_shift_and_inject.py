#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import copy
import os
import shutil
import unittest
import yaml

from unittest import mock

import numpy as np
import tables as tb

import bdaq53
from bdaq53.tests import bdaq_mock
from bdaq53.tests import utils

from bdaq53.scans.scan_analog import AnalogScan
from bdaq53.scans.scan_digital import DigitalScan
from bdaq53.scans.scan_threshold_fast import FastThresholdScan
from bdaq53.scans.scan_crosstalk import CrosstalkScan
from bdaq53.scans.scan_in_time_threshold import InTimeThrScan
from bdaq53.scans.scan_source_injection import SourceScanInj

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))

rd53_cfg_file = os.path.join(bdaq53_path, 'chips', 'rd53a_default.cfg.yaml')
itkpixv1_cfg_file = os.path.join(bdaq53_path, 'chips', 'ITkPixV1_default.cfg.yaml')


class TestShiftInject(unittest.TestCase):

    scan_configuration = {
        'start_column': 0,
        'stop_column': 400,
        'start_row': 0,
        'stop_row': 192,
        'n_injections': 100,

        'VCAL_MED': 500,
        'VCAL_HIGH': 1300}

    @classmethod
    def setUpClass(cls):
        super(TestShiftInject, cls).setUpClass()

        # Load standard bench config to change in test cases
        with open(bench_config) as f:
            cls.bench_config = yaml.full_load(f)
        cls.bench_config['general']['use_database'] = False  # deactivate failing feature
        cls.bench_config['modules']['module_0']['chip_0']['chip_config_file'] = rd53_cfg_file  # always use std. config

    @classmethod
    def tearDown(cls):
        utils.try_remove('output_data')  # always delete output from previous test
        utils.try_remove('commands_new.h5')  # always delete output from previous test
        utils.try_remove('commands_old.h5')  # always delete output from previous test

    def old_scan_loop(self, n_injections=100, **kwargs):
        for fe, _ in self.chip.masks.shift(masks=['enable', 'injection']):
            if not fe == 'skipped' and fe == 'SYNC':
                self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections)
            elif not fe == 'skipped':
                self.chip.inject_analog_single(repetitions=n_injections)

    def old_scan_loop_digital(self, n_injections=100, **kwargs):
        for fe, _ in self.chip.masks.shift(masks=['enable', 'injection']):
            if not fe == 'skipped':
                self.chip.inject_digital(repetitions=n_injections)

    def old_scan_loop_crosstalk(self, injection_type='cross_injection', n_injections=100, VCAL_MED=0, VCAL_HIGH_start=0, VCAL_HIGH_stop=4096, VCAL_HIGH_step=102, **_):
        vcal_high_range = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)
        for scan_param_id, vcal_high in enumerate(vcal_high_range):
            self.chip.setup_analog_injection(vcal_high=vcal_high, vcal_med=VCAL_MED)
            for fe, _ in self.chip.masks.shift(masks=['enable', 'injection'], pattern=injection_type, cache=True, skip_empty=False):
                if fe == 'SYNC':
                    self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections)
                else:
                    self.chip.inject_analog_single(repetitions=n_injections)

    def old_scan_loop_intime_threshold(self, start_column=0, stop_column=400, start_row=0, stop_row=192, n_injections=100, VCAL_MED=500, VCAL_HIGH_start=1000, VCAL_HIGH_stop=4000, VCAL_HIGH_step=100, **_):
        vcal_high_range = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)
        finedelay_range = [15]  # Case if no finedelay map is provided
        for scan_param_id, vcal_high in enumerate(vcal_high_range):
            for delay in finedelay_range:
                self.chip.setup_analog_injection(vcal_high=vcal_high, vcal_med=VCAL_MED, fine_delay=delay)
                for fe, _ in self.chip.masks.shift(masks=['enable', 'injection'], cache=True):
                    if not fe == 'skipped' and fe == 'SYNC':
                        self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections)
                    elif not fe == 'skipped':
                        self.chip.inject_analog_single(repetitions=n_injections)

    def old_scan_loop_source_scan_inj(self, n_injections=1, **kwargs):
        for fe, _ in self.chip.masks.shift(masks=['enable', 'injection']):
            if not fe == 'skipped' and fe == 'SYNC':
                self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections, send_trigger=False)
            elif not fe == 'skipped':
                self.chip.inject_analog_single(repetitions=n_injections, send_trigger=False)
        self.chip.write_trigger_latency(500)

    def old_scan_loop_ptot(self, n_injections=100, latency=23, **kwargs):
        for fe, active_pixels in (self.chip.masks.shift(masks=['enable', 'injection', 'hitbus'], pattern='ptot')):
            if not fe == 'skipped':
                for n in range(8):
                    self.chip.enable_core_col_clock(core_cols=[i + n for i in range(0, 50, 8)])
                    self.chip.inject_analog_single(repetitions=n_injections, latency=latency)

    def old_scan_loop_digital_ptot(self, n_injections=100, latency=23, cal_edge_width=32, **kwargs):
        for fe, active_pixels in (self.chip.masks.shift(masks=['enable', 'injection', 'hitbus'], pattern='ptot')):
            if not fe == 'skipped':
                self.chip.inject_digital(repetitions=n_injections, latency=23, cal_edge_width=cal_edge_width)

    def old_scan_loop_fast(self, n_injections=100, VCAL_MED=500, VCAL_HIGH_start=1000, VCAL_HIGH_stop=4000, VCAL_HIGH_step=100, CAL_EDGE_latency=9, wait_cycles=300, **_):
        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH_start, vcal_med=VCAL_MED)
        if self.chip.chip_type.lower() == 'rd53a':
            trigger_latency = self.chip.registers['LATENCY_CONFIG'].get()
            self.chip.registers['LATENCY_CONFIG'].write(CAL_EDGE_latency * 4 + 12)
        vcal_high_range = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)
        scan_param_id_range = range(0, len(vcal_high_range))
        for fe, _ in self.chip.masks.shift(masks=['enable', 'injection'], cache=True):
            for cnt, vcal_high in enumerate(vcal_high_range):
                self.chip.registers['VCAL_HIGH'].write(vcal_high)
                if not fe == 'skipped' and fe == 'SYNC':
                    self.chip.inject_analog_single(repetitions=n_injections, latency=CAL_EDGE_latency,
                                                   wait_cycles=wait_cycles, send_ecr=True)
                elif not fe == 'skipped':
                    self.chip.inject_analog_single(repetitions=n_injections, latency=CAL_EDGE_latency,
                                                   wait_cycles=wait_cycles)
            vcal_high_range = np.flip(vcal_high_range)
            scan_param_id_range = np.flip(scan_param_id_range)
        if self.chip.chip_type.lower() == 'rd53a':
            self.chip.registers['LATENCY_CONFIG'].write(trigger_latency)

    def old_scan_loop_fast_ptot(self, n_injections=100, VCAL_MED=500, VCAL_HIGH_start=1000, VCAL_HIGH_stop=4000, VCAL_HIGH_step=100, wait_cycles=200, **kwargs):
        vcal_high_range = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)
        scan_param_id_range = range(0, len(vcal_high_range))
        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH_start, vcal_med=VCAL_MED)
        for fe, active_pixels in (self.chip.masks.shift(masks=['enable', 'injection', 'hitbus'], pattern='ptot', cache=True)):
            for cnt, vcal_high in enumerate(vcal_high_range):
                self.chip.registers['VCAL_HIGH'].write(vcal_high)
                for n in range(8):
                    self.chip.enable_core_col_clock(core_cols=[i + n for i in range(0, 50, 8)])
                    if not fe == 'skipped' and fe == 'SYNC':
                        self.chip.inject_analog_single(repetitions=n_injections, latency=23,
                                                       wait_cycles=200, send_ecr=True)
                    elif not fe == 'skipped':
                        self.chip.inject_analog_single(repetitions=n_injections, latency=23,
                                                       wait_cycles=200)
            vcal_high_range = np.flip(vcal_high_range)
            scan_param_id_range = np.flip(scan_param_id_range)

    def test_shift_inject_loop_rd53a_digital(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53A '''
        with bdaq_mock.BdaqMock(send_commands_file='commands_new.h5'):
            with DigitalScan(scan_config=self.scan_configuration, bench_config=self.bench_config) as scan:
                scan.configure()
                scan.scan()
        with bdaq_mock.BdaqMock(send_commands_file='commands_old.h5'):
            with DigitalScan(scan_config=self.scan_configuration, bench_config=self.bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop_digital
                scan.configure()
                scan.scan()

        data_equal, error_msg = utils.compare_h5_files('commands_old.h5', 'commands_new.h5')
        self.assertTrue(data_equal, msg=error_msg)

    def test_shift_inject_loop_rd53a(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53A '''
        with bdaq_mock.BdaqMock(send_commands_file='commands_new.h5'):
            with AnalogScan(scan_config=self.scan_configuration, bench_config=self.bench_config) as scan:
                scan.configure()
                scan.scan()
        with bdaq_mock.BdaqMock(send_commands_file='commands_old.h5'):
            with AnalogScan(scan_config=self.scan_configuration, bench_config=self.bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop
                scan.configure()
                scan.scan()

        data_equal, error_msg = utils.compare_h5_files('commands_old.h5', 'commands_new.h5')
        self.assertTrue(data_equal, msg=error_msg)

    def test_shift_inject_loop_fast_rd53a(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53A and fast injection loop '''
        with bdaq_mock.BdaqMock(send_commands_file='commands_new.h5'):
            with FastThresholdScan(scan_config=self.scan_configuration, bench_config=self.bench_config) as scan:
                scan.configure()
                scan.scan()
        with bdaq_mock.BdaqMock(send_commands_file='commands_old.h5'):
            with FastThresholdScan(scan_config=self.scan_configuration, bench_config=self.bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop_fast
                scan.configure()
                scan.scan()

        data_equal, error_msg = utils.compare_h5_files('commands_old.h5', 'commands_new.h5')
        self.assertTrue(data_equal, msg=error_msg)

    def test_shift_inject_loop_rd53b(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53B '''
        bench_config = copy.deepcopy(self.bench_config)
        bench_config['modules']['module_0']['chip_0']['chip_type'] = 'itkpixv1'
        bench_config['modules']['module_0']['chip_0']['use_ptot'] = False
        bench_config['modules']['module_0']['chip_0']['chip_config_file'] = itkpixv1_cfg_file  # always use std. config

        with bdaq_mock.BdaqMock(send_commands_file='commands_new.h5'):
            with AnalogScan(scan_config=self.scan_configuration, bench_config=bench_config) as scan:
                scan.configure()
                scan.scan()
        with bdaq_mock.BdaqMock(send_commands_file='commands_old.h5'):
            with AnalogScan(scan_config=self.scan_configuration, bench_config=bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop
                scan.configure()
                scan.scan()

        data_equal, error_msg = utils.compare_h5_files('commands_old.h5', 'commands_new.h5')
        self.assertTrue(data_equal, msg=error_msg)

    def test_shift_inject_loop_rd53b_ptot_digital(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53B and PToT enabled '''

        bench_config = copy.deepcopy(self.bench_config)
        bench_config['modules']['module_0']['chip_0']['chip_type'] = 'itkpixv1'
        bench_config['modules']['module_0']['chip_0']['use_ptot'] = True
        bench_config['modules']['module_0']['chip_0']['chip_config_file'] = itkpixv1_cfg_file  # always use std. config

        with bdaq_mock.BdaqMock(send_commands_file='commands_new.h5'):
            with DigitalScan(scan_config=self.scan_configuration, bench_config=bench_config) as scan:
                scan.configure()
                scan.scan()
        with bdaq_mock.BdaqMock(send_commands_file='commands_old.h5'):
            with DigitalScan(scan_config=self.scan_configuration, bench_config=bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop_digital_ptot
                scan.configure()
                scan.scan()

        data_equal, error_msg = utils.compare_h5_files('commands_old.h5', 'commands_new.h5')
        self.assertTrue(data_equal, msg=error_msg)

    def test_shift_inject_loop_rd53b_ptot(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53B and PToT enabled '''

        bench_config = copy.deepcopy(self.bench_config)
        bench_config['modules']['module_0']['chip_0']['chip_type'] = 'itkpixv1'
        bench_config['modules']['module_0']['chip_0']['use_ptot'] = True
        bench_config['modules']['module_0']['chip_0']['chip_config_file'] = itkpixv1_cfg_file  # always use std. config

        with bdaq_mock.BdaqMock(send_commands_file='commands_new.h5'):
            with AnalogScan(scan_config=self.scan_configuration, bench_config=bench_config) as scan:
                scan.configure()
                scan.scan()
        with bdaq_mock.BdaqMock(send_commands_file='commands_old.h5'):
            with AnalogScan(scan_config=self.scan_configuration, bench_config=bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop_ptot
                scan.configure()
                scan.scan()

        data_equal, error_msg = utils.compare_h5_files('commands_old.h5', 'commands_new.h5')
        self.assertTrue(data_equal, msg=error_msg)

    def test_shift_inject_loop_fast_ptot(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53B and PToT enabled and fast injection loop '''

        bench_config = copy.deepcopy(self.bench_config)
        bench_config['modules']['module_0']['chip_0']['chip_type'] = 'itkpixv1'
        bench_config['modules']['module_0']['chip_0']['use_ptot'] = True
        bench_config['modules']['module_0']['chip_0']['chip_config_file'] = itkpixv1_cfg_file  # always use std. config

        with bdaq_mock.BdaqMock(send_commands_file='commands_new.h5'):
            with FastThresholdScan(scan_config=self.scan_configuration, bench_config=bench_config) as scan:
                scan.configure()
                scan.scan()
        with bdaq_mock.BdaqMock(send_commands_file='commands_old.h5'):
            with FastThresholdScan(scan_config=self.scan_configuration, bench_config=bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop_fast_ptot
                scan.configure()
                scan.scan()

        data_equal, error_msg = utils.compare_h5_files('commands_old.h5', 'commands_new.h5')
        self.assertTrue(data_equal, msg=error_msg)

    def test_shift_inject_loop_crosstalk_rd53a(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53A '''

        with bdaq_mock.BdaqMock(send_commands_file='commands_new.h5'):
            with CrosstalkScan(scan_config=self.scan_configuration, bench_config=self.bench_config) as scan:
                scan.configure()
                scan.scan()
        with bdaq_mock.BdaqMock(send_commands_file='commands_old.h5'):
            with CrosstalkScan(scan_config=self.scan_configuration, bench_config=self.bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop_crosstalk
                scan.configure()
                scan.scan()

        data_equal, error_msg = utils.compare_h5_files('commands_old.h5', 'commands_new.h5')
        self.assertTrue(data_equal, msg=error_msg)

    def test_shift_inject_loop_intime_threshold_rd53a(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53A '''

        with bdaq_mock.BdaqMock(send_commands_file='commands_new.h5'):
            with InTimeThrScan(scan_config=self.scan_configuration, bench_config=self.bench_config) as scan:
                scan.configure()
                scan.scan()
        with bdaq_mock.BdaqMock(send_commands_file='commands_old.h5'):
            with InTimeThrScan(scan_config=self.scan_configuration, bench_config=self.bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop_intime_threshold
                scan.configure()
                scan.scan()

        data_equal, error_msg = utils.compare_h5_files('commands_old.h5', 'commands_new.h5')
        self.assertTrue(data_equal, msg=error_msg)

    def test_shift_inject_loop_source_scan_inj_rd53a(self):
        ''' Test if new shift and inject function sends the same commands as old one, in case of RD53A '''

        with bdaq_mock.BdaqMock(send_commands_file='commands_new.h5'):
            with SourceScanInj(scan_config=self.scan_configuration, bench_config=self.bench_config) as scan:
                scan.configure()
                scan.scan()
        with bdaq_mock.BdaqMock(send_commands_file='commands_old.h5'):
            with SourceScanInj(scan_config=self.scan_configuration, bench_config=self.bench_config) as scan:
                self.chip = scan.chip
                scan._scan = self.old_scan_loop_source_scan_inj
                scan.configure()
                scan.scan()

        data_equal, error_msg = utils.compare_h5_files('commands_old.h5', 'commands_new.h5')
        self.assertTrue(data_equal, msg=error_msg)


if __name__ == '__main__':
    unittest.main()
