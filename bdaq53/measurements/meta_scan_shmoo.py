#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This meta script performs digital scans at different VDDD voltages and CMD frequencies.
'''

import os
import time
import tables as tb
from copy import deepcopy
import yaml

from basil.dut import Dut

from bdaq53.system import logger, scan_base
from bdaq53.scans.scan_digital import DigitalScan
from bdaq53.scans.test_registers import RegisterTest
from bdaq53.analysis.plotting import Plotting


scan_configuration = {
    # Supply voltage paramters
    'VDDD_mV_start': 800,
    'VDDD_mV_stop': 1200,
    'VDDD_mV_step': 100,

    'VDDA': 1.2,
    'VDDD': 1.2,

    # Frequency parameters
    'F_CMD_start': 100,
    'F_CMD_stop': 200,
    'F_CMD_step': 20,
}

digital_scan_configuration = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192
}

register_test_configuration = {
    'ignore': ['PIX_PORTAL',
               'GLOBAL_PULSE_ROUTE'
               ]
}

device_configuration = {
    'transfer_layer':
        [
            {
                'name': 'Serial1',
                'type': 'Serial',
                'init': {
                    'port': '/dev/ttyUSB0',
                    'read_termination': "\r\n",
                    'write_termination': "\n",
                    'baudrate': 19200,
                    'timeout': 2.0,
                    'xonxoff': True,
                    'parity': 'N',
                    'stopbits': 1,
                    'bytesize': 8
                }
            }
        ],

    'hw_drivers':
        [
            {
                'name': 'LVPowersupply1',
                'type': 'tti_ql355tp',
                'interface': 'Serial1'
            }
        ],

    'registers':
        [
            {
                'name': 'VDDA',
                'type': 'FunctionalRegister',
                'hw_driver': 'LVPowersupply1',
                'arg_names': ['value', 'on'],
                'arg_add': {"channel": 1}
            },
            {
                'name': 'VDDD',
                'type': 'FunctionalRegister',
                'hw_driver': 'LVPowersupply1',
                'arg_names': ['value', 'on'],
                'arg_add': {"channel": 2}
            }
        ]
}


class RawDataTable(tb.IsDescription):
    supply_voltage = tb.Float32Col(pos=1)
    frequency = tb.Int32Col(pos=2)
    discard_count = tb.Int32Col(pos=3)
    soft_error_count = tb.Int32Col(pos=4)
    hard_error_count = tb.Int32Col(pos=5)
    step_failed = tb.Int32Col(pos=6)


class DigitalScanShmoo(DigitalScan):
    def _configure(self, aurora_ref, **_):
        self.bdaq.init(aurora_ref=aurora_ref)
        self.chip.init()
        super(DigitalScanShmoo, self)._configure(**_)


class RegisterTestShmoo(RegisterTest):
    def _configure(self, aurora_ref, **_):
        self.bdaq.init(aurora_ref=aurora_ref)
        self.chip.init()
        super(RegisterTestShmoo, self)._configure(**_)


def load_bench_config(config=None):
    try:
        if config is None:
            config = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'bdaq53' + os.sep + 'testbench.yaml')
        with open(config) as f:
            cfg = yaml.full_load(f)
    except TypeError:
        cfg = config

    return cfg


class MetaScanShmoo(object):
    def __init__(self):
        self.log = logger.setup_derived_logger('MetaShmooScn')
        self.bench_config = load_bench_config()

        # Init powersupply
        self.ps = Dut(device_configuration)
        self.ps.init()

        # Need ScanBase __init__ to obtain multi-chip/multi-module variable lists
        # FIXME: Very ugly workaround
        # TODO: temporary turn off logging here
        tmp_scan = DigitalScan()
        self._output_directories = tmp_scan._output_directories_per_scan
        self._scan_configuration_per_scan = tmp_scan._scan_configuration_per_scan
        del tmp_scan

        self._output_filename = time.strftime("%Y%m%d_%H%M%S") + '_meta_shmoo_scan'

        self._output_filenames = [os.path.join(directory, self._output_filename) for directory in self._output_directories]

        self._h5_files = []
        self._raw_data_tables = []

        for filename in self._output_filenames:
            h5file = tb.open_file(filename + '.h5', 'w')
            raw_data_table = h5file.create_table(h5file.root, name='data', title='Data', description=RawDataTable)

            self._h5_files.append(h5file)
            self._raw_data_tables.append(raw_data_table)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            self.log.error(exc_value)
            self.log.error('Scan failed!')

    def scan(self, VDDD_mV_start=1100, VDDD_mV_stop=1300, VDDD_mV_step=50, F_CMD_start=140, F_CMD_stop=180, F_CMD_step=200, VDDA=1.2, VDDD=1.2, **_):
        for supply_voltage_mV in range(VDDD_mV_start, VDDD_mV_stop, VDDD_mV_step):
            supply_voltage = supply_voltage_mV / 1000.0

            for frequency in range(F_CMD_start, F_CMD_stop + F_CMD_step, F_CMD_step):
                digital_scan_config = deepcopy(digital_scan_configuration)

                # set voltages and frequency
                self.ps['VDDA'].set_enable(on=False)
                self.ps['VDDD'].set_enable(on=False)
                self.ps['VDDA'].set_voltage(VDDA)
                self.ps['VDDA'].set_current_limit(0.7)
                self.ps['VDDD'].set_voltage(supply_voltage)
                self.ps['VDDD'].set_current_limit(0.7)
                digital_scan_config['aurora_ref'] = frequency

                self.log.info('Starting digital scan at VDDA=%1.2fV and f=%uMHz' % (supply_voltage, frequency))
                step_failed = False
                with DigitalScanShmoo(bench_config=self.bench_config, scan_config=digital_scan_config, record_chip_status=False) as scn:
                    try:
                        scn.scan()
                        scn.analyze()
                    except Exception:
                        self.log.info('skipping')
                        step_failed = True

                for filename, raw_data_table in zip(self._output_filenames, self._raw_data_tables):
                    try:
                        with tb.open_file(filename + '.h5') as in_file:  # get error counters from scan
                            discard_count = in_file.root.configuration.link_status.aurora_link[0][1]
                            soft_error_count = in_file.root.configuration.link_status.aurora_link[1][1]
                            hard_error_count = in_file.root.configuration.link_status.aurora_link[2][1]
                    except Exception:
                        discard_count = -1
                        soft_error_count = -1
                        hard_error_count = -1

                    row = raw_data_table.row
                    row['supply_voltage'] = supply_voltage
                    row['frequency'] = frequency
                    row['discard_count'] = discard_count
                    row['soft_error_count'] = soft_error_count
                    row['hard_error_count'] = hard_error_count
                    row['step_failed'] = step_failed
                    row.append()
                    raw_data_table.flush()

                # run a register test
                self.log.info('Starting register test')
                with RegisterTestShmoo(bench_config=self.bench_config, scan_config=register_test_configuration) as test:
                    test.scan()
                    test.analyze()

    def analyze(self):
        for filename in self._output_filenames:
            with tb.open_file(filename + '.h5') as in_file:
                with tb.open_file(filename + '_interpreted.h5', 'a') as outfile:
                    outfile.copy_node(in_file.root.configuration.link_status.aurora_link, outfile.root, recursive=True)

            with Plotting(analyzed_data_file=filename + '.h5') as p:
                p.create_standard_plots()


if __name__ == '__main__':
    with MetaScanShmoo() as scan:
        scan.scan(**scan_configuration)
        scan.analyze()
