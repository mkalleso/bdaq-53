#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

''' Module to manage firmware download, compilation and flashing using vivado.
    Mainly for CI runner but also useful during headless session (e.g. test beams)
'''

import argparse
import urllib
import re
import requests
import math
import fileinput
import pkg_resources
import tarfile
import shutil
import os.path
from sys import platform

import pexpect
from tqdm import tqdm
import git

import bdaq53
from bdaq53.system import logger

log = logger.setup_derived_logger('FirmwareManager')

repository = r'https://gitlab.cern.ch/silab/bdaq53/'
firmware_dev_url = repository + r'wikis/Hardware/Firmware-(development-versions)'
firmware_url = repository + r'tags'

sitcp_repo = r'https://github.com/BeeBeansTechnologies/SiTCP_Netlist_for_Kintex7'
sitcp_10G_repo = r'https://github.com/BeeBeansTechnologies/SiTCPXG_Netlist_for_Kintex7'

bdaq53_path = os.path.dirname(bdaq53.__file__)


def get_web_page(url):
    ''' Read web page.
    '''
    response = urllib.request.urlopen(url)  # Add md for markdown
    return response.read().decode('utf-8')


def find_links(url):
    response = get_web_page(url)
    version = pkg_resources.get_distribution("bdaq53").version
    version_str = version.split('.')[0] + '.' + version.split('.')[1] + '.0_'
    links = re.findall(r'\[%s(.*?)\]' % version_str, response)

    return links


def download_firmware(name, url):
    ''' Download firmware tar.gz file from url

        The release firmwares are stored with the tag information (firmware_url)
        and the development firmware is attached to a wiki page (firmware_dev_url)
    '''
    response = get_web_page(url)
    version = pkg_resources.get_distribution("bdaq53").version
    version_str = version.split('.')[0] + '.' + version.split('.')[1] + '.0_'
    links = re.findall(r'\[%s(.*?)\]' % version_str, response)

    download_links = []

    for l in links:
        if name in l:
            download_links.append(l)

    if not download_links:
        log.error('No firmware for version %s with name %s at %s\nPossible firmwares: %s', version_str, name, url, ', '.join(links))
        return

    if len(download_links) > 1:
        raise RuntimeError('Found multiple firmwares with name %s: %s Please specify!', name, str(download_links))

    m = re.search('uploads/\w+/(.*?)%s%s' % (version_str, download_links[0]), response)
    download_link = repository + 'wikis/' + m.group(0)

    log.info('Downloading %s', download_link)
    # Streaming, so we can iterate over the response.
    r = requests.get(download_link, stream=True)

    # Total size in bytes.
    total_size = int(r.headers.get('content-length', 0))
    block_size = 1024
    wrote = 0
    with open(name, 'wb') as f:
        for data in tqdm(r.iter_content(block_size), total=math.ceil(total_size // block_size), unit='KB', unit_scale=True):
            wrote = wrote + len(data)
            f.write(data)
    if total_size != 0 and wrote != total_size:
        raise RuntimeError('Download failed!')
    return name


def unpack_files(name):
    ''' Extracts the bit file from the tar.gz file

        name: string
            Name of bit file (.tar.gz suffix) and
            Name of compressed bitfile (name.bit)
    '''
    tar = tarfile.open(name, "r:gz")
    for m in tar.getmembers():
        if '.bit' in m.name:
            m.name = os.path.basename(m.name)  # unpack without folder
            tar.extract(m, path=".")
            log.debug('Unpacked %s', m.name)
            bit_name = m.name
        if '.mcs' in m.name:
            m.name = os.path.basename(m.name)  # unpack without folder
            tar.extract(m, path=".")
            log.debug('Unpacked %s', m.name)
            mcs_name = m.name
    tar.close()
    return bit_name, mcs_name


def flash_firmware(name):
    ''' Flash firmware using vivado in tcl mode
    '''

    def get_return_string(timeout=1):
        ''' Helper function to get full return string.

            This complexity needed here since Xilinx does multi line returns
        '''
        flushed = bytearray()
        try:
            while not vivado.expect(r'.+', timeout=timeout):
                flushed += vivado.match.group(0)
        except pexpect.exceptions.TIMEOUT:
            pass
        return flushed.decode('utf-8')

    try:
        vivado = pexpect.spawn('vivado_lab -mode tcl', timeout=10)  # try lab version
        vivado.expect('Vivado', timeout=5)
    except pexpect.exceptions.ExceptionPexpect:
        try:
            vivado = pexpect.spawn('vivado -mode tcl', timeout=10)  # try full version
            vivado.expect('Vivado', timeout=20)
        except pexpect.exceptions.ExceptionPexpect:
            log.error('Cannot execute vivado / vivado_lab commend')
            return
    vivado.expect(['vivado_lab%', 'Vivado%'], timeout=20)  # Booted up when showing prompt

    log.info('Connecting to JTAG interface')

    vivado.sendline('open_hw')
    vivado.expect(['vivado_lab%', 'Vivado%'])  # Command finished when showing prompt

    vivado.sendline('connect_hw_server')
    vivado.expect('localhost')  # Printed when successful
    get_return_string()

    vivado.sendline('current_hw_target')
    ret = get_return_string()
    log.info('Connected to FPGA: %s', ret)
    if 'WARNING' in ret:
        log.error('Cannot find programmer hardware, Xilinx warning:\n%s', ret)
        vivado.sendline('exit')
        vivado.expect('Exiting')
        return

    vivado.sendline('open_hw_target')
    vivado.expect('Opening hw_target')  # Printed when programmer found

    vivado.sendline('current_hw_device [lindex [get_hw_devices] 0]')
    vivado.expect(['vivado_lab%', 'Vivado%'])  # Printed when finished
    ret = get_return_string()

    if 'xc7k160t' in ret and 'BDAQ' not in name:
        log.error('The selected bitfile \'%s\' does not match the connected FPGA type \'xc7k160t\'', name)
        vivado.sendline('exit')
        vivado.expect('Exiting')
        return
    if 'xc7k325t' in ret and 'KC705' not in name:
        log.error('The selected bitfile \'%s\' does not match the connected FPGA type \'xc7k325t\'', name)
        vivado.sendline('exit')
        vivado.expect('Exiting')
        return

    if '.bit' in name:
        log.info('Writing only to FPGA, not to FLASH memory. Config lost after power cycle!')

        vivado.sendline('set devPart [get_property PART [current_hw_device]]')
        vivado.expect(['vivado_lab%', 'Vivado%'])  # Printed when finished

        vivado.sendline('set_property PROGRAM.FILE {%s} [current_hw_device]' % name)
        vivado.expect(['vivado_lab%', 'Vivado%'])  # Printed when finished

        log.info('Writing firmware %s', name)

        vivado.sendline('program_hw_devices [current_hw_device]')
        vivado.expect('End of startup status: HIGH')  # firmware upload successful
    elif '.mcs' in name:
        log.info('Writing to the FLASH memory. Config kept even after power cycle.')

        if 'BDAQ' in name:
            flash_chip = 's25fl512s-spi-x1_x2_x4'
        elif 'USBPix3' in name or 'KX1' in name:
            flash_chip = 'mt25ql256-spi-x1_x2_x4'
        elif 'KC705' in name:
            flash_chip = 'mt25ql128-spi-x1_x2_x4'

        vivado.sendline('create_hw_cfgmem -hw_device [current_hw_device] [lindex [get_cfgmem_parts {%s}] 0]' % flash_chip)

        vivado.sendline('set_property PROGRAM.ADDRESS_RANGE {use_file} [get_property PROGRAM.HW_CFGMEM [current_hw_device]]')
        vivado.sendline('set_property PROGRAM.FILES {%s} [get_property PROGRAM.HW_CFGMEM [current_hw_device]]' % name)
        vivado.sendline('set_property PROGRAM.BLANK_CHECK 0 [ get_property PROGRAM.HW_CFGMEM [current_hw_device]]')
        vivado.sendline('set_property PROGRAM.ERASE 1 [get_property PROGRAM.HW_CFGMEM [current_hw_device]]')
        vivado.sendline('set_property PROGRAM.CFG_PROGRAM 1 [get_property PROGRAM.HW_CFGMEM [current_hw_device]]')
        vivado.sendline('set_property PROGRAM.VERIFY 1 [get_property PROGRAM.HW_CFGMEM [current_hw_device]]')

        vivado.sendline('create_hw_bitstream -hw_device [current_hw_device] [get_property PROGRAM.HW_CFGMEM_BITFILE [current_hw_device]]')
        vivado.sendline('program_hw_devices [current_hw_device]')
        vivado.expect('End of startup status: HIGH', timeout=10)

        vivado.sendline('program_hw_cfgmem -hw_cfgmem [ get_property PROGRAM.HW_CFGMEM [current_hw_device]]')
        vivado.expect('Flash programming completed successfully', timeout=2 * 60)  # Generous timeout
        log.info(get_return_string())

        vivado.sendline('boot_hw_device [current_hw_device]')
        vivado.expect('Done pin status: HIGH')

    vivado.sendline('close_hw_target')
    vivado.expect('Closing')
    vivado.sendline('exit')
    vivado.expect('Exiting')

    log.success('All done!')


def compile_firmware(name):
    ''' Compile firmware using vivado in tcl mode
    '''

    def get_return_string(timeout=1):
        ''' Helper function to get full return string.

            This complexity needed here since Xilinx does multi line returns
        '''
        flushed = bytearray()
        try:
            while not vivado.expect(r'.+', timeout=timeout):
                flushed += vivado.match.group(0)
        except (pexpect.exceptions.TIMEOUT, pexpect.exceptions.EOF):
            pass
        return flushed.decode('utf-8')

    supported_firmwares = ['BDAQ53', 'BDAQ53_10G', 'USBPIX3', 'KC705', 'BDAQ53_10G_4LANE', 'BDAQ53_4LANE',
                           'BDAQ53_RX640', 'BDAQ53_RX640_10G', 'USBPIX3_RX640', 'KC705_RX640']
    if name not in supported_firmwares:
        log.error('Can only compile firmwares: %s', ','.join(supported_firmwares))
        return

    log.info('Compile firmware %s', name)

    vivado_tcl = os.path.join(bdaq53_path, '..', 'firmware/vivado')

    # Use mappings from run.tcl
    fpga_types = {'BDAQ53': 'xc7k160tffg676-2',
                  'USBPIX3': 'xc7k160tfbg676-1',
                  'KC705': 'xc7k325tffg900-2'}
    constrains_files = {'BDAQ53': 'bdaq53.xdc',
                        'USBPIX3': 'usbpix3.xdc',
                        'KC705': 'kc705_gmii.xdc'}
    flash_sizes = {'BDAQ53': '64',
                   'USBPIX3': '64',
                   'KC705': '16'}

    for k, v in fpga_types.items():
        if k in name:
            fpga_type = v
            constraints_files = constrains_files[k]
            if '10G' in name:
                constraints_files = constraints_files.split('.xdc')[0] + '_10G' + '.xdc'
            flash_size = flash_sizes[k]
            board_name = k

    if '_SMA' in name:
        connector = '_SMA'
    elif '_FMC_LPC' in name:
        connector = '_FMC_LPC'
    else:
        connector = '""'

    if 'RX640' in name:
        speed = '_RX640'
    else:
        speed = '_RX1280'

    if 'KX1' in name:
        option = '_KX1'
    else:
        option = '""'

    if '10G' in name:
        eth_speed = '10G'
    else:
        eth_speed = '1G'
    if '_4LANE' in name:
        lanes = '_4LANE'
    else:
        lanes = '_1LANE'

    command_args = fpga_type + ' ' + board_name + ' ' + connector + ' ' + constraints_files + ' ' + flash_size + ' ' + option + ' ' + lanes + ' ' + speed + ' ' + eth_speed
    command = 'vivado -mode tcl -source run.tcl -tclargs %s' % command_args
    log.info('Compiling firmware. Takes about 20 minutes!')
    try:
        vivado = pexpect.spawn(command, cwd=vivado_tcl, timeout=10)
        vivado.expect('Vivado', timeout=5)
    except pexpect.exceptions.ExceptionPexpect:
        log.error('Cannot execute vivado command %d.\nMaybe paid version is missing, that is needed for compilation?', command)
        return

    import time
    timeout = 100  # 500 seconds with no new print to screen
    t = 0
    while t < timeout:
        r = get_return_string()
        if r:
            if 'write_cfgmem completed successfully' in r:
                break
            print('.', end='', flush=True)
            t = 0
        else:
            time.sleep(5)
            t += 1
    else:
        raise RuntimeError('Timeout during compilation, check vivado.log')

    # Move firmware to current folder
    cwd = os.getcwd()
    vivado_tcl = os.path.join(bdaq53_path, '..', 'firmware/vivado')
    output_name = board_name + (option if option != '""' else '') + (connector if connector != '""' else '') + lanes + (speed if speed != '""' else '') + ('_' + eth_speed if eth_speed != '""' else '')
    shutil.move(os.path.join(vivado_tcl, r'output/%s' % output_name + '.bit'), cwd)
    log.info('SUCCESS!')


def find_vivado(path):
    ''' Search in std. installation paths for vivado(_lab) binary
    '''
    out_path = os.environ.get('PATH').split(':')
    for elem in out_path:
        if "xilinx/vivado/" in elem.lower():
            return os.path.join(elem.split('/bin')[0], 'bin')

    if platform == "linux" or platform == "linux2":
        linux_install_path = '/opt/' if not path else path
        # Try vivado full install
        paths = where(name='vivado', path=linux_install_path)
        for path in paths:
            if 'bin' in path:
                return os.path.dirname(os.path.realpath(path))
        # Try vivado lab install
        paths = where(name='vivado_lab', path=linux_install_path)
        for path in paths:
            if 'bin' in path:
                return os.path.dirname(os.path.realpath(path))
    else:
        raise NotImplementedError('Only Linux supported')


def where(name, path, flags=os.F_OK):
    result = []
    paths = [path]
    for outerpath in paths:
        for innerpath, _, _ in os.walk(outerpath):
            path = os.path.join(innerpath, name)
            if os.access(path, flags):
                result.append(os.path.normpath(path))
    return result


def run(name, path=None, create=False, target='flash'):
    ''' Steps to download/compile/flash matching firmware to FPGA

        name: str
            Firmware name:
                If name has .bit suffix try to flash from local file
                If not available find suitable firmware online, download, extract, and flash
        compile: boolean
            Compile firmware
    '''

    vivado_path = find_vivado(path)
    if vivado_path:
        log.debug('Found vivado binary at %s', vivado_path)
        os.environ["PATH"] += os.pathsep + vivado_path
    else:
        if path:
            log.error('Cannot find vivado installation in %s', path)
        else:
            log.error('Cannot find vivado installation!')
            if not create:
                log.error('Install vivado lab from here:\nhttps://www.xilinx.com/support/download.html')
            else:
                log.error('Install vivado paid version to be able to compile firmware')
        return

    if not create:
        if os.path.isfile(name):
            log.info('Found existing local bit file')
            bit_file = name
        else:
            if not name.endswith('.tar.gz'):
                name += '.tar.gz'
            stable_firmware = True  # std. setting: use stable (tag) firmware
            version = pkg_resources.get_distribution("bdaq53").version
            if not os.getenv('CI'):
                try:
                    import git
                    try:
                        repo = git.Repo(search_parent_directories=True, path=bdaq53_path)
                        active_branch = repo.active_branch
                        if active_branch != 'master':
                            stable_firmware = False  # use development firmware
                    except git.InvalidGitRepositoryError:  # no github repo --> use stable firmware
                        pass
                except ImportError:  # git not available
                    log.warning('Git not properly installed, assume software release %s', version)
                    pass
                if stable_firmware:
                    tag_list = get_tag_list(firmware_url)
                    matches = [i for i in range(len(tag_list)) if version in tag_list[i]]
                    if not matches:
                        raise RuntimeError('Cannot find tag version %s at %s', version, firmware_url)
                    tag_url = firmware_url + '/' + tag_list[matches[0]]
                    log.info('Download stable firmware version %s', version)
                    archiv_name = download_firmware(name, tag_url)
                else:
                    log.info('Download development firmware')
                    archiv_name = download_firmware(name, firmware_dev_url + '.md')
            else:  # always use development version for CI runner
                archiv_name = download_firmware(name, firmware_dev_url + '.md')
            if not archiv_name:
                return
            bit_file, mcs_file = unpack_files(archiv_name)
        if target == 'flash':
            flash_firmware(mcs_file)
        elif target == 'fpga':
            flash_firmware(bit_file)
        else:
            log.error('No JTAG target (fpga, flash) specified')
    else:
        get_si_tcp()  # get missing SiTCP sources
        compile_firmware(name)


def get_tag_list(url):
    ''' Extracts all tag names from firmware_url

        This is needed since the naming scheme is inconsistent
    '''

    response = get_web_page(url)
    return re.findall(r'href="/silab/bdaq53/tags/(.*?)"', response)


def get_si_tcp():
    ''' Download SiTCP/SiTCP10G sources from official github repo and apply patches
    '''

    def line_prepender(filename, line):
        with open(filename, 'rb+') as f:
            content = f.read()
            f.seek(0, 0)
            # Python 3, wtf?
            add = bytearray()
            add.extend(map(ord, line))
            add.extend(map(ord, '\n'))
            f.write(add + content)

    sitcp_folder = os.path.join(bdaq53_path, '..', 'firmware/SiTCP/')

    # Only download if not already existing SiTCP git repository
    if not os.path.isdir(os.path.join(sitcp_folder)):
        if not os.path.isdir(os.path.join(sitcp_folder, '.git')):
            log.info('Downloading SiTCP')

            # Has to be moved to be allowed to use existing folder for git checkout
            git.Repo.clone_from(url=sitcp_repo,
                                to_path=sitcp_folder, branch='master')
            # Patch sources, see README of bdaq53
            line_prepender(filename=sitcp_folder + 'TIMER.v', line=r'`default_nettype wire')
            line_prepender(filename=sitcp_folder + 'WRAP_SiTCP_GMII_XC7K_32K.V', line=r'`default_nettype wire')
            for line in fileinput.input([sitcp_folder + 'WRAP_SiTCP_GMII_XC7K_32K.V'], inplace=True):
                print(line.replace("assign\tMY_IP_ADDR[31:0]\t= (~FORCE_DEFAULTn | (EXT_IP_ADDR[31:0]==32'd0) \t? DEFAULT_IP_ADDR[31:0]\t\t: EXT_IP_ADDR[31:0]\t\t);",
                                   'assign\tMY_IP_ADDR[31:0]\t= EXT_IP_ADDR[31:0];'), end='')
        else:  # update if existing
            g = git.cmd.Git(sitcp_folder)
            g.pull()

    sitcp_10G_folder = os.path.join(bdaq53_path, '..', 'firmware/SiTCP10G/')
    # Only download if not already existing SiTCP10G git repository
    if not os.path.isdir(os.path.join(sitcp_10G_folder, '.git')):
        log.info('Downloading SiTCP10G')

        # Has to be moved to be allowed to use existing folder for git checkout
        git.Repo.clone_from(url=sitcp_10G_repo,
                            to_path=sitcp_10G_folder, branch='master')
        # Patch sources, see README of bdaq53
        for line in fileinput.input([sitcp_10G_folder + 'WRAP_SiTCPXG_XC7K_128K.v'], inplace=True):
            print(line.replace("\t\t.MY_IP_ADDR	\t\t\t\t\t(MY_IP_ADDR[31:0]	\t\t\t),\t// in\t: My IP address[31:0]",
                               "\t\t.MY_IP_ADDR	\t\t\t\t\t({8'd192, 8'd168, 8'd100, 8'd12}),\t// in\t: My IP address[31:0]"), end='')

    else:  # update if existing
        g = git.cmd.Git(sitcp_10G_folder)
        g.pull()


def main():
    parser = argparse.ArgumentParser(description='BDAQ53 firmware manager.', formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('--firmware',
                        nargs=1,
                        help='Firmware file name (e.g. auto, BDAQ53, KC705, ...)\n See https://gitlab.cern.ch/silab/bdaq53/tags',)

    parser.add_argument('--vivado_path',
                        default=[None],
                        nargs=1,
                        help='Path of vivado installation',)

    parser.add_argument('-c', '--compile',
                        action='store_true',
                        help='Compile firmware',)

    parser.add_argument('--target',
                        default='fpga',
                        help='Selects firmware target:\n  fpga: firmware is written to the FPGA and lost after power-cycle (useful for debugging)\n  flash: firmware is written to the persistent flash memory and loaded after power-cycle',)

    args = parser.parse_args()

    if args.firmware is None:
        log.info('Available development firmware versions:')
        for link in find_links(firmware_dev_url + '.md'):
            print(link[:-7])
    else:
        run(args.firmware[0], path=args.vivado_path[0], create=args.compile, target=args.target)


if __name__ == '__main__':
    run('development_BDAQ53_1LANE_RX640')
