#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Implementation of the RD53B analog monitoring board I2C interface
'''

import logging
import time
from struct import pack, unpack_from
from array import array

from basil.HL.GPAC import GpioPca9554, HardwareLayer

loglevel = logging.INFO


class NC_GPIO(GpioPca9554):
    ''' GPIO expander : PCA9554'''

    def __init__(self, intf, conf, NC_CHIP_ADDR=0x00, NC_GPIO_CFG=0x00, NC_CHIP_OUT=0x01):
        super(NC_GPIO, self).__init__(intf, conf)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(loglevel)
        self.PCA9554_ADD = NC_CHIP_ADDR
        self.PCA9554_OUT = NC_CHIP_OUT
        self.GPIO_CFG = NC_GPIO_CFG

    def _modify_register(self, mask, value):
        ''' Read and write the registers '''

        # read
        reg_read = self._read_output_port()
        self.logger.debug('register read: %s' % format(reg_read, '#010b'))

        # modify
        reg_mod = (reg_read & ~mask) | (value & mask)
        self.logger.debug('register modify: %s' % format(reg_mod, '#010b'))

        # write
        self._write_output_port(reg_mod)

    def _modify_register_open_drain(self, mask, value):
        ''' Read and write the registers configured as open drain '''

        # read
        self._intf.write(self._base_addr + self.PCA9554_ADD, array('B', pack('B', self.PCA9554_CFG)))
        reg_read = unpack_from('B', self._intf.read(self._base_addr + self.PCA9554_ADD | 1, size=1))[0]
        self.logger.debug('register read: %s' % format(reg_read, '#010b'))

        # modify
        reg_mod = (reg_read & ~mask) | (value & mask)
        self.logger.debug('register modify: %s' % format(reg_mod, '#010b'))

        # write
        self._write_output_port_select(reg_mod)

    def _read_port(self, mask):
        ''' Read the input registers '''

        return self._read_input_port() & mask


class ADG729(HardwareLayer):
    ''' I2C switch : ADG729BRUZ '''

    def __init__(self, intf, conf, NC_CHIP_ADDR=0x00):
        super(ADG729, self).__init__(intf, conf)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(loglevel)
        self.BASE_ADD = NC_CHIP_ADDR

    def _modify_register(self, mask, value):
        ''' Read and write the registers '''

        # read
        reg_read = self._get_i2c()
        self.logger.debug('register read: %s' % format(reg_read, '#010b'))

        # modify
        reg_mod = (reg_read & ~mask) | (value & mask)
        self.logger.debug('register modify: %s' % format(reg_mod, '#010b'))

        # write
        self._set_i2c(reg_mod)

    def _read_port(self, mask):
        ''' function to read from input registers '''
        return self._get_i2c & mask

    def _set_i2c(self, bus):
        ''' function to write to output register '''
        self._intf.write(self.BASE_ADD, array('B', pack('B', bus)))

    def _get_i2c(self):
        ''' function to read from output registers '''
        return unpack_from('B', self._intf.read(self.BASE_ADD | 1, size=1))[0]


class MonitoringBoard(GpioPca9554):
    # ADC Multiplexer Map
    _adc_mux_map = {
        'VINA': 0x00,
        'VIND': 0x01,
        'VDDA': 0x02,
        'VDDD': 0x03,
        'VDD_OVP': 0x04,
        'IMUX_OUT': 0x05,
        'VMUX_OUT': 0x06,
        'R_IREF': 0x07,
        'VOFS': 0x08,
        'VDD_PRE': 0x09,
        'VREF_ADC': 0xA,
        'NTC': 0xB,
        'REXTA': 0xC,
        'REXTD': 0xD,
        'VREFA': 0xE,
        'VREFD': 0xF,

    }

    _gpio_expander_map = {
        # Register name: [chip no, lsb offset, bits]

        # GPIO expander 0 @ I2C address (0x40) for the analog monitoring card multiplexer
        'MUX_SEL': [0, 0, 4, 0],

        # GPIO expander 1 @ I2C address (0x45) for the analog monitoring card IREF_TRIM, BYPASS and EN_VDD_SHUNT
        'IREF_TRIM': [1, 0, 4, 1],
        'BYPASS': [1, 4, 1, 1],
        'EN_VDD_SHUNT': [1, 5, 1, 0],

        # GPIO matrix switch ADG729BRUZ @ I2C address (0x8A) for external analog and digital resistors
        'REXTA': [2, 0, 4, 0],
        'REXTD': [2, 4, 4, 0],

        # GPIO matrix switch ADG729BRUZ @ I2C address (0x88) for external offset resistors
        'ROFS': [3, 0, 4, 0],
        'ROFS_LP': [3, 4, 4, 0]
    }

    def __init__(self, intf,
                 conf={'name': 'GpioPca9554', 'type': 'GpioPca9554', 'interface': 'intf', 'base_addr': 0x00}):
        super().__init__(intf, conf)
        self._base_addr = conf['base_addr']

        self.logger = logging.getLogger('Monitoring_Board')
        self.logger.setLevel(loglevel)

        # GPIO expander 0 @ I2C address (0x40)
        self.gpio_0 = NC_GPIO(intf, conf, NC_CHIP_ADDR=0x40, NC_GPIO_CFG=0x00)

        # GPIO expander 1 @ I2C address (0x44)
        self.gpio_1 = NC_GPIO(intf, conf, NC_CHIP_ADDR=0x44, NC_GPIO_CFG=0x00)

        # ADG729 switch 0 @ I2C address (0x8B)
        self.adg_0 = ADG729(intf, conf, NC_CHIP_ADDR=0x8B)

        # ADG729 switch 1 @ I2C address (0x89)
        self.adg_1 = ADG729(intf, conf, NC_CHIP_ADDR=0x89)

    def init(self):

        # Initialising the boards
        self.gpio_0.init()
        self.gpio_1.init()

        self.adg_0.init()
        self.adg_1.init()

        # Writing to registers of GPIO expander 0
        self.write_gpio_expander('MUX_SEL', 3)

        # Writing to registers of GPIO expander 1
        self.write_gpio_expander('IREF_TRIM', 7)
        self.write_gpio_expander('BYPASS', 0)
        self.write_gpio_expander('EN_VDD_SHUNT', 0)

        # Writing to registers of ADG729 switch 0
        self.write_gpio_expander('REXTA', 0)
        self.write_gpio_expander('REXTD', 0)

        # Writing to registers of ADG729 switch 1
        self.write_gpio_expander('ROFS', 0)
        self.write_gpio_expander('ROFS_LP', 0)

    def _calculate_mask(self, regname):
        ''' Calculate the mask for the appropriate bits of the 8bit register '''
        mask = (pow(2, self._gpio_expander_map[regname][2]) - 1) << self._gpio_expander_map[regname][1]
        return mask

    def write_gpio_expander(self, regname, value):
        ''' Choose the appropriate register to WRITE '''
        time.sleep(0.1)
        val = (value << self._gpio_expander_map[regname][1])
        mask = self._calculate_mask(regname)
        self.logger.debug('write_gpio_expander: mask: %s' % format(mask, '#010b'))
        self.logger.debug('write_gpio_expander: value: %s, shifted value: %s (%s)' % (hex(value), hex(val), format(val, '#010b')))

        # select the chip
        which_gpio_chip = self._gpio_expander_map[regname][0]
        if which_gpio_chip == 0:
            self.gpio_0._modify_register(mask, val)
        elif which_gpio_chip == 1:

            # Modify the registers configured as "open drain"
            if (self._gpio_expander_map[regname][3] == 1):
                self.gpio_1._modify_register(mask, 0)
                self.gpio_1._modify_register_open_drain(mask, val)
            else:
                self.gpio_1._modify_register(mask, val)

        elif which_gpio_chip == 2:
            self.adg_0._modify_register(mask, val)
        elif which_gpio_chip == 3:
            self.adg_1._modify_register(mask, val)
        else:
            self.logger.error('Invalid GPIO expander chip selected')
        time.sleep(0.1)

    def read_gpio_expander(self, regname):
        ''' Choose the appropriate register to READ '''
        # select the chip
        which_gpio_chip = self._gpio_expander_map[regname][0]
        mask = self._calculate_mask(regname)
        if which_gpio_chip == 0:
            value = self.gpio_0._read_port(mask)
        elif which_gpio_chip == 1:
            value = self.gpio_1._read_port(mask)
        elif which_gpio_chip == 2:
            value = self.adg_0._read_port(mask)
        elif which_gpio_chip == 3:
            value = self.adg_1._read_port(mask)
        else:
            self.logger.error('Invalid GPIO expander chip selected')
        self.logger.debug('read_gpio_expander: mask: %s' % format(mask, '#010b'))
        self.logger.debug('read_gpio_expander: value: %s' % format(value, '#010b'))

        # realign bits
        value = value >> self._gpio_expander_map[regname][1]
        self.logger.debug('read_gpio_expander: value: %s (%s)' % (hex(value), format(value, '#010b')))
        return value
