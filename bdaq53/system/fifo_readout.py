#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import sys
import datetime
import threading
import queue
import numpy as np

from threading import Thread, Lock
from collections import deque
from queue import Queue, Empty
from time import sleep, time, mktime

from bdaq53.system import logger
from bdaq53.analysis import analysis_utils as au


data_iterable = ("data", "timestamp_start", "timestamp_stop", "error")


class FifoError(Exception):
    pass


class SoftError(Exception):  # happens more often and can be ignored
    pass


class RxSyncError(FifoError):  # no aurora sync
    pass


class HardError(FifoError):  # aurora hard error
    pass


class FifoDiscardError(FifoError):  # fpga fifo overflowing --> data loss
    pass


class NoDataTimeout(FifoError):
    pass


class StopTimeout(FifoError):
    pass


class FifoReadout():
    ''' Continuous polling to get the FIFO data.

        Seperate watchdog to check the receiver status for all reveivers.
        Seperate process to filter the raw data by receiver id.
    '''

    def __init__(self, bdaq):
        self.log = logger.setup_derived_logger('FIFO Readout')

        self.bdaq = bdaq

        self.readout_thread = None
        self.worker_thread = None
        self.watchdog_thread = None
        self.errback = None

        self.stopped_filter_readout = threading.Event()
        self.stop_readout = threading.Event()
        self.force_stop = threading.Event()

        self.readout_interval = 0.05  # 20 Hz

        # Stuff for calculating rate of readout words per time
        self._moving_average_time_period = 10.0
        self._words_per_read = deque(maxlen=int(self._moving_average_time_period / self.readout_interval))
        self._calculate_word_rate = threading.Event()
        self._word_rate_result = Queue(maxsize=1)

        # Total number of received words
        self._record_count = 0

        self._is_running = False

        # self._clear_channels_next_readout = True

        self.timestamp = None
        self.update_timestamp()

        self.reset_rx()
        self.reset_sram_fifo()

        self.callback = None  # callback function for data (callback usually stores raw data in scan base)
        self.channels = []  # receiver channels to use (e.g. rx0, RX1, ...)
        self.fill_buffer = False
        self._data_buffers = {}
        self.data_buffer_lock = Lock()

    def reset_channels(self):
        self.channels = []

    def attach_channel(self, channel):
        ''' Add a receiver channel to be used

            channel: string, e.g. 'rx0'
        '''
        self.channels.append(channel)
        if channel not in self._data_buffers:
            with self.data_buffer_lock:
                self._data_buffers[channel] = np.array([], np.uint32)

    def set_callback(self, callback):
        ''' Set the callback to be called with data from a receiver channel (e.g. rx0) '''
        self.callback = callback

    def get_data_buffer(self, receiver):
        ''' Return and reset data buffer '''
        with self.data_buffer_lock:
            ret = np.copy(self._data_buffers[receiver])
            self._data_buffers[receiver] = np.array([], np.uint32)
        return ret

    def start(self, errback=None, reset_rx=False, reset_sram_fifo=False, no_data_timeout=None, fill_buffer=False):
        if self._is_running:
            raise RuntimeError('FIFO readout is already running.')

        # self._clear_channels_next_readout = True

        self.errback = errback

        self.fill_buffer = fill_buffer

        self.log.debug('Starting main FIFO readout...')

        if reset_rx:
            self.reset_rx(channels=self.channels)
        if reset_sram_fifo:
            self.reset_sram_fifo()
        else:
            fifo_size = self.bdaq['FIFO']['FIFO_SIZE']
            if fifo_size != 0:
                self.log.warning('Non empty FIFO not cleared when starting FIFO readout: size = %i', fifo_size)

        self._record_count = 0
        self._words_per_read.clear()

        # Clear queue
        self._data_queue = queue.Queue()

        # Reset events used to control the readout thread externally
        self.stopped_filter_readout.clear()
        self.stop_readout.clear()
        self.force_stop.clear()

        if self.errback:
            self.watchdog_thread = Thread(target=self.watchdog, name='WatchdogThread')
            self.watchdog_thread.daemon = True
            self.watchdog_thread.start()

        # Seperate thread to filter FIFO raw data by receiver channel
        # If too slow should be changed to seperate process
        self.filter_process = Thread(target=self.filter_readout_data, name='ReadoutProcess', args=(self._data_queue, self.stopped_filter_readout, ))
        self.filter_process.daemon = True
        self.filter_process.start()

        # Seperate thread polling data from FIFO
        self.readout_thread = Thread(target=self.readout, name='ReadoutThread', kwargs={'no_data_timeout': no_data_timeout})
        self.readout_thread.daemon = True
        self.readout_thread.start()

        self._is_running = True

    def stop(self, timeout=10.0):
        if not self._is_running:
            raise RuntimeError('Readout not running: use start() before stop()')

        self.stop_readout.set()
        try:
            self.readout_thread.join(timeout=timeout)
            if self.readout_thread.is_alive():
                if timeout:
                    raise StopTimeout('FIFO stop timeout after %0.1f second(s)' % timeout)
                else:
                    self.log.warning('FIFO stop timeout')
        except StopTimeout as e:
            self.force_stop.set()
            if self.errback:
                self.errback(sys.exc_info())
            else:
                self.log.error(e)
            self.readout_thread.join()
            del self.readout_thread

        # Close filter process
        self.stopped_filter_readout.wait()
        self.filter_process.join()
        del self.filter_process

        if self.errback:
            self.watchdog_thread.join()

        del self._data_queue

        self.errback = None  # callback for errors
        self._is_running = False

        self.log.debug('Stopped main FIFO readout')

    def print_readout_status(self):
        sync_status = self.get_rx_sync_status()
        discard_count = self.get_rx_fifo_discard_count()
        soft_error_count = self.get_rx_soft_error_count()
        hard_error_count = self.get_rx_hard_error_count()

        if not any(self.get_rx_sync_status()) or any(discard_count) or any(soft_error_count) or any(hard_error_count):
            try:
                queue_size = self._data_queue.qsize()
            except AttributeError as e:
                queue_size = 0
            self.log.warning('RX errors detected')
            self.log.warning('Received words:               %d', self._record_count)
            self.log.warning('Data queue size:             %d', queue_size)
            self.log.warning('FIFO size:                   %d', self.bdaq['FIFO']['FIFO_SIZE'])
            self.log.warning('Channel:                     %s', " | ".join([channel.name.rjust(3) for _, channel in sorted(self.bdaq.rx_channels.items())]))
            self.log.warning('RX sync:                     %s', " | ".join(["YES".rjust(3) if status is True else "NO".rjust(3) for status in sync_status]))
            self.log.warning('RX FIFO discard counter:     %s', " | ".join([repr(count).rjust(3) for count in discard_count]))
            self.log.warning('RX soft errors:              %s', " | ".join([repr(count).rjust(3) for count in soft_error_count]))
            self.log.warning('RX hard errors:              %s', " | ".join([repr(count).rjust(3) for count in hard_error_count]))

        return discard_count, soft_error_count, hard_error_count

    def data_words_per_second(self):
        if self._word_rate_result.full():
            self._word_rate_result.get()
        self._calculate_word_rate.set()
        try:
            result = self._word_rate_result.get(timeout=2 * self.readout_interval)
        except Empty:
            self._calculate_word_rate.clear()
            return None
        return result / float(self._moving_average_time_period)

    def filter_readout_data(self, input_queue, stopped_event):
        ''' Runs in seperate process to filter raw data by receiver'''
        stopped_event = stopped_event
        polling_interval = 0.05

        valid_rx_ids = [int(channel[2]) for channel in self.channels]

        # If rx_id of a word matches forward to corresponding callback.
        # If rx_id of a word does not match any of the currently active receivers, call callback for every chip and handle/skip it during analysis.

        try:
            while True:
                try:
                    data = input_queue.get(block=False)
                except Empty:
                    sleep(polling_interval)
                else:
                    if data is None:    # If None then exit
                        break
                    else:
                        for receiver in self.channels:
                            rx_id = int(receiver[2])
                            raw_data = data[0]
                            tdc_id = raw_data & au.TDC_HEADER
                            # Select only certain raw data word for the channel raw data
                            sel = ((raw_data & au.TRIGGER_HEADER == au.TRIGGER_HEADER) |  # Forward all trigger headers to every chip
                                   (tdc_id == au.TDC_ID_0) |  # Forward all TDC headers to every chip
                                   (tdc_id == au.TDC_ID_1) |  # Forward all TDC headers to every chip
                                   (tdc_id == au.TDC_ID_2) |  # Forward all TDC headers to every chip
                                   (tdc_id == au.TDC_ID_3) |  # Forward all TDC headers to every chip
                                   ((raw_data >> 20) & 0xf == rx_id) |  # Forward if rx_id matches (USER_K/AURORA, (also UNKNOWN_WORD))
                                   np.isin(((raw_data >> 20) & 0xf), valid_rx_ids, invert=True))  # Forward to every chip, if rx_id does not match any active receiver (rx_id must be checked in analysis!)
                            filtered_data = raw_data[sel]
                            if self.fill_buffer:
                                with self.data_buffer_lock:
                                    self._data_buffers[receiver] = np.append(self._data_buffers[receiver], filtered_data)
                            if self.callback:
                                self.callback(data_tuple=(filtered_data, data[1], data[2], data[3]), receiver=receiver)

        except KeyboardInterrupt:   # Need to catch KeyboardInterrupt from main process
            pass

        stopped_event.set()

    def readout(self, no_data_timeout=None):
        '''
            Readout thread continuously reading FIFO. Uses read_data() and appends data to self._data_queue

            no_data_timeout:
                number: abort readout of FIFO after specified seconds with no data received
                None: deactivate timeout feature
        '''
        self.log.debug('Starting %s', self.readout_thread.name)
        curr_time = self.get_float_time()
        time_wait = 0.0

        while not self.force_stop.wait(time_wait if time_wait >= 0.0 else 0.0):
            try:
                time_read = time()
                if no_data_timeout and curr_time + no_data_timeout < self.get_float_time():
                    raise NoDataTimeout('Received no data for %0.1f second(s)' % no_data_timeout)
                data = self.read_data()
                n_words = data.shape[0]
                self._record_count += n_words
            except Exception:
                no_data_timeout = None  # Raise exception only once
                if self.errback:
                    self.errback(sys.exc_info())
                else:
                    raise
                if self.stop_readout.is_set():
                    break
            else:  # no exception
                if n_words == 0:
                    if self.stop_readout.is_set():
                        break
                    else:
                        continue
                last_time, curr_time = self.update_timestamp()
                status = 0
                self._data_queue.put((data, last_time, curr_time, status))
                self._words_per_read.append(n_words)
            finally:
                time_wait = self.readout_interval - (time() - time_read)

            if self._calculate_word_rate.is_set():
                self._calculate_word_rate.clear()
                self._word_rate_result.put(sum(self._words_per_read))

        self._data_queue.put(None)    # Last item, None will stop filter process and callback threads

        self.log.debug('Stopped %s', self.readout_thread.name)

    def watchdog(self):
        ''' Runs in seperate thread to check Reveiver status '''
        self.log.debug('Starting %s', self.watchdog_thread.name)
        n_channels = len(self.bdaq.rx_channels)
        n_soft_errors = [0] * n_channels
        n_hard_errors = [0] * n_channels
        while not self.stop_readout.wait(self.readout_interval * 10):
            try:
                if not any(self.get_rx_sync_status()):
                    raise RxSyncError('Aurora sync lost')
                cnt = self.get_rx_hard_error_count()
                if any(cnt) and any(cnt[i] > n_hard_errors[i] for i in range(n_channels)):
                    n_hard_errors = cnt
                    raise HardError('Aurora hard errors detected ', cnt)
                cnt = self.get_rx_soft_error_count()
                if any(cnt) and any(cnt[i] > n_soft_errors[i] for i in range(n_channels)):
                    n_soft_errors = cnt
                    raise SoftError('Aurora soft errors detected ', cnt)
                cnt = self.get_rx_fifo_discard_count()
                if any(cnt):
                    raise FifoDiscardError('RX FIFO discard error(s) detected ', cnt)
            except Exception:  # called on RX exceptions
                self.errback(sys.exc_info())
        self.log.debug('Stopped %s', self.watchdog_thread.name)

    def read_data(self):
        '''
            Read FIFO and return data array
            Can be used without threading.

            Returns
            ----------
            data : list
                    A list of FIFO data words.
        '''
        return self.bdaq['FIFO'].get_data()

    def update_timestamp(self):
        curr_time = self.get_float_time()
        last_time = self.timestamp
        self.timestamp = curr_time
        return last_time, curr_time

    def get_float_time(self):
        '''
            Returns time as double precision floats - Time64 in pytables - mapping to and from python datetime's
        '''
        t1 = time()
        t2 = datetime.datetime.fromtimestamp(t1)
        return mktime(t2.timetuple()) + 1e-6 * t2.microsecond

    def reset_sram_fifo(self):
        fifo_size = self.bdaq['FIFO']['FIFO_SIZE']
        self.log.debug('Resetting FIFO: size = %i', fifo_size)
        self.update_timestamp()
        self.bdaq['FIFO']['RESET']
        sleep(0.01)     # Sleep here for a while
        fifo_size = self.bdaq['FIFO']['FIFO_SIZE']
        if fifo_size != 0:
            self.log.warning('FIFO not empty after reset: size = %i', fifo_size)

    def reset_rx(self, channels=None):
        self.log.debug('Resetting RX')
        if channels:
            [channel for channel in channels if self.bdaq.rx_channels[channel].reset_logic()]
        else:
            [rx for _, rx in self.bdaq.rx_channels.items() if rx.reset_logic()]
        sleep(0.1)  # Sleep here for a while

    def get_rx_sync_status(self, rx_channel=None):
        if rx_channel is None:
            return [True if rx.RX_READY else False for _, rx in sorted(self.bdaq.rx_channels.items())]
        else:
            return self.bdaq.rx_channels[rx_channel].RX_READY

    def get_rx_fifo_discard_count(self, rx_channel=None):
        if rx_channel is None:
            return [rx.LOST_COUNT for _, rx in sorted(self.bdaq.rx_channels.items())]
        else:
            return self.bdaq.rx_channels[rx_channel].LOST_COUNT

    def get_rx_soft_error_count(self, rx_channel=None):
        if rx_channel is None:
            return [rx.SOFT_ERROR_COUNTER for _, rx in sorted(self.bdaq.rx_channels.items())]
        else:
            return self.bdaq.rx_channels[rx_channel].SOFT_ERROR_COUNTER

    def get_rx_hard_error_count(self, channels=None, rx_channel=None):
        if rx_channel is None:
            return [rx.HARD_ERROR_COUNTER for _, rx in sorted(self.bdaq.rx_channels.items())]
        else:
            return self.bdaq.rx_channels[rx_channel].HARD_ERROR_COUNTER
