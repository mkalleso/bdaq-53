#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import time
import yaml
import os

from bdaq53.chips.chip_base import RegisterObject
from bdaq53.chips.ITkPixV1 import ITkPixV1, ITkPixV1MaskObject, ITkPixV1Calibration
from bdaq53.system import logger as logger

FLAVOR_COLS = {'LIN': range(0, 432)}


def get_flavor(col):
    for fe, cols in FLAVOR_COLS.items():
        if col in cols:
            return fe


def get_tdac_range(fe):
    return 0, 32, 32, 1


class CROCv1(ITkPixV1):
    '''
    Main class for CROCv1 chip
    '''

    flavor_cols = FLAVOR_COLS

    def __init__(self, bdaq, chip_sn='0x0000', chip_id=0, receiver='rx0', config=None):
        self.log = logger.setup_derived_logger('CROCv1 - ' + chip_sn)
        self.bdaq = bdaq
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        if chip_id not in range(16):
            raise ValueError("Invalid Chip ID %r, use integer 0-15" % chip_id)

        self.chip_type = 'CROCv1'
        self.chip_sn = chip_sn
        self.chip_id = chip_id
        self.receiver = receiver

        if config is None or len(config) == 0:
            self.log.warning("No explicit configuration supplied. Using 'CROCv1_default.cfg.yaml'!")
            with open(os.path.join(os.path.dirname(__file__), 'chips', 'CROCv1_default.cfg.yaml'), 'r') as f:
                self.configuration = yaml.full_load(f)
        elif isinstance(config, dict):
            self.configuration = config
        elif isinstance(config, str) and os.path.isfile(config):
            with open(config) as f:
                self.configuration = yaml.full_load(f)
        else:
            raise TypeError('Supplied config has unknown format!')

        self.registers = RegisterObject(self, 'CROCv1_registers.yaml')

        masks = {'enable': {'default': False},
                 'injection': {'default': False},
                 'hitbus': {'default': False},
                 'tdac': {'default': 0},
                 'lin_gain_sel': {'default': True},
                 'injection_delay': {'default': 351}}
        self.masks = ITkPixV1MaskObject(self, masks, (432, 336))

        # Load disabled pixels from chip config
        if 'disable' in self.configuration.keys():
            for pix in self.configuration['disable']:
                self.masks.disable_mask[pix[0], pix[1]] = False

        self.calibration = ITkPixV1Calibration(self.configuration['calibration'])

        self.ptot_enabled = False  # Flag to indicate if PTOT mode is enabled
        self.reset_tot_latches = self.configuration.get('reset_tot_latches', False)

    def init_communication(self, repetitions=1000, write_chip_reset=True):
        self.log.info('Initializing communication...')
        # Configure cmd encoder. TODO: why is this not needed?
        # self.bdaq['cmd'].reset()
        self.bdaq.disable_auto_sync()
        self.bdaq.set_chip_type_ITkPixV1()
        if write_chip_reset:
            self._write_reset(write=True, repetitions=repetitions)
        self.write_sync_01(write=True, repetitions=repetitions)
        self.write_sync(write=True, repetitions=repetitions * 2)
        self.bdaq.enable_auto_sync()
        self.write_ecr()

        # Prepare chip
        self.use_programmed_configuration()  # Switch to programmed configuration in order to programm registers other than the default
        self.write_trimbits()  # Make sure that trimbits are written
        self.registers['DataMerging'].write(0b000001000000)  # Switch chip to 1.28 GHz and disable chip id. FIXME: Raw data analysis needs to know that!
        # self.registers['DataConcentratorConf'].write(0b00000010000)  # Switch chip to 1.28 GHz and disable chip id. FIXME: Raw data analysis needs to know that!
        self.registers['RingOscConfig'].write(0x7fff)
        self.registers['RingOscConfig'].write(0x5eff)

        # Wait for PLL lock
        self.bdaq.wait_for_pll_lock()
        self.setup_aurora(tx_lanes=self.bdaq.rx_lanes[self.receiver], bypass_mode=self.bdaq.configuration['hardware']['bypass_mode'],
                          receiver=self.receiver, lane_test=self.configuration.get('lane_test', False))

        # Workaround for locking problems.
        for _ in range(30):
            self.bdaq.set_chip_type_ITkPixV1()
            self.write_command(self.write_sync(write=False) * 32)
            self.write_ecr()
            try:
                self.bdaq.wait_for_aurora_sync()
            except RuntimeError:
                pass
            else:
                break

            time.sleep(0.01)
        else:
            self.bdaq.wait_for_aurora_sync()
        self.log.success('Communication established')

if __name__ == '__main__':
    CROCv1_chip = CROCv1()
    CROCv1_chip.init()
