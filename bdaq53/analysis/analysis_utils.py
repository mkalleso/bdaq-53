#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import ast
import math
import logging
import tables as tb
import multiprocessing as mp
from functools import partial
import warnings

from scipy.special import erf
from scipy.optimize import curve_fit, OptimizeWarning
import numba
import numpy as np
from tqdm import tqdm

logger = logging.getLogger('Analysis')

# Word defines
TRIGGER_HEADER = 0x80000000
HEADER = 0xF0000000  # header for FPGA word types (TDC, TLU, AURORA)
AURORA_HEADER = 0xFFFF0000  # high word header for AURORA words
AURORA_HEADER_RXID_MASK = 0xFF0FFFFF  # AURORA RX_ID encoded in AURORA FPGA header
# Created from FPGA depending on word type
USERK_FRAME_ID = 0x01000000
HEADER_ID = 0x00010000
# Created by FPGA: trigger/TDC module words
TDC_HEADER = 0x70000000
TDC_ID_0 = 0x10000000
TDC_ID_1 = 0x20000000
TDC_ID_2 = 0x30000000
TDC_ID_3 = 0x40000000

# Data Masks
BCID_MASK = 0x7FFF
TRG_MASK = 0x7FFFFFFF  # Trigger data (number and/or time stamp)
TDC_TRIG_DIST_MASK = 0x0FF00000
TDC_TIMESTAMP_MASK = 0x0FFFF000
TDC_VALUE_MASK = 0x00000FFF


# Event status bits
E_USER_K = 0x00000001  # event has user K words
E_EXT_TRG = 0x00000002  # event has trigger word from RO system
E_TDC = 0x00000004  # event has TDC word(s) from RO system
E_BCID_INC_ERROR = 0x00000008  # BCID does not increase as expected
E_TRG_ID_INC_ERROR = 0x00000010  # TRG ID does not increase by 1
E_NOT_USED = 0x00000020  # not used event error
E_EVENT_TRUNC = 0x00000040  # event data interpretation aborted
E_UNKNOWN_WORD = 0x00000080  # unknown word occured
# Event structure wrong (hit before header or number of data header wrong)
E_STRUCT_WRONG = 0x00000100
E_EXT_TRG_ERR = 0x00000200  # event has external trigger number increase error
E_INV_RX_ID = 0x00000400  # receiver ID encoded in user K word or data word does not match receiver ID of this chip

# Status bit for TDC; keep this separate in order to set it for each TDC line (per hit)
H_HAS_TDC = 0x00000001  # Hit has TDC word
H_TDC_OVF = 0x00000002  # TDC overflow
H_TDC_AMBIGUOUS = 0x00000004  # unique TDC hit assignment impossible
H_TDC_ERR = 0x00000008  # TDC error


@numba.njit
def is_tdc_word(word):
    # Check if word is TDC word from any TDC line
    return ((word & TDC_HEADER == TDC_ID_0) or (word & TDC_HEADER == TDC_ID_1) or
            (word & TDC_HEADER == TDC_ID_2) or (word & TDC_HEADER == TDC_ID_3))


class ConfigDict(dict):
    ''' Dictionary with different value data types:
        str / int / float / list / tuple depending on value

        key can be string or byte-array. Contructor can
        be called with all data types.

        If cast of object to known type is not possible the
        string representation is returned as fallback
    '''

    def __init__(self, *args):
        for key, value in dict(*args).items():
            self.__setitem__(key, value)

    def __setitem__(self, key, val):
        # Change types on value setting
        key, val = self._type_cast(key, val)
        dict.__setitem__(self, key, val)

    def _type_cast(self, key, val):
        ''' Return python objects '''
        # Some data is in binary array representation (e.g. pytable data)
        # These must be convertet to string
        if isinstance(key, (bytes, bytearray)):
            key = key.decode()
        if isinstance(val, (bytes, bytearray)):
            val = val.decode()
        if 'chip_sn' in key:
            return key, val
        try:
            if isinstance(val, np.generic):
                return key, val.item()
            return key, ast.literal_eval(val)
        except (ValueError, SyntaxError):  # fallback to return the object
            return key, val


def scurve(x, A, mu, sigma):
    return 0.5 * A * erf((x - mu) / (np.sqrt(2) * sigma)) + 0.5 * A


def zcurve(x, A, mu, sigma):
    return -0.5 * A * erf((x - mu) / (np.sqrt(2) * sigma)) + 0.5 * A


@numba.njit
def translate_mapping(core_column, region, pixel_id):
    '''
        Translate mapping between raw data format (core column and region) and
        absolute column and row.

        ----------
        Parameters:
            core_column : int
                Core column number [0:49]
            region : int
                Region in the core column [0:383]
            pixel_id : int
                Pixel in the region [0:3]

        Returns:
            column : int
                Absolute column number [0:399]
            row : int
                Absolute row number [0:191]
    '''

    if pixel_id > 3:
        raise ValueError('pixel_id cannot be larger than 3!')
    column = core_column * 8 + pixel_id
    if region % 2 == 1:
        column += 4
    row = int(region / 2)

    return column, row


@numba.njit
def get_pixel_id(column, row):
    ''' Translates absolute column and row number into
        pixel id which corresponds to the index of the hitor line
        connected to the column and row number. So far only working
        for 50 x 50 um pixels.

        ----------
        Parameters:
            column : int
                Absolute column number [0:399]
            row : int
                Absolute row number [0:191]

        Returns:
            pixel_id : int
                Pixel ID indicated to which Hitor line
                the pixel (column, row) is conected.
    '''

    if np.mod(row, 2) == 0:  # even row
        index = np.mod(column + 2, 4)  # map into 4 x 4 unit cell
    else:  # odd row
        index = np.mod(column, 4)  # map into 4 x 4 unit cell

    if np.mod(column, 2) == 0:  # even column
        index += 1
    else:
        index -= 1

    return index


# Has to be defined to make numba overwrite it
def build_event(hits, data_out_i, hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id,
                hist_event_status, hist_tdc_status, hist_tdc_value, hist_ptot, hist_ptoa, hit_buffer, hit_buffer_i,
                start_bcid, scan_param_id, event_status, tdc_status):
    pass


# Overload build_event function with the new function build_event_jitted. This is needed in order to
# overcome the problem when type-checking the hit_buffer array (dtype depends on analysis option).
@numba.extending.overload(build_event, jit_options={'cache': True})
def build_event_jitted(hits, data_out_i, hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id,
                       hist_event_status, hist_tdc_status, hist_tdc_value, hist_ptot, hist_ptoa, hit_buffer, hit_buffer_i,
                       start_bcid, scan_param_id, event_status, tdc_status):
    '''
        Fill result data structures (hit array and histograms) from hit buffer.

        To be called at the end of one event.
    '''
    def _build_event(hits, data_out_i, hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id,
                     hist_event_status, hist_tdc_status, hist_tdc_value, hist_ptot, hist_ptoa, hit_buffer, hit_buffer_i,
                     start_bcid, scan_param_id, event_status, tdc_status):
        # Copy hits from buffer to result data structure
        for i in range(hit_buffer_i):
            hits[data_out_i] = hit_buffer[i]
            # Set relative BCID of event hits
            if start_bcid < hit_buffer[i]['bcid']:
                hits[data_out_i]['rel_bcid'] = hit_buffer[i]['bcid'] - start_bcid
            else:  # Overflow of 15-bit bcid
                hits[data_out_i]['rel_bcid'] = hit_buffer[i]['bcid'] + 2**15 - start_bcid

            col, row = hits[data_out_i]['col'], hits[data_out_i]['row']
            if col < hist_occ.shape[0] and row < hist_occ.shape[1]:
                if hits[data_out_i]['trigger_id'] < 32:
                    hist_trigger_id[col, row, scan_param_id, hits[data_out_i]['trigger_id']] += 1
                # Only set relative BCID of events where event building worked
                if (not (event_status & E_BCID_INC_ERROR) and
                    not (event_status & E_TRG_ID_INC_ERROR) and
                        not (event_status & E_STRUCT_WRONG)):
                    if hits[data_out_i]['rel_bcid'] < 32:
                        hist_rel_bcid[col, row, scan_param_id, hits[data_out_i]['rel_bcid']] += 1

                hits[data_out_i]['scan_param_id'] = scan_param_id
                hits[data_out_i]['event_status'] = event_status
                # Fill histograms
                hist_occ[col, row, scan_param_id] += 1
                hist_tot[col, row, scan_param_id, hits[data_out_i]['tot']] += 1

            data_out_i += 1

        for i in range(32):
            if event_status & (0b1 << i):
                hist_event_status[i] += 1

        return data_out_i

    def _build_event_tdc(hits, data_out_i, hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id,
                         hist_event_status, hist_tdc_status, hist_tdc_value, hist_ptot, hist_ptoa, hit_buffer, hit_buffer_i,
                         start_bcid, scan_param_id, event_status, tdc_status):
        # Copy hits from buffer to result data structure
        data_out_i_last_tdc = -1
        for i in range(hit_buffer_i):
            hits[data_out_i] = hit_buffer[i]
            # Set relative BCID of event hits
            if start_bcid < hit_buffer[i]['bcid']:
                hits[data_out_i]['rel_bcid'] = hit_buffer[i]['bcid'] - start_bcid
            else:  # Overflow of 15-bit bcid
                hits[data_out_i]['rel_bcid'] = hit_buffer[i]['bcid'] + 2**15 - start_bcid

            col, row = hits[data_out_i]['col'], hits[data_out_i]['row']
            if hits[data_out_i]['trigger_id'] < 32:
                hist_trigger_id[col, row, scan_param_id, hits[data_out_i]['trigger_id']] += 1
            # Only set relative BCID of events where event building worked
            if (not (event_status & E_BCID_INC_ERROR) and
                not (event_status & E_TRG_ID_INC_ERROR) and
                    not (event_status & E_STRUCT_WRONG)):
                if hits[data_out_i]['rel_bcid'] < 32:
                    hist_rel_bcid[col, row, scan_param_id, hits[data_out_i]['rel_bcid']] += 1

            hits[data_out_i]['scan_param_id'] = scan_param_id
            hits[data_out_i]['event_status'] = event_status
            # Fill histograms
            hist_occ[col, row, scan_param_id] += 1
            hist_tot[col, row, scan_param_id, hits[data_out_i]['tot']] += 1

            # Set TDC status
            pixel_id = get_pixel_id(col, row)
            if tdc_status[pixel_id] & H_HAS_TDC:  # Hitor line has a TDC word
                # Set ambiguous here in case of several hits and only one TDC word
                tdc_status[pixel_id] |= H_TDC_AMBIGUOUS
                # Set also TDC status ambiguous for first pixel hit
                if data_out_i_last_tdc != -1:
                    hits[data_out_i_last_tdc]['tdc_status'] |= H_TDC_AMBIGUOUS
            if hits[data_out_i]['tdc_value'] != 2**16 - 1:
                # In case of valid TDC value, set pixel hit has TDC word
                tdc_status[pixel_id] |= H_HAS_TDC
                # Remember the first hit of each pixel id. Need this index in order to
                # set the corresponding TDC status to ambiguous if more hits with same pixel id are found.
                if not tdc_status[pixel_id] & H_TDC_AMBIGUOUS:
                    data_out_i_last_tdc = data_out_i
            # Write TDC status to hits
            hits[data_out_i]['tdc_status'] = tdc_status[pixel_id]
            # Fill TDC status histogram
            for i in range(4):
                if tdc_status[pixel_id] & (0b1 << i):
                    hist_tdc_status[pixel_id][i] += 1
            # Fill TDC histogram
            if hits[data_out_i]['tdc_value'] < hist_tdc_value.shape[0] - 1:
                hist_tdc_value[hits[data_out_i]['tdc_value']] += 1

            data_out_i += 1

        # Fill event status histogram
        for i in range(32):
            if event_status & (0b1 << i):
                hist_event_status[i] += 1

        return data_out_i

    def _build_event_ptot(hits, data_out_i, hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id,
                          hist_event_status, hist_tdc_status, hist_tdc_value, hist_ptot, hist_ptoa, hit_buffer, hit_buffer_i,
                          start_bcid, scan_param_id, event_status, tdc_status):
        # Copy hits from buffer to result data structure
        for i in range(hit_buffer_i):
            hits[data_out_i] = hit_buffer[i]
            # Set relative BCID of event hits
            if start_bcid < hit_buffer[i]['bcid']:
                hits[data_out_i]['rel_bcid'] = hit_buffer[i]['bcid'] - start_bcid
            else:  # Overflow of 15-bit bcid
                hits[data_out_i]['rel_bcid'] = hit_buffer[i]['bcid'] + 2**15 - start_bcid

            col, row = hits[data_out_i]['col'], hits[data_out_i]['row']
            if hits[data_out_i]['trigger_id'] < 32:
                hist_trigger_id[col, row, scan_param_id, hits[data_out_i]['trigger_id']] += 1
            # Only set relative BCID of events where event building worked
            if (not (event_status & E_BCID_INC_ERROR) and
                not (event_status & E_TRG_ID_INC_ERROR) and
                    not (event_status & E_STRUCT_WRONG)):
                if hits[data_out_i]['rel_bcid'] < 32:
                    hist_rel_bcid[col, row, scan_param_id, hits[data_out_i]['rel_bcid']] += 1

            hits[data_out_i]['scan_param_id'] = scan_param_id
            hits[data_out_i]['event_status'] = event_status
            # Fill histograms
            hist_occ[col, row, scan_param_id] += 1
            hist_tot[col, row, scan_param_id, hits[data_out_i]['tot']] += 1
            hist_ptot[col, row, scan_param_id, hits[data_out_i]['ptot']] += 1
            hist_ptoa[col, row, scan_param_id, hits[data_out_i]['ptoa']] += 1

            data_out_i += 1

        for i in range(32):
            if event_status & (0b1 << i):
                hist_event_status[i] += 1

        return data_out_i

    def _build_event_tdc_and_ptot(hits, data_out_i, hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id,
                                  hist_event_status, hist_tdc_status, hist_tdc_value, hist_ptot, hist_ptoa, hit_buffer, hit_buffer_i,
                                  start_bcid, scan_param_id, event_status, tdc_status):
        # Copy hits from buffer to result data structure
        data_out_i_last_tdc = -1
        for i in range(hit_buffer_i):
            hits[data_out_i] = hit_buffer[i]
            # Set relative BCID of event hits
            if start_bcid < hit_buffer[i]['bcid']:
                hits[data_out_i]['rel_bcid'] = hit_buffer[i]['bcid'] - start_bcid
            else:  # Overflow of 15-bit bcid
                hits[data_out_i]['rel_bcid'] = hit_buffer[i]['bcid'] + 2**15 - start_bcid

            col, row = hits[data_out_i]['col'], hits[data_out_i]['row']
            if hits[data_out_i]['trigger_id'] < 32:
                hist_trigger_id[col, row, scan_param_id, hits[data_out_i]['trigger_id']] += 1
            # Only set relative BCID of events where event building worked
            if (not (event_status & E_BCID_INC_ERROR) and
                not (event_status & E_TRG_ID_INC_ERROR) and
                    not (event_status & E_STRUCT_WRONG)):
                if hits[data_out_i]['rel_bcid'] < 32:
                    hist_rel_bcid[col, row, scan_param_id, hits[data_out_i]['rel_bcid']] += 1

            hits[data_out_i]['scan_param_id'] = scan_param_id
            hits[data_out_i]['event_status'] = event_status
            # Fill histograms
            hist_occ[col, row, scan_param_id] += 1
            hist_tot[col, row, scan_param_id, hits[data_out_i]['tot']] += 1
            hist_ptot[col, row, scan_param_id, hits[data_out_i]['ptot']] += 1
            hist_ptoa[col, row, scan_param_id, hits[data_out_i]['ptoa']] += 1

            # Set TDC status
            pixel_id = get_pixel_id(col, row)
            if tdc_status[pixel_id] & H_HAS_TDC:  # Hitor line has a TDC word
                # Set ambiguous here in case of several hits and only one TDC word
                tdc_status[pixel_id] |= H_TDC_AMBIGUOUS
                # Set also TDC status ambiguous for first pixel hit
                if data_out_i_last_tdc != -1:
                    hits[data_out_i_last_tdc]['tdc_status'] |= H_TDC_AMBIGUOUS
            if hits[data_out_i]['tdc_value'] != 2**16 - 1:
                # In case of valid TDC value, set pixel hit has TDC word
                tdc_status[pixel_id] |= H_HAS_TDC
                # Remember the first hit of each pixel id. Need this index in order to
                # set the corresponding TDC status to ambiguous if more hits with same pixel id are found.
                if not tdc_status[pixel_id] & H_TDC_AMBIGUOUS:
                    data_out_i_last_tdc = data_out_i
            # Write TDC status to hits
            hits[data_out_i]['tdc_status'] = tdc_status[pixel_id]
            # Fill TDC status histogram
            for i in range(4):
                if tdc_status[pixel_id] & (0b1 << i):
                    hist_tdc_status[pixel_id][i] += 1
            # Fill TDC histogram
            if hits[data_out_i]['tdc_value'] < hist_tdc_value.shape[0] - 1:
                hist_tdc_value[hits[data_out_i]['tdc_value']] += 1

            data_out_i += 1

        # Fill event status histogram
        for i in range(32):
            if event_status & (0b1 << i):
                hist_event_status[i] += 1

        return data_out_i

    # Check signiture (dtype of hit_buffer). If TDC (+3) or PTOT (+2) data analysis is enabled have more
    # keys in dtype of hit_buffer.
    if len(hit_buffer.dtype) == 14:
        return _build_event_tdc
    elif len(hit_buffer.dtype) == 13:
        return _build_event_ptot
    elif len(hit_buffer.dtype) == 16:
        return _build_event_tdc_and_ptot
    else:
        return _build_event


# Has to be defined to make numba overwrite it
def fill_buffer(hit_buffer, hit_buffer_i, event_number, trg_number, tdc_value, tdc_timestamp, ptoa, trg_id, bcid,
                trg_tag, col, row, tot, ptot):
    pass


# Overload fill_buffer function with the new function fill_buffer_jitted. This is needed in order to
# overcome the problem when type-checking the hit_buffer array (dtype depends on analysis option).
@numba.extending.overload(fill_buffer, jit_options={'cache': True})
def fill_buffer_jitted(hit_buffer, hit_buffer_i, event_number, trg_number, tdc_value, tdc_timestamp, ptoa, trg_id, bcid,
                       trg_tag, col, row, tot, ptot):
    # Full set of parameters is needed here since number of arguments has to match with overloaded function.
    def fill_hit_buffer(hit_buffer, hit_buffer_i, event_number, trg_number, tdc_value, tdc_timestamp, ptoa, trg_id, bcid,
                        trg_tag, col, row, tot, ptot):
        hit_buffer[hit_buffer_i]['bcid'] = bcid
        hit_buffer[hit_buffer_i]['event_number'] = event_number
        hit_buffer[hit_buffer_i]['ext_trg_number'] = trg_number
        hit_buffer[hit_buffer_i]['trigger_id'] = trg_id
        hit_buffer[hit_buffer_i]['col'] = col
        hit_buffer[hit_buffer_i]['row'] = row
        hit_buffer[hit_buffer_i]['tot'] = tot
        hit_buffer[hit_buffer_i]['trigger_tag'] = trg_tag

    def fill_hit_buffer_tdc(hit_buffer, hit_buffer_i, event_number, trg_number, tdc_value, tdc_timestamp, ptoa, trg_id, bcid,
                            trg_tag, col, row, tot, ptot):
        # Get pixel ID (ID which determines to which Hitor line the actual pixel is connected)
        pixel_id = get_pixel_id(col, row)
        hit_buffer[hit_buffer_i]['bcid'] = bcid
        hit_buffer[hit_buffer_i]['event_number'] = event_number
        hit_buffer[hit_buffer_i]['ext_trg_number'] = trg_number
        hit_buffer[hit_buffer_i]['trigger_id'] = trg_id
        hit_buffer[hit_buffer_i]['col'] = col
        hit_buffer[hit_buffer_i]['row'] = row
        hit_buffer[hit_buffer_i]['tot'] = tot
        hit_buffer[hit_buffer_i]['trigger_tag'] = trg_tag
        hit_buffer[hit_buffer_i]['tdc_value'] = tdc_value[pixel_id]
        hit_buffer[hit_buffer_i]['tdc_timestamp'] = tdc_timestamp[pixel_id]

    def fill_hit_buffer_ptot(hit_buffer, hit_buffer_i, event_number, trg_number, tdc_value, tdc_timestamp, ptoa, trg_id, bcid,
                             trg_tag, col, row, tot, ptot):
        hit_buffer[hit_buffer_i]['bcid'] = bcid
        hit_buffer[hit_buffer_i]['event_number'] = event_number
        hit_buffer[hit_buffer_i]['ext_trg_number'] = trg_number
        hit_buffer[hit_buffer_i]['trigger_id'] = trg_id
        hit_buffer[hit_buffer_i]['col'] = col
        hit_buffer[hit_buffer_i]['row'] = row
        hit_buffer[hit_buffer_i]['tot'] = tot
        hit_buffer[hit_buffer_i]['ptot'] = ptot
        hit_buffer[hit_buffer_i]['ptoa'] = ptoa

    def fill_hit_buffer_tdc_and_ptot(hit_buffer, hit_buffer_i, event_number, trg_number, tdc_value, tdc_timestamp, ptoa, trg_id, bcid,
                                     trg_tag, col, row, tot, ptot):
        # Get pixel ID (ID which determines to which Hitor line the actual pixel is connected)
        pixel_id = get_pixel_id(col, row)
        hit_buffer[hit_buffer_i]['bcid'] = bcid
        hit_buffer[hit_buffer_i]['event_number'] = event_number
        hit_buffer[hit_buffer_i]['ext_trg_number'] = trg_number
        hit_buffer[hit_buffer_i]['trigger_id'] = trg_id
        hit_buffer[hit_buffer_i]['col'] = col
        hit_buffer[hit_buffer_i]['row'] = row
        hit_buffer[hit_buffer_i]['tot'] = tot
        hit_buffer[hit_buffer_i]['ptot'] = ptot
        hit_buffer[hit_buffer_i]['ptoa'] = ptoa
        hit_buffer[hit_buffer_i]['trigger_tag'] = trg_tag
        hit_buffer[hit_buffer_i]['tdc_value'] = tdc_value[pixel_id]
        hit_buffer[hit_buffer_i]['tdc_timestamp'] = tdc_timestamp[pixel_id]

    # Check signiture (dtype of hit_buffer). If TDC (+3) or PTOT (+2) data analysis is enabled have more
    # keys in dtype of hit_buffer.
    if len(hit_buffer.dtype) == 14:
        return fill_hit_buffer_tdc
    elif len(hit_buffer.dtype) == 13:
        return fill_hit_buffer_ptot
    elif len(hit_buffer.dtype) == 16:
        return fill_hit_buffer_tdc_and_ptot
    else:
        return fill_hit_buffer


@numba.njit
def number_of_set_bits(i):
    ''' Count set bits of 32-bit integer

    Variable-precision SWAR algorithm
    '''
    i = i - ((i >> 1) & 0x55555555)
    i = (i & 0x33333333) + ((i >> 2) & 0x33333333)
    return (((i + (i >> 4) & 0xF0F0F0F) * 0x1010101) & 0xffffffff) >> 24


@numba.njit
def reverse_bits(x):
    ''' Reverse bit order of 8-bit integer.
    '''
    x = (x >> 4) | (x << 4)
    x = (((x & 0xaa) >> 1) | ((x & 0x55) << 1))
    x = (((x & 0xcc) >> 2) | ((x & 0x33) << 2))
    return x


@numba.njit
def check_difference(value_1, value_2, bits, delta=1):
    ''' Returns true if value_2 - value_1 == delta

        Treads overflow at of value_2 at 2**bits correctly
    '''
    if value_2 >= value_1:
        if value_2 != value_1 + delta:
            return False
        else:
            return True
    else:
        if value_2 + 2**bits != value_1 + delta:
            return False
        else:
            return True


@numba.njit
def check_max_difference(value_1, value_2, bits, delta=1):
    ''' Returns true if value_2 - value_1 <= delta

        Treads overflow at of value_2 at 2**bits correctly
    '''
    if value_2 >= value_1:
        if value_2 <= value_1 + delta:
            return True
        else:
            return False
    else:
        if value_2 + 2**bits <= value_1 + delta:
            return True
        else:
            return False


@numba.njit(cache=True, fastmath=True)
def rm_msb(word, n_bits, length):
    ''' Helper function to remove highest n_bits.
    '''
    word_buf = word & (2**(length - n_bits) - 1)
    word_buf = word_buf | 2 ** (length - n_bits)  # Make sure that leading one is set
    return word_buf


@numba.njit(cache=True, fastmath=True)
def get_length(word):
    ''' Helper function to get bit-position of highest bit which is 1.
    '''
    ret = 0
    word2 = word
    while word2:
        word2 = word2 >> 1
        ret += 1
    if ret != 0:
        ret -= 1
    return ret


def process_userk(userk_in):
    userk_out = np.zeros(userk_in.shape[0] * 2, dtype={'names': ['Address', 'Data'], 'formats': ['uint16', 'uint16']})
    userk_counter = 0
    for i in userk_in:
        AuroraKWord = i['AuroraKWord']
        if AuroraKWord == 0:
            userk_out[userk_counter]['Address'] = i['Data1_Addr']
            userk_out[userk_counter]['Data'] = i['Data1']

            userk_out[userk_counter + 1]['Address'] = i['Data0_Addr']
            userk_out[userk_counter + 1]['Data'] = i['Data0']

            userk_counter = userk_counter + 2

        if AuroraKWord == 1:
            userk_out[userk_counter]['Address'] = i['Data1_Addr']
            userk_out[userk_counter]['Data'] = i['Data1']

            userk_counter = userk_counter + 1

        if AuroraKWord == 2:
            userk_out[userk_counter]['Address'] = i['Data0_Addr']
            userk_out[userk_counter]['Data'] = i['Data0']

            userk_counter = userk_counter + 1

    userk_out = userk_out[:userk_counter]

    for i in userk_out:
        logger.debug('Address= %s \t\t\tData= %s', hex(i['Address']), hex(i['Data']))

    return userk_out


def init_outs(n_hits, n_scan_params, cols=400, rows=192, analyze_tdc=False, analyze_ptot=False):
    # Prevent arrays > 250 Mb for up to 100 different parameter settings to ease in RAM analysis
    if n_scan_params < 25:
        dtype_large_hist = np.uint32
    elif n_scan_params < 50:
        logger.info('Reduce 4D histogram data type from uint32 to unint16 to reduce memory footprint')
        dtype_large_hist = np.uint16
    else:
        logger.info('Reduce 4D histogram data type from uint32 to unint8 to reduce memory footprint')
        dtype_large_hist = np.uint8
    hist_occ = np.zeros(shape=(cols, rows, n_scan_params), dtype=np.uint32)
    hist_tot = np.zeros(shape=(cols, rows, n_scan_params, 16), dtype=dtype_large_hist)
    hist_rel_bcid = np.zeros(shape=(cols, rows, n_scan_params, 32), dtype=dtype_large_hist)
    hist_trigger_id = np.zeros(shape=(cols, rows, n_scan_params, 32), dtype=dtype_large_hist)
    hist_event_status = np.zeros(shape=(32), dtype=np.uint32)
    hist_tdc_status = np.zeros(shape=(4, 8), dtype=np.uint32)  # for each HitOr line
    hist_tdc_value = np.zeros(shape=(500), dtype=np.uint32)
    hist_bcid_error = np.zeros(shape=(32), dtype=np.uint32)
    hist_ptot = np.zeros(shape=(cols, rows, n_scan_params, 256), dtype=dtype_large_hist)
    hist_ptoa = np.zeros(shape=(cols, rows, n_scan_params, 32), dtype=dtype_large_hist)
    names = ['event_number', 'ext_trg_number', 'trigger_id', 'bcid', 'rel_bcid',
             'col', 'row', 'tot', 'scan_param_id', 'trigger_tag', 'event_status']
    formats = ['int64', 'uint32', 'uint8', 'uint16', 'uint8', 'uint16', 'uint16',
               'uint8', 'uint32', 'uint8', 'uint32']

    if analyze_tdc:
        names.extend(['tdc_value', 'tdc_timestamp', 'tdc_status'])
        formats.extend(['uint16', 'uint16', 'uint8'])
    if analyze_ptot:
        names.extend(['ptot', 'ptoa'])
        formats.extend(['uint16', 'uint8'])

    hits = np.zeros(shape=n_hits, dtype={'names': names, 'formats': formats})

    return hits, hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id, hist_event_status, hist_tdc_status, hist_tdc_value, hist_bcid_error, hist_ptot, hist_ptoa


def get_meta_data_index_at_scan_param_id(scan_parameter_values):
    '''Takes the scan parameter values and returns the indices where the scan parammeter id changes

    Parameters
    ----------
    scan_parameter_values : numpy.recordarray

    Returns
    -------
    numpy.ndarray:
        index where scan parameter value was used first
    '''
    diff = np.concatenate(([1], np.diff(scan_parameter_values)))
    idx = np.concatenate((np.where(diff)[0], [len(scan_parameter_values)]))
    return idx[:-1]


def get_ranges_from_array(arr, append_last=True):
    '''Takes an array and calculates ranges [start, stop[. The last range end is none to keep the same length.

    Parameters
    ----------
    arr : array like
    append_last: bool
        If True, append item with a pair of last array item and None.

    Returns
    -------
    numpy.array
        The array formed by pairs of values by the given array.

    Example
    -------
    >>> a = np.array((1,2,3,4))
    >>> get_ranges_from_array(a, append_last=True)
    array([[1, 2],
           [2, 3],
           [3, 4],
           [4, None]])
    >>> get_ranges_from_array(a, append_last=False)
    array([[1, 2],
           [2, 3],
           [3, 4]])
    '''
    right = arr[1:]
    if append_last:
        left = arr[:]
        right = np.append(right, None)
    else:
        left = arr[:-1]
    return np.column_stack((left, right))


@numba.njit(locals={'cluster_shape': numba.int64})
def calc_cluster_shape(cluster_array):
    '''Boolean 8x8 array to number.
    '''
    cluster_shape = 0
    indices_x, indices_y = np.nonzero(cluster_array)
    for index in np.arange(indices_x.size):
        cluster_shape += 2**xy2d_morton(indices_x[index], indices_y[index])
    return cluster_shape


@numba.njit(numba.int64(numba.uint32, numba.uint32))
def xy2d_morton(x, y):
    ''' Tuple to number.

    See: https://stackoverflow.com/questions/30539347/
         2d-morton-code-encode-decode-64bits
    '''
    x = (x | (x << 16)) & 0x0000FFFF0000FFFF
    x = (x | (x << 8)) & 0x00FF00FF00FF00FF
    x = (x | (x << 4)) & 0x0F0F0F0F0F0F0F0F
    x = (x | (x << 2)) & 0x3333333333333333
    x = (x | (x << 1)) & 0x5555555555555555

    y = (y | (y << 16)) & 0x0000FFFF0000FFFF
    y = (y | (y << 8)) & 0x00FF00FF00FF00FF
    y = (y | (y << 4)) & 0x0F0F0F0F0F0F0F0F
    y = (y | (y << 2)) & 0x3333333333333333
    y = (y | (y << 1)) & 0x5555555555555555

    return x | (y << 1)


def imap_bar(func, args, n_processes=None, unit='it', unit_scale=False):
    ''' Apply function (func) to interable (args) with progressbar
    '''
    p = mp.Pool(n_processes)
    res_list = []
    pbar = tqdm(total=len(args), unit=unit, unit_scale=unit_scale)
    for _, res in enumerate(p.imap(func, args)):
        pbar.update()
        res_list.append(res)
    pbar.close()
    p.close()
    p.join()
    return res_list


def get_threshold(x, y, n_injections):
    ''' Fit less approximation of threshold from s-curve.

        From: https://doi.org/10.1016/j.nima.2013.10.022

        Parameters
        ----------
        x, y : numpy array like
            Data in x and y
        n_injections: integer
            Number of injections
    '''

    # Sum over last dimension to support 1D and 2D hists
    M = y.sum(axis=len(y.shape) - 1)  # is total number of hits
    d = np.diff(x)[0]  # Delta x
    if not np.all(np.diff(x) == d):
        raise NotImplementedError('Threshold can only be calculated for equidistant x values!')
    return x.max() - (d * M).astype(float) / n_injections


def get_noise(x, y, n_injections):
    ''' Fit less approximation of noise from s-curve.

        From: https://doi.org/10.1016/j.nima.2013.10.022

        Parameters
        ----------
        x, y : numpy array like
            Data in x and y
        n_injections: integer
            Number of injections
    '''

    mu = get_threshold(x, y, n_injections)
    d = np.abs(np.diff(x)[0])

    mu1 = y[x < mu].sum()
    mu2 = (n_injections - y[x > mu]).sum()

    return d * (mu1 + mu2).astype(float) / n_injections * np.sqrt(np.pi / 2.)


def fit_scurve(scurve_data, scan_params, n_injections, sigma_0):
    '''
        Fit one pixel data with Scurve.
        Has to be global function for the multiprocessing module.

        Returns:
            (mu, sigma, chi2/ndf)
    '''

    # Typecast to working types
    scurve_data = np.array(scurve_data, dtype=float)
    # Scipy bug: fit does not work on float32 values, without any error message
    scan_params = np.array(scan_params, dtype=float)

    # Deselect masked values (== nan)
    x = scan_params[~np.isnan(scurve_data)]
    y = scurve_data[~np.isnan(scurve_data)]

    # Only fit data that is fittable
    if np.all(y == 0) or np.all(np.isnan(y)) or x.shape[0] < 3:
        return (0., 0., 0.)
    if y.max() < 0.2 * n_injections:
        return (0., 0., 0.)

    # Calculate data errors, Binomial errors
    min_err = np.sqrt(0.5 - 0.5 / n_injections)  # Set arbitrarly to error of 0.5 injections, needed for fit minimizers
    yerr = np.full_like(y, min_err, dtype=float)
    yerr[y <= n_injections] = np.sqrt(y[y <= n_injections] * (1. - y[y <= n_injections].astype(float) / n_injections))  # Binomial errors
    yerr[yerr < min_err] = min_err
    # Additional hits not following fit model set high error
    sel_bad = y > n_injections
    yerr[sel_bad] = (y - n_injections)[sel_bad]

    # Calculate threshold start value:
    mu = get_threshold(x=x, y=y, n_injections=n_injections)

    # Set fit start values
    p0 = [mu, sigma_0]

    # Bounds makes the optimizer 5 times slower and are therefore deactivated.
    # TODO: Maybe we find a better package?
#     bounds = [[x.min() - 5 * np.diff(x)[0], 0.05 * np.min(np.diff(x))],
#               [x.max() + 5 * np.diff(x)[0], x.max() - x.min()]]

    # Special case: step function --> omit fit, set to result
    if not np.any(np.logical_and(y != 0, y != n_injections)):
        # All at n_inj or 0 --> set to mean between extrema
        return (mu + np.min(np.diff(x)) / 2., 0.01 * np.min(np.diff(x)), 1e-6)

    # Special case: Nothing to optimize --> set very good start values
    # Will trigger OptimizeWarning
    if np.count_nonzero(np.logical_and(y != 0, y != n_injections)) == 1:
        # Only one not at n_inj or 0 --> set mean to the point
        idx = np.ravel(np.where(np.logical_and(y != 0, y != n_injections)))[0]
        p0 = (x[idx], 0.1 * np.min(np.diff(x)))

    try:
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", OptimizeWarning)
            popt = curve_fit(f=lambda x, mu, sigma: scurve(x, n_injections, mu, sigma),
                             xdata=x, ydata=y, p0=p0, sigma=yerr,
                             absolute_sigma=True if np.any(yerr) else False)[0]
            chi2 = np.sum((y - scurve(x, n_injections, popt[0], popt[1])) ** 2)
    except RuntimeError:  # fit failed
        return (0., 0., 0.)

    # Treat data that does not follow an S-Curve, every fit result is possible here but not meaningful
    max_threshold = x.max() + 5. * np.abs(popt[1])
    min_threshold = x.min() - 5. * np.abs(popt[1])
    if popt[1] <= 0 or not min_threshold < popt[0] < max_threshold:
        return (0., 0., 0.)

    return (popt[0], popt[1], chi2 / (y.shape[0] - 3 - 1))


def _mask_bad_data(scurve, n_injections):
    ''' This function tries to find the maximum value that is described by an S-Curve
        and maskes all values above.

        Multiple methods are used and the likelyhood that a bad S-Curve can happen
        by chance is valued. Especially these cases are treated:
        1. Additional noisy data
                       *
                      *
        n_inj-     ***
                  *
                 *
          0  - **
        2. Very noisy pixel leading to stuck pixels that see less hits
        n_inj-
                  *
                 * *
          0  - **   *
        3. Double S-Curve
                     *
        n_inj-     **     ***
                  *      *
                 *    * *
          0  - **      *
        4. Noisy data that looks bad but is ok (statistically possible)
        n_inj-          ***
                  * * *
                 * *
          0  - **

        Returns:
        --------
        numpy boolean array as a mask for good settings, True for bad settings
    '''

    scurve_mask = np.ones_like(scurve, dtype=np.bool)

    # Speedup, nothing to do if no slope
    if not np.any(scurve) or np.all(scurve == n_injections):
        return scurve_mask

    # Initialize result to best case (complete range can be used)
    idx_stop = scurve.shape[0]

    # Step 1: Find good maximum setting to restrict the range
    if np.any(scurve == n_injections):  # There is at least one setting seeing all injections
        idcs_stop = np.ravel(np.argwhere(scurve == n_injections))  # setting indices with all injections
        if len(idcs_stop) > 1:  # Several indexes
            # Find last index of the first region at n_injections
            if np.argmin(np.diff(idcs_stop) != 1) != 0:
                idx_stop = idcs_stop[np.argmin(np.diff(idcs_stop) != 1)] + 1
            else:  # Only one settled region, take last index
                idx_stop = idcs_stop[-1] + 1
        else:
            idx_stop = idcs_stop[-1] + 1
        scurve_cut = scurve[:idx_stop]
    elif scurve.max() > n_injections:  # Noisy pixels; no good maximum value; take latest non-noisy setting
        idx_stop = np.ravel(np.argwhere(scurve > n_injections))[0]
        scurve_cut = scurve[:idx_stop]
    else:  # n_injections not reached; scurve not fully recorded or pixel very noisy to have less hits
        scurve_cut = scurve

    # First measurement already with too many hits; no reasonable fit possible
    if idx_stop == 0:
        return scurve_mask

    # Check if first measurement is already noisy (> n_injections or more hits then following stuck settings)
    # Return if very noisy since no fit meaningful possible
    y_idx_sorted = scurve_cut.argsort()  # sort y value indeces to check for monotony
    if y_idx_sorted[0] != 0 and (scurve[0] > n_injections or (scurve[0] - scurve[1]) > 2 * np.sqrt(scurve[0] * (1. - float(scurve[0]) / n_injections))):
        return scurve_mask

    # Step 2: Find first local maximum
    sel = np.r_[True, scurve_cut[1:] >= scurve_cut[:-1]] & np.r_[scurve_cut[:-1] > scurve_cut[1:], True]  # Select local maximum; select last index if flat maximum, flat maximum expected for scurve
    y_max_idcs = np.arange(scurve_cut.shape[0])[sel]
    if np.any(y_max_idcs):  # Check for a maxima
        # Loop over maxima
        for y_max_idx in y_max_idcs:
            y_max = scurve_cut[y_max_idx]
            y_diff = np.diff(scurve_cut.astype(np.int))
            y_dist = (y_max.astype(np.int) - scurve_cut.astype(np.int)).astype(np.int)
            y_dist[y_max_idx + 1:] *= -1
            y_err = np.sqrt(scurve_cut * (1. - scurve_cut.astype(float) / n_injections))
            min_err = np.sqrt(0.5 - 0.5 / n_injections)
            y_err[y_err < min_err] = min_err
            # Only select settings where the slope cannot be explained by statistical fluctuations
            try:
                if np.any(y_diff < -2 * y_err[1:]):
                    idx_stop_diff = np.ravel(np.where(y_diff < -2 * y_err[1:]))[0]
                else:
                    idx_stop_diff = idx_stop
                idx_stop_dist = np.ravel(np.where(y_dist < -2 * y_err))[0]
                idx_stop = min(idx_stop_diff + 1, idx_stop_dist)
                break
            except IndexError:  # No maximum found
                pass

    scurve_mask[:idx_stop] = False

    return scurve_mask


def fit_scurves_multithread(scurves, scan_params, n_injections=None, invert_x=False, optimize_fit_range=False, rows=192):
    ''' Fit Scurves on all available cores in parallel.

        Parameters
        ----------
        scurves: numpy array like
            Histogram with S-Curves. Channel index in the first and data in the second dimension.
        scan_params: array like
            Values used durig S-Curve scanning.
        n_injections: integer
            Number of injections
        invert_x: boolean
            True when x-axis inverted
        optimize_fit_range: boolean
            Reduce fit range of each S-curve independently to the S-Curve like range. Take full
            range if false
    '''

    scan_params = np.array(scan_params)  # Make sure it is numpy array

    if invert_x:
        scan_params *= -1

    if optimize_fit_range:
        scurve_mask = np.ones_like(scurves, dtype=np.bool)  # Mask to specify fit range for all scurves
        for i, scurve in enumerate(scurves):
            scurve_mask[i] = _mask_bad_data(scurve, n_injections)
        scurves_masked = np.ma.masked_array(scurves, scurve_mask)
    else:
        scurves_masked = np.ma.masked_array(scurves)

    # Calculate noise median for better fit start value
    logger.info("Calculate S-curve fit start parameters")
    sigmas = []
    for curve in tqdm(scurves_masked, unit=' S-curves', unit_scale=True):
        # Calculate from pixels with valid data (maximum = n_injections)
        if curve.max() == n_injections:
            if np.all(curve.mask == np.ma.nomask):
                x = scan_params
            else:
                x = scan_params[~curve.mask]

            sigma = get_noise(x=x, y=curve.compressed(), n_injections=n_injections)
            sigmas.append(sigma)
    sigma_0 = np.median(sigmas)
    sigma_0 = np.max([sigma_0, np.diff(scan_params).min() * 0.01])  # Prevent sigma = 0

    logger.info("Start S-curve fit on %d CPU core(s)", mp.cpu_count())
    partialfit_scurve = partial(fit_scurve,
                                scan_params=scan_params,
                                n_injections=n_injections,
                                sigma_0=sigma_0)

    result_list = imap_bar(partialfit_scurve, scurves_masked.tolist(), unit=' Fits', unit_scale=True)  # Masked array entries to list leads to NaNs
    result_array = np.array(result_list)
    logger.info("S-curve fit finished")

    thr = result_array[:, 0]
    if invert_x:
        thr *= -1
    sig = np.abs(result_array[:, 1])
    chi2ndf = result_array[:, 2]
    thr2D = np.reshape(thr, (400, rows))
    sig2D = np.reshape(sig, (400, rows))
    chi2ndf2D = np.reshape(chi2ndf, (400, rows))
    return thr2D, sig2D, chi2ndf2D


def range_of_parameter(meta_data):
    ''' Calculate the raw data word indeces of each scan parameter
    '''
    _, index = np.unique(meta_data['scan_param_id'], return_index=True)
    expected_values = np.arange(np.max(meta_data['scan_param_id']) + 1)

    # Check for scan parameter IDs with no data
    sel = np.isin(expected_values, meta_data['scan_param_id'])
    if not np.all(sel):
        logger.warning('No words for scan parameter IDs: %s', str(expected_values[~sel]))

    start = meta_data[index]['index_start']
    stop = np.append(start[:-1] + np.diff(start), meta_data[-1]['index_stop'])

    return np.column_stack((expected_values[sel], start, stop))


def words_of_parameter(data, meta_data):
    ''' Yield all raw data words of a scan parameter

        Warning:
        --------
            This function can lead to high RAM usage, since no chunking is used.

        Returns:
        --------
            Tuple: (scan parameter ID, data)
    '''

    par_range = range_of_parameter(meta_data)
    for scan_par_id, start, stop in par_range:
        yield scan_par_id, data[start:stop]


@numba.njit
def hist_2d_index(x, y, shape=None):
    ''' Histograms integer indices in 2D.
        Parameters
        ----------
        x, y: numpy array like with unsigned integers, 1 dimension
        shape: tuple with the shape in (x, y)
            If not defined calculated from values
    '''
    if x.shape != y.shape:
        raise ValueError("Array lengths have to be the same")
    if not shape:
        shape = (x.max(), y.max())
    result = np.zeros(shape=shape, dtype=np.uint16)
    for i in range(0, x.shape[0]):
        result[x[i], y[i]] += 1
    return result


@numba.njit
def hist_3d_index(x, y, z, shape=None):
    ''' Histograms integer indices in 3D.
        Parameters
        ----------
        x, y, z: numpy array like with unsigned integers, 1 dimension
        shape: tuple with the shape in (x, y, z)
            If not defined calculated from values
    '''
    if x.shape != y.shape or x.shape != z.shape:
        raise ValueError("Array lengths have to be the same")
    if not shape:
        shape = (x.max(), y.max(), z.max())
    result = np.zeros(shape=shape, dtype=np.uint16)
    for i in range(0, x.shape[0]):
        result[x[i], y[i], z[i]] += 1
    return result


def get_mean_from_histogram(counts, bin_positions, axis=0):
    ''' Compute average of an array that represents a histogram along the specified axis.

        The bin positions are the values and counts the occurences of these values.

        Uses vectorized numpy function without looping and is therefore fast.

        Parameters
        ----------
        counts: Array containing occurences of values to be averaged
        axis: None or int
        bin_positions: array_like associated with the values in counts.
                        Shape of count array or 1D array with shape of axis.
    '''
    weights = bin_positions
    with np.errstate(divide='ignore', invalid='ignore'):
        return np.average(counts, axis=axis, weights=weights) * weights.sum(axis=min(axis, len(weights.shape) - 1)) / np.nansum(counts, axis=axis)


def get_std_from_histogram(counts, bin_positions, axis=0):
    ''' Compute RMS of an array that represents a histogram along the specified axis.

        The bin positions are the values and counts the occurences of these values.

        Uses vectorized numpy function without looping and is therefore fast.

        Parameters
        ----------
        counts: Array containing occurences of values to be averaged
        axis: None or int
        bin_positions: array_like associated with the values in counts.
                        Same shape like count array is needed!
    '''

    if np.any(bin_positions.sum(axis=axis) == 0):
        raise ValueError('The bin position are all 0 for at least one axis. Maybe you forgot to transpose the bin position array?')
    # Mean for each pixel
    mean = get_mean_from_histogram(counts, bin_positions, axis=axis)
    weights = (bin_positions - np.expand_dims(mean, axis=axis)) ** 2
    rms_2 = get_mean_from_histogram(counts, bin_positions=weights, axis=axis)
    return np.sqrt(rms_2)


def hits_of_parameter(hits, reverse_order=False, chunk_size=1000000):
    ''' Yield all hits of a scan parameter.

       Do not exceed chunk_size.

       Warning: only supports monotone increasing scan parameter ids!

       Parameters
       ----------
       hits: pytables tables
       chunk_size: unsigned integer
           Number of hits in RAM
    '''
    offset = 0 if not reverse_order else hits.shape[0]
    min_par = hits[0]["scan_param_id"]
    max_par = hits[-1]["scan_param_id"]
    scan_parameter_selection = range(min_par, max_par + 1)

    for scan_par_id in scan_parameter_selection:
        while True:
            actual_hits = hits[offset:offset + chunk_size]
            sel = actual_hits['scan_param_id'] == scan_par_id
            actual_hits = actual_hits[sel]
            offset += actual_hits.shape[0]

            if len(actual_hits) == 0:
                break

            yield scan_par_id, actual_hits


def copy_configuration_node(infile, outfile):
    '''
        Copy the configuration group from infile to outfile
    '''
    with tb.open_file(outfile, 'w') as out_file:
        with tb.open_file(infile, 'r') as in_file:
            out_file.create_group(out_file.root, name='configuration', title='Configuration')
            out_file.copy_children(in_file.root.configuration, out_file.root.configuration, recursive=True)


def calculate_inl_dnl(data):
    LSB = (data[-1][1] - data[0][1]) / (data[-1][0] - data[0][0])
    x = [i[0] for i in data]
    y = [i[1] for i in data]
    results = np.zeros((len(data) - 1), dtype={'names': ['dac', 'voltage', 'dnl', 'inl'],
                                               'formats': ['uint16', 'float64', 'float64', 'float64']})

    def _lin(x, *p):
        m, b = p
        return m * x + b

    try:
        p0 = (1., 0.)
        coeff, _ = curve_fit(_lin, x, y, p0=p0)
    except RuntimeError:
        coeff = (0, 0)
        logging.error('Linear fit failed!')

    for i in range(len(data) - 1):
        results['dac'][i] = data[i][0]
        results['voltage'][i] = data[i][1]
        results['dnl'][i] = (data[i + 1][1] - data[i][1]) / LSB - 1
        results['inl'][i] = (data[i][1] - _lin(data[i][0], *coeff)) / coeff[0]

    return results


def pixel_hits_prob(n, mu):
    '''
    Returns the Poisson-probability of less than n events if expectation value is mu

    Parameters
    ----------
    mu:
        Poisson expectation value
    n:
        Number of events
    '''
    def part_poisson(x, mu):
        return (mu**x / math.factorial(x))

    if n < 0.:
        return 0.

    result = 0.
    for i in range(0, int(n)):
        result += part_poisson(i, mu)

    if type(n) == float:
        result += part_poisson(int(n), mu) * (n - int(n))

    return result * np.exp(-mu)


def pixel_hits_prob_root_less_n(n, mu, quantile_alpha):
    '''
    Returns the Poisson-probability of less than n events, if expectation value is mu,
    minus the threshold probability alpha (so that one can search for the root as function of the parameters).

    Parameters
    ----------
    mu:
        Poisson expectation value
    n:
        Number of events
    quantile_alpha:
        Threshold probability
    '''
    return pixel_hits_prob(n=n, mu=mu) - quantile_alpha


def pixel_hits_prob_root_more_n(n, mu, quantile_alpha):
    '''
    Returns the Poisson-probability of more than n events, if expectation value is mu,
    minus the threshold probability alpha (so that one can search for the root as function of the parameters).

    Parameters
    ----------
    mu:
        Poisson expectation value
    n:
        Number of events
    quantile_alpha:
        Threshold probability
    '''
    return (1 - pixel_hits_prob(n=n, mu=mu)) - quantile_alpha


if __name__ == '__main__':
    pass
