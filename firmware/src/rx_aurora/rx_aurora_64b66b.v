/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps/1ps
`default_nettype wire

`include "rx_aurora_64b66b_core.v"


module rx_aurora_64b66b
#(
    parameter BASEADDR = 16'h0000,
    parameter HIGHADDR = 16'h0000,
    parameter ABUSWIDTH = 16,
    parameter IDENTIFIER = 0,
    parameter AURORA_CHANNELS = 1
)(
    input wire [1:0] CHIP_TYPE,
    input wire BUS_CLK,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    inout wire [7:0] BUS_DATA,
    input wire BUS_RST,
    input wire BUS_WR,
    input wire BUS_RD,

    input wire [3:0] RXP, RXN,
    output wire RX_CLK,

    input wire GTXQ0_P, GTXQ0_N,
    input wire INIT_CLK_IN,
    input wire DRP_CLK_IN,
    output wire REFCLK_OUT,

    input wire FIFO_READ,
    output wire FIFO_EMPTY,
    output wire [31:0] FIFO_DATA,

    output wire LOST_ERROR,
    output wire RX_SOFT_ERROR,
    output wire RX_HARD_ERROR,
    output wire RX_LANE_UP,
    output wire [3:0] RX_MULTILLANE_UP,
    output wire RX_CHANNEL_UP,
    output wire PLL_LOCKED,

    input wire AURORA_RESET
);


wire [3:0] RXP_shifted, RXN_shifted;

`ifdef _4LANE
    localparam AURORA_NR_OF_LANES = 4;
`elsif _2LANE
    localparam AURORA_NR_OF_LANES = 2;
`elsif _3LANE
    localparam AURORA_NR_OF_LANES = 3;
`elsif _1LANE
    localparam AURORA_NR_OF_LANES = 1;
`endif


// change lane order
genvar i;
generate
    for (i=0; i < AURORA_NR_OF_LANES; i=i+1) begin
        assign RXP_shifted[i] = RXP[4-AURORA_NR_OF_LANES+i];  // 2lane: 0:2, 1:3. 4lane: 0:0, 1:1, ...
        assign RXN_shifted[i] = RXN[4-AURORA_NR_OF_LANES+i];
    end
endgenerate


wire IP_RD, IP_WR;
wire [ABUSWIDTH-1:0] IP_ADD;
wire [7:0] IP_DATA_IN;
wire [7:0] IP_DATA_OUT;

bus_to_ip #( .BASEADDR(BASEADDR), .HIGHADDR(HIGHADDR), .ABUSWIDTH(ABUSWIDTH) ) i_bus_to_ip
(
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),

    .IP_RD(IP_RD),
    .IP_WR(IP_WR),
    .IP_ADD(IP_ADD),
    .IP_DATA_IN(IP_DATA_IN),
    .IP_DATA_OUT(IP_DATA_OUT)
);


localparam ARB_WIDTH = 2;

wire FIFO_USER_K_EMPTY;
wire [31:0] FIFO_USER_K_DATA;

wire ARB_READY_OUT;
wire ARB_WRITE_OUT;
wire ARB_GRANT_USER_K, ARB_GRANT_DATA;
wire [31:0] ARB_DATA_OUT, FIFO_DATA_DATA;
wire FIFO_DATA_EMPTY;

// map FIFO signals to arbiter
assign ARB_READY_OUT = FIFO_READ;
assign FIFO_DATA = {ARB_GRANT_USER_K, ARB_DATA_OUT[23:0]};    //  8 bits of additional IDs, flags etc | 8 bits frame information | 16 bits of data,
assign FIFO_EMPTY = ~ARB_WRITE_OUT;

rrp_arbiter
#(
    .WIDTH(ARB_WIDTH)
) i_rrp_arbiter
(
    .RST(BUS_RST),
    .CLK(BUS_CLK),

    .WRITE_REQ({!FIFO_USER_K_EMPTY, !FIFO_DATA_EMPTY}), // indicate will of writing data
    .HOLD_REQ({2'b00}),                                 // wait for writing for given stream (priority)
    .DATA_IN({FIFO_USER_K_DATA, FIFO_DATA_DATA}),       // incoming data for arbitration
    .READ_GRANT({ARB_GRANT_USER_K, ARB_GRANT_DATA}),    // indicate to stream that data has been accepted

    .READY_OUT(ARB_READY_OUT),              // indicates ready for outgoing stream
    .WRITE_OUT(ARB_WRITE_OUT),              // indicates will of write to outgoing stream
    .DATA_OUT(ARB_DATA_OUT)                 // outgoing data stream
    );


rx_aurora_64b66b_core
#(
    .ABUSWIDTH(ABUSWIDTH),
    .IDENTIFIER(IDENTIFIER),
    .AURORA_LANES(AURORA_NR_OF_LANES)
) i_aurora_rx_core
(
    .CHIP_TYPE(CHIP_TYPE),
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(IP_ADD),
    .BUS_DATA_IN(IP_DATA_IN),
    .BUS_RD(IP_RD),
    .BUS_WR(IP_WR),
    .BUS_DATA_OUT(IP_DATA_OUT),

    .RXP(RXP_shifted),
    .RXN(RXN_shifted),
    .RX_CLK(RX_CLK),

    .GTXQ0_P(GTXQ0_P),
    .GTXQ0_N(GTXQ0_N),

    .INIT_CLK(INIT_CLK_IN),

    .DRP_CLK_IN(DRP_CLK_IN),

    .REFCLK_OUT(REFCLK_OUT),

    .FIFO_READ(ARB_GRANT_DATA),   // FIFO_READ
    .FIFO_EMPTY(FIFO_DATA_EMPTY),
    .FIFO_DATA(FIFO_DATA_DATA),

    .USERK_FIFO_READ(ARB_GRANT_USER_K),
    .USERK_FIFO_EMPTY(FIFO_USER_K_EMPTY),
    .USERK_FIFO_DATA(FIFO_USER_K_DATA),

    .LOST_ERROR(LOST_ERROR),
    .RX_SOFT_ERROR(RX_SOFT_ERROR),
    .RX_HARD_ERROR(RX_HARD_ERROR),

    .RX_LANE_UP(RX_LANE_UP),
    .RX_MULTILLANE_UP(RX_MULTILLANE_UP),
    .RX_CHANNEL_UP(RX_CHANNEL_UP),
    .RX_READY(),
    .PLL_LOCKED(PLL_LOCKED),

    .AURORA_RESET(AURORA_RESET)
);

endmodule
