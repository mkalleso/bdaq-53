`ifndef DESERIALIZER_FIND_SYNC_HEADER__SV
`define DESERIALIZER_FIND_SYNC_HEADER__SV

`timescale 1ns/1ps

module DeserializerFindSyncHeader
  (input logic [65:0] Data66,
   input logic DataValid,
   input logic Clk,
   input logic Rst_b,

   output logic [65:0] SyncData66,
   output logic SyncDataValid);

   logic [131:0] buf_132_notmr;
   logic [65:0]  syncData66_notmr;
   
   logic [5:0] 	 bufIdx;
   logic [5:0] 	 validCount;
 	 

   always_ff @(posedge Clk) begin
      if (Rst_b == 1'b0) begin
	 buf_132_notmr <= '0;
	 syncData66_notmr <= '0;
	 bufIdx <= '0;
	 SyncDataValid <= '0;
      end else begin
	 if (DataValid == 1'b1)
	   buf_132_notmr <= {Data66,buf_132_notmr[66 +: 66]};

	 if ((buf_132_notmr[(bufIdx) +: 2] == 2'b11) || (buf_132_notmr[(bufIdx) +: 2] == 2'b00)) begin // wrong sync header
	    bufIdx <= bufIdx + 1;
	    validCount <= '0;
	 end else begin
	    if ( ( validCount != 6'b111111 ) & DataValid)
	      validCount <= validCount + 1;
	    syncData66_notmr <= buf_132_notmr[bufIdx +: 66];
	 end
	 SyncDataValid <= DataValid & (validCount[5]);
	 
      end
   end // always_ff @

   assign SyncData66 = syncData66_notmr;
   
   
endmodule // DeserializerFindSyncHeader
`endif
