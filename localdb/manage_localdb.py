# LocalDB clommand line interface for BDAQ53
from .localdb_interface import DBReset, LocalDBinterface
import argparse
import os
import sys
import shutil

from time import sleep

import pymongo
import pexpect

from bdaq53.system import logger

log = logger.setup_derived_logger("LocalDBManager")


bdaq53_localdb_path = os.path.dirname(__file__)

LOCALDB_HOST = "127.0.0.1"
LOCALDB_PORT = 27017  # default mongodb port
DB_VERSION = 1.01


def check_environment():
    if not shutil.which("docker"):
        log.error("Cannot find docker executable. Is docker installed? See https://gitlab.cern.ch/silab/bdaq53/-/wikis/Local-data-base#installation")
        return False

    if not shutil.which("docker-compose"):
        log.error("Cannot find docker-compose executable. See https://gitlab.cern.ch/silab/bdaq53/-/wikis/Local-data-base#installation")
        return False

    child = pexpect.spawn("docker ps")
    if "permission denied" in child.read().decode():
        log.error("Current user has no rights to run docker. See https://gitlab.cern.ch/silab/bdaq53/-/wikis/Local-data-base#faq")
        return False
    return True


def start_database():
    # Use setup bash script
    cmd = "/bin/bash " + os.path.join(bdaq53_localdb_path, "setup_localdb.sh" + " -u silab -p silab -i BONN -d %s" % (os.path.join(bdaq53_localdb_path, "local_data")))
    child = pexpect.spawn(cmd, encoding="utf-8", logfile=sys.stdout, cwd=bdaq53_localdb_path)
    child.expect("All done", timeout=10 * 60)


def init_database():
    client = pymongo.MongoClient(host=LOCALDB_HOST, port=LOCALDB_PORT, serverSelectionTimeoutMS=3000)

    if "localdb" not in client.list_database_names():
        log.error("Please download the modules from the production database: http://http://127.0.0.1:5000/localdb/")
        return False

    collections = client["localdb"].list_collection_names()

    if "fs.files" not in collections:
        client["localdb"]["fs.files"].create_index([("hash", pymongo.DESCENDING), ("_id", pymongo.DESCENDING)])
        log.info('Create "fs.files" collection with index')

    if "component" not in collections or not any(["serialNumber" in i for i in list(client.localdb.component.index_information())]):
        client["localdb"]["component"].create_index([("serialNumber", pymongo.DESCENDING)])
        log.info('Create "component" collection with index')

    if "testRun" not in collections or not any(["startTime" in i for i in list(client.localdb.testRun.index_information())]):
        client["localdb"]["testRun"].create_index([("startTime", pymongo.DESCENDING), ("user_id", pymongo.DESCENDING), ("address", pymongo.DESCENDING)])
        log.info('Create "testRun" collection with index')

    if "componentTestRun" not in collections or not any(["name" in i for i in list(client.localdb.componentTestRun.index_information())]):
        client["localdb"]["componentTestRun"].create_index([("name", pymongo.DESCENDING), ("testRun", pymongo.DESCENDING)])
        log.info('Create "componentTestRun" collection with index')


def stop_database():
    # Use setup bash script
    child = pexpect.spawn("docker-compose down -v", encoding="utf-8", logfile=sys.stdout, cwd=bdaq53_localdb_path)
    child.expect(pexpect.EOF)


def delete_database():
    if not ping_data_base():
        log.error("Database is not running")
    else:
        try:
            LocalDBinterface(LOCALDB_HOST, LOCALDB_PORT, debug=False, dummy=False, _reset_db="soft")
        except DBReset:
            pass


def ping_data_base():
    client = pymongo.MongoClient(host=LOCALDB_HOST, port=LOCALDB_PORT, serverSelectionTimeoutMS=3000)
    try:
        client.admin.command("ping")  # The ping command is cheap and does not require auth.
    except pymongo.errors.ConnectionFailure:
        return False

    return True


def check_database_status():

    if not ping_data_base():
        log.error("No MongoDB reached at %s:%d" % (LOCALDB_HOST, LOCALDB_PORT))
        return False

    client = pymongo.MongoClient(host=LOCALDB_HOST, port=LOCALDB_PORT, serverSelectionTimeoutMS=3000)

    log.info("MongoDB running at %s:%d" % (LOCALDB_HOST, LOCALDB_PORT))

    if "localdbtools" not in client.list_database_names():
        log.error("Cannot find a localdb setup at %s:%d" % (LOCALDB_HOST, LOCALDB_PORT))
        return False
    tools_collections = client["localdbtools"].list_collection_names()

    if "QC.module.types" not in tools_collections:
        log.error("Please download the modules types from the production database: http://127.0.0.1:5000/localdb/download_pdinfo")
        return False

    if "localdb" not in client.list_database_names():
        log.error("Please download the modules from the production database: http://http://127.0.0.1:5000/localdb/")
        return False

    collections = client["localdb"].list_collection_names()

    # Do not require downloaded module info from production DB, since interface is likely often not working
    # if "QC.module.status" not in collections or "QC.status" not in tools_collections:
    #     log.error("Please download the modules from the production database or wait for the download: http://127.0.0.1:5000/localdb/download_component")
    #     return False

    # Check for collections required for uploading data
    if "component" not in collections or "componentTestRun" not in collections or "fs.files" not in collections or "testRun" not in collections:
        log.error('Not all database collections found. Call "bdaq_localdb --init"')
        return False

    return True


def main():
    parser = argparse.ArgumentParser(description="BDAQ53 local database manager.", formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument(
        "--start", required=False, action="store_true", help="Setup and start the local data base\nFor setup see https://gitlab.cern.ch/silab/bdaq53/-/wikis/Local-data-base",
    )

    parser.add_argument(
        "--init", required=False, action="store_true", help="Create required database collections",
    )

    parser.add_argument(
        "--stop", required=False, action="store_true", help="Stop the current local db server, but keep the data.",
    )

    parser.add_argument(
        "--status", required=False, action="store_true", help="Check if the database is setup and ready for usage.",
    )

    parser.add_argument(
        "--delete", required=False, action="store_true", help="Delete all local db data.",
    )

    parser.add_argument(
        "--upload", nargs=1, help="Upload chip or module data. Filename to interpreted data file.",
    )

    args = parser.parse_args()

    # Check if required programs are available
    if not check_environment():
        return

    if args.start:  # startup the database using docker
        log.info("Setup local database")
        start_database()
        sleep(0.2)  # some time for start up
        init_database()
        sleep(0.2)  # some time for start up
        if not check_database_status():
            return

        log.info("Done")

    if args.init:  # startup the database using docker
        log.info("Initialize local database")
        init_database()

    if args.status:  # check database status
        log.info("Check local database status")
        if check_database_status():
            log.info("Database ready for usage")

    if args.stop:  # startup the database using docker
        log.info("Stop local database")
        stop_database()

    if args.delete:  # startup the database using docker
        log.info("Deleting database")
        delete_database()

    if args.upload:
        filename = args.upload[0]

        if check_database_status():
            if not os.path.isfile(filename):
                log.error('Cannot find file "%s"' % filename)
            else:
                log.info(os.path.abspath(filename))
                module_path = os.path.abspath(os.path.join(os.path.dirname(filename), os.pardir))
                chips_folders = [d for d in os.listdir(module_path) if os.path.isdir(os.path.join(module_path, d))]
                for f in chips_folders:
                    chip_path = os.path.join(module_path, f, os.path.basename(filename))
                    if not os.path.isfile(chip_path):
                        log.error('Cannot find file "%s"' % filename)
                    else:
                        ldb = LocalDBinterface(LOCALDB_HOST, LOCALDB_PORT, debug=False, dummy=False, _reset_db=None)
                        ldb.init()
                        log.info('Upload chip file "%s"' % chip_path)
                        ldb.upload_scan_for_chip(chip_path)
