#! /bin/bash

#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

HELPSTRING="Setup tool for localDB for BDAQ53 using docker.
Prerequisites:
    docker, docker-compose>=1.16.0, md5sum, git.
Options:
    -h Show this help message.
    -u [mandatory] User name. e.g. 'silab'
    -p [mandatory] Password for default user.
    -i [mandatory] Institution code as defined in the ITk Production DB.
    -d [mandatory] Data directory. Base directory for docker volumes. Absolute path.
    -b [optional] Force rebuild of localdb_viewer docker image."

FORCE_BUILD=false

while getopts u:p:i:d:hb option
do
case "${option}"
in
h) echo "$HELPSTRING"; exit;;
u) USR=${OPTARG};;
p) PASS=${OPTARG};;
i) INSTITUTION=${OPTARG};;
d) DATA_DIR=${OPTARG};;
b) FORCE_BUILD=true
esac
done

# echo "USR = $USR"
# echo "PASS = $PASS"
# echo "INSTITUTION = $INSTITUTION"
# echo "DATA_DIR = $DATA_DIR"
# echo "FORCE_BUILD = $FORCE_BUILD"

if [ "$USR" = "" ] || [ "$PASS" = "" ] || [ "$INSTITUTION" = "" ] || [ "$DATA_DIR" = "" ]; then
    echo "ERROR: Options -u (USER), -p (PASSWORD), -i (INSTITUTION) and -d (DATA_DIRECTORY) must be defined!"
    echo "$HELPSTRING"
    exit
fi
PASS_HASH=`echo -n $PASS | md5sum | awk '{print $1}'`
USR_HASH=`echo -n $USR | md5sum | awk '{print $1}'`

# Create data directories
echo "Creating localDB data directories..."
mkdir $DATA_DIR
mkdir $DATA_DIR"/mongodb"
mkdir $DATA_DIR"/influxdb"
mkdir $DATA_DIR"/grafana"

# Create .env file for docker compose
echo "Creating .env file..."
ENV_TEMPLATE="INFLUXDB_USERNAME='admin'
INFLUXDB_PASSWORD='admin'
GRAFANA_USERNAME=$USR
GRAFANA_PASSWORD=$PASS

DATA_PATH_MONGODB=$DATA_DIR/mongodb
DATA_PATH_INFLUXDB=$DATA_DIR/influxdb
DATA_PATH_GRAFANA=$DATA_DIR/grafana"
# LOCALDB_USERNAME='$USR'
# LOCALDB_PASSWORD='$PASS'


echo "$ENV_TEMPLATE" > ./.env

# Setup viewer admin access (content of viewer/setup_viewer.sh)
echo -e $USR_HASH\\n$PASS_HASH > localdb_viewer/.KeyFile

# Create custom localdb_server image from Dockerfile
IMAGE_EXISTS=false
[ -z $(docker images -q localdb_viewer) ] || IMAGE_EXISTS=true

if [ "$IMAGE_EXISTS" = false ] || [ "$FORCE_BUILD" = true ]; then
    echo "Creating custom image for viewer application..."
    git clone --depth 1 --branch ldbtoolv1.6.2 https://gitlab.cern.ch/YARR/localdb-tools localdb_viewer/localdb_tools
    git clone --depth 1 --branch v2.0.0 https://gitlab.cern.ch/YARR/utilities/plotting-tools localdb_viewer/localdb_tools/viewer/plotting-tool
    # Development branch required to use c++17 as the updated root version in docker, issue: https://gitlab.cern.ch/YARR/utilities/localdb-qcanalysis-tools/-/issues/1
    git clone --depth 1 --branch devel https://gitlab.cern.ch/YARR/utilities/localdb-qcanalysis-tools.git localdb_viewer/localdb_tools/viewer/analysis-tool
    docker build -t localdb_viewer localdb_viewer/
else
    echo "Viewer application image already exists."
fi

# Create containers via docer compose
echo "Creating localDB container stack..."
docker-compose up -d

# Create telegraf user in influxdb for telegraf plugin to work
echo "Creating telegraf user in influxdb..."
docker exec influxdb influx -execute "CREATE DATABASE telegraf"
docker exec influxdb influx -database "telegraf" -execute "CREATE USER telegraf WITH PASSWORD 'secretpassword'"

# Create localDB admin account (content of /create_admin.sh)
echo "Chreating users and setting up admin access..."
MONGO_CODE="db.dropUser('$USR')
            db.createUser({user: '$USR',
                           pwd: '$PASS',
                           roles: [{role: 'readWrite', db: 'localdb'},
                                   {role: 'readWrite', db: 'localdbtools'},
                                   {role: 'userAdmin', db: 'localdb'},
                                   {role: 'userAdmin', db: 'localdbtools'}]
            })
            db.dropUser('$USR_HASH')
            db.createUser({user: '$USR_HASH',
                           pwd: '$PASS_HASH',
                           roles: [{role: 'readWrite', db: 'localdb'},
                                   {role: 'readWrite', db: 'localdbtools'},
                                   {role: 'userAdmin', db: 'localdb'},
                                   {role: 'userAdmin', db: 'localdbtools'}]
            })"

docker exec mongodb bash -c "echo \"$MONGO_CODE\" > /tmp/mongo_admin.js"
docker exec mongodb mongo localdb /tmp/mongo_admin.js -> /tmp/mongo_admin.log

# Create localDB viewer account
MONGO_CODE="db.viewer.user.remove({'username':'$USR'})
            db.viewer.user.insert({'sys': {'rev':0, 'cts':new Date(Date.now()), 'mts':new Date(Date.now())},
                        'username': '$USR',
                        'name': '$USR',
                        'auth': 'adminViewer',
                        'institution': '',
                        'Email': '',
                        'password': '$PASS_HASH'
            })"

docker exec mongodb bash -c "echo \"$MONGO_CODE\" > /tmp/mongo_viewer.js"
docker exec mongodb mongo localdbtools /tmp/mongo_viewer.js -> /tmp/mongo_viewer.log

# Create local config file .localdb.yaml
cp config/localdb.yaml.default .localdb.yaml
sed -i -e "s/USRNAME/$USR/g" .localdb.yaml
sed -i -e "s/INSTITUTION/$INSTITUTION/g" .localdb.yaml

# Cleanup
echo "Cleaning up..."
rm -rf localdb_viewer/localdb_tools
rm localdb_viewer/.KeyFile
rm .env

echo "All done!"
