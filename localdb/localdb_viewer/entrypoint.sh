#!/bin/bash
set -e

if [ "${1:0:1}" = '-' ]; then
    set -- python3 app.py "$@"
    cd /home/localdb_tools/viewer
fi

exec "$@"
