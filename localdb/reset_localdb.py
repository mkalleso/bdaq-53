from manage_localdb import localDB_interface

if __name__ == "__main__":
    localDB_interface("127.0.0.1", 27017, debug=False, dummy=False, _reset_db="soft")
