#!/usr/bin/env python

from setuptools import setup
from setuptools import find_packages

import bdaq53

version = bdaq53.__version__

author = "Michael Daas, Yannick Dieter, Tomasz Hemperek, David-Leon Pohl, Mark Standke, Marco Vogt"
author_email = ""

# Requirements
install_requires = [
    "basil-daq>=3.2.0<3.3.0",
    "chardet<4.0",
    "bitarray>=0.8.1<1.6.1",
    "lxml",
    "matplotlib>=3.2.0",
    "numpy",
    "online_monitor>=0.4.1<0.5",
    "pillow",
    "pixel_clusterizer==3.1.4",
    "tables",
    "pyyaml",
    "pyzmq",
    "scipy",
    "numba",
    "tqdm",
    "pyserial",
    "slackclient>=2.0.1",
    "gitpython",
    "pexpect",
    "coloredlogs",
    "uncertainties",
    "requests",
    "pyqtgraph<0.12",
    "pymongo"
]  # Aditional requirements for localDB module

setup(
    name="bdaq53",
    version=version,
    description="DAQ for RD53A prototype",
    url="https://gitlab.cern.ch/silab/bdaq53",
    license="",
    long_description="",
    author=author,
    maintainer=author,
    author_email=author_email,
    maintainer_email=author_email,
    install_requires=install_requires,
    python_requires=">=3.0",
    setup_requires=["online_monitor>=0.4.1<0.5"],
    packages=find_packages(),
    include_package_data=True,
    platforms="any",
    entry_points={
        "console_scripts": [
            "bdaq = bdaq53.bdaq53_cli:main",
            "bdaq_localdb = localdb.manage_localdb:main",
            "bdaq_firmware = bdaq53.manage_firmware:main",
            "bdaq_monitor = bdaq53.analysis.online_monitor.start_bdaq53_monitor:main",
            "bdaq_eudaq = bdaq53.scans.scan_eudaq:main",
            "bdaq_config = bdaq53.manage_configuration:main",
            "bdaq_db = bdaq53.manage_databases:main",
            "bdaq_fixtures = bdaq53.tests.create_fixtures:main",
            # Old commands left for backwards compatibility
            "bdaq53 = bdaq53.bdaq53_cli:deprecated",
            "bdaq53_monitor = bdaq53.bdaq53_cli:deprecated",
            "bdaq53_eudaq = bdaq53.bdaq53_cli:deprecated",
        ]
    },
)

# FIXME: bad practice to put code into setup.py
# Add the online_monitor bdaq53 plugins
try:
    import os
    from online_monitor.utils import settings

    # Get the absoulte path of this package
    package_path = os.path.dirname(bdaq53.__file__)
    # Add online_monitor plugin folder to entity search paths
    settings.add_producer_sim_path(os.path.join(package_path, "analysis", "online_monitor"))
    settings.add_converter_path(os.path.join(package_path, "analysis", "online_monitor"))
    settings.add_receiver_path(os.path.join(package_path, "analysis", "online_monitor"))
except ImportError:
    pass
